<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// we will use the composer autoloader to make sure we autoload the packages installed by composer
// require_once APPPATH.'../vendor/autoload.php';

class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('SecurityUser');
        $this->load->library('grocery_CRUD');

        date_default_timezone_set("America/Guayaquil");
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -  
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function index() {
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Inicio";

            $dataHeader['titlePage'] = $titulo;
            $dataContent['debug'] = $debug;

            $data['header'] = $this->load->view('arquitectos/blocks/header', $dataHeader);
            $data['menu'] = $this->load->view('arquitectos/blocks/menu-admin', $dataHeader );

            $data['content'] = $this->load->view('arquitectos/admin/index', array());
            $data['footer'] = $this->load->view('arquitectos/blocks/footer', array());
        } else {
            redirect("admin/login");
        }
    }

    public function login() {
        $debug = false;

        $dataHeader['titlePage'] = 'Inicio Admin';
        $dataContent['debug'] = $debug;

        $data['header'] = $this->load->view('arquitectos/blocks/header', $dataHeader);
        $data['content'] = $this->load->view('arquitectos/login', $dataContent);
        $data['footer'] = $this->load->view('arquitectos/blocks/footer', array());
    }

     public function logout() {

        $securityUser = new SecurityUser();
        $securityUser->logout();
        $debug = false;

        $dataHeader['titlePage'] = 'Inicio Admin';
        $dataContent['debug'] = $debug;

        $data['header'] = $this->load->view('arquitectos/blocks/header', $dataHeader);
        $data['content'] = $this->load->view('arquitectos/login', $dataContent);
        $data['footer'] = $this->load->view('arquitectos/blocks/footer', array());
    }

    public function autentificar() {

        $username = $this->input->post("username");
        $password = $this->input->post("password");

        $securityUser = new SecurityUser();

        $securityUser->login($username, $password);
        // echo "Paso";
        // die();

        if ( $this->session->userdata('user_login')) {
            // Para el admin
            redirect("admin/index");
        }else{
            redirect("admin/logout");
        }
    }
    
    function securityCheck() {
        $usuario = $this->session->userdata('user_login');
        if ($usuario == "") {
            return false;
        } else {
            return true;
        }
    }

    function _add_default_date_value(){
        $value = !empty($value) ? $value : date("d/m/Y");
        $return = '<input type="text" name="date" value="'.$value.'" class="datepicker-input" /> ';
        $return .= '<a class="datepicker-input-clear" tabindex="-1">Clear</a> (dd/mm/yyyy)';
        return $return;
    }

    function encrypt_pw($post_array, $pk) {
        if(!empty($post_array['clave'])) {
            $data['clave'] = md5($post_array['clave']);
            $this->db->where('id', $pk);
            $this->db->update('usuario', $data);
        }
    }

    //////////////////////////////////////////////////////////////
    // arquitectos CajaNegraEC - Tratamiento data
    //////////////////////////////////////////////////////////////

    //Function to generate the map field

    public function show_map_field($value = false, $primary_key = false) {
        return '<p>Filtrar la posición arrastrando el puntero a su ubicación</p>
                <div id="retailer-map" style="width:530px; height:300px;"></div>';
    }

    public function plot_point_js() {
        $retailer_id = $this->session->userdata('retailer_id');
        $retailer = $this->cModel->getByField('retailers', 'rid', $retailer_id);
        if (count($retailer) > 0) {
            $latitude = $retailer['latitude'];
            $longitude = $retailer['longitude'];
            $script = '
                var map;
                var marker;
                var circle;
                var geocoder;
                window.onload = function() {
                    geocoder = new google.maps.Geocoder();
                    var latlng = new google.maps.LatLng(' . $latitude . ',' . $longitude . ');
                    var myOptions = {
                      zoom: 18,
                      center: latlng,
                      mapTypeId: google.maps.MapTypeId.SATELLITE
                    };
                    map = new google.maps.Map(document.getElementById("retailer-map"), myOptions);
                    addMarker(map.getCenter());
                    google.maps.event.addListener(map,"click", function(event) {
                        //alert("You cannot reset the location by changing pointer in here");
                        //addMarker(event.latLng);
                    });
                }
                            
                function addMarker(location) {
                    if(marker) {marker.setMap(null);}
                    marker = new google.maps.Marker({
                        position: location,
                        draggable: true
                    });
                    marker.setMap(map);
                }
            ';
            echo $script;
        }
    }

    public function unset_map_field_add($post_array){
        unset($post_array['map']);
        return $post_array;
    }

    public function unset_map_field_edit($post_array){
        unset($post_array['map']);
        return $post_array;
    }



    //////////////////////////////////////////////////////////////
    // arquitectos CajaNegraEC - Redimensionar Imagenes
    //////////////////////////////////////////////////////////////


    public function redimensionarImgsCliente($uploader_response,$field_info, $files_to_upload){
        $this->load->library('image_moo');
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;


        if ( $field_info->field_name == "logo" ) {
            $this->image_moo
                    ->load($file_uploaded)
                    ->set_background_colour("#FFFFFF")
                    ->resize(800,1000, TRUE)
                    ->save($file_uploaded,TRUE);
            
            if($this->image_moo->errors) {
                print $this->image_moo->display_errors();
            }
        }elseif ( $field_info->field_name == "bannerpromo" ) {
            $this->image_moo
                    ->load($file_uploaded)
                    ->set_background_colour("#FFFFFF")
                    ->resize(800,200, TRUE)
                    ->save($file_uploaded,TRUE);

            if($this->image_moo->errors) {
                print $this->image_moo->display_errors();
            }
        }

        return true;
    }

    public function redimensionarImgsLocal($uploader_response,$field_info, $files_to_upload){
        $this->load->library('image_moo');
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;

        if ( $field_info->field_name == "logo" ) {
            $this->image_moo
                    ->load($file_uploaded)
                    ->set_background_colour("#000000")
                    ->resize(800,1000, TRUE)
                    ->save($file_uploaded,TRUE);
            
            if($this->image_moo->errors) {
                print $this->image_moo->display_errors();
            }
        }elseif ( $field_info->field_name == "foto" ) {
            $this->image_moo
                    ->load($file_uploaded)
                    ->set_background_colour("#FFFFFF")
                    ->resize(800,400, TRUE)
                    ->save($file_uploaded,TRUE);

            if($this->image_moo->errors) {
                print $this->image_moo->display_errors();
            }
        }elseif ( $field_info->field_name == "foto1" ) {
            $this->image_moo
                    ->load($file_uploaded)
                    ->set_background_colour("#FFFFFF")
                    ->resize(800,400, TRUE)
                    ->save($file_uploaded,TRUE);

            if($this->image_moo->errors) {
                print $this->image_moo->display_errors();
            }
        }elseif ( $field_info->field_name == "foto2" ) {
            $this->image_moo
                    ->load($file_uploaded)
                    ->set_background_colour("#FFFFFF")
                    ->resize(800,400, TRUE)
                    ->save($file_uploaded,TRUE);

            if($this->image_moo->errors) {
                print $this->image_moo->display_errors();
            }
        }elseif ( $field_info->field_name == "foto3" ) {
            $this->image_moo
                    ->load($file_uploaded)
                    ->set_background_colour("#FFFFFF")
                    ->resize(800,400, TRUE)
                    ->save($file_uploaded,TRUE);

            if($this->image_moo->errors) {
                print $this->image_moo->display_errors();
            }
        }elseif ( $field_info->field_name == "foto4" ) {
            $this->image_moo
                    ->load($file_uploaded)
                    ->set_background_colour("#FFFFFF")
                    ->resize(800,400, TRUE)
                    ->save($file_uploaded,TRUE);

            if($this->image_moo->errors) {
                print $this->image_moo->display_errors();
            }
        }

        return true;
    }


    public function redimensionarImgsPromocion($uploader_response,$field_info, $files_to_upload){
        $this->load->library('image_moo');
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;


        if ( $field_info->field_name == "foto" ) {
            $this->image_moo
                    ->load($file_uploaded)
                    ->set_background_colour("#FFFFFF")
                    ->resize(800,800, TRUE)
                    ->save($file_uploaded,TRUE);
            
            if($this->image_moo->errors) {
                print $this->image_moo->display_errors();
            }
        }elseif ( $field_info->field_name == "fotoprincipal" ) {
            $this->image_moo
                    ->load($file_uploaded)
                    ->set_background_colour("#FFFFFF")
                    ->resize(800,400, TRUE)
                    ->save($file_uploaded,TRUE);

            if($this->image_moo->errors) {
                print $this->image_moo->display_errors();
            }
        }

        return true;
    }

    //////////////////////////////////////////////////////////////
    // CajaNegraEC - Funciones para mantenimientos
    //////////////////////////////////////////////////////////////




    //////////////////////////////////////////////////////////////
    // CajaNegraEC - Mantenimientos
    //////////////////////////////////////////////////////////////

    public function portfolio(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Portfolio";

            $crud = new grocery_CRUD();
            $crud->set_table("portafolio");
            $crud->set_subject( $titulo );
            $crud->display_as('titulo', 'Titulo');
            $crud->display_as('titulo_en', 'Titulo (Inglés)');
            $crud->display_as('descripcion', 'Descripcion del portafolio');
            $crud->display_as('descripcion_en', 'Descripcion del portafolio (Inglés)');
            $crud->display_as('home_imagen', 'Foto Preview (680x770 px)');
            $crud->display_as('imagen', 'Foto Completa (1024x768 px)');
            $crud->display_as('estado','Estado');
            
            $crud->field_type('estado', 'dropdown', array(
                '0' => 'Inactivo',
                '1' => 'Activo',
            ));
            $crud->set_field_upload('imagen', 'assets/uploads/portfolio','jpg|png|jpeg');
            $crud->set_field_upload('home_imagen', 'assets/uploads/portfolio_home','jpg|png|jpeg');

            $crud->columns('titulo', 'home_imagen', 'imagen', 'estado' );
            $crud->fields('titulo', 'titulo_en', 'descripcion', 'descripcion_en', 'home_imagen', 'imagen', 'estado' );
            $crud->required_fields('titulo', 'imagen','home_imagen', 'estado');

            $crud->unset_texteditor('descripcion','full_text');
            $crud->unset_texteditor('descripcion_en','full_text');

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;


            $data['header'] = $this->load->view('arquitectos/blocks/header', $dataHeader);
            $data['menu'] = $this->load->view('arquitectos/blocks/menu-admin', $dataContent );

            $data['content'] = $this->load->view('arquitectos/admin/admin-content', $output);
            $data['footer'] = $this->load->view('arquitectos/blocks/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }

    public function audio(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Canciones";

            $crud = new grocery_CRUD();
            $crud->set_table("audio");
            $crud->set_subject( $titulo );
            $crud->display_as('nombre', 'Nombre');
            $crud->display_as('archivo', 'Archivo');
            $crud->display_as('estado','Estado');
            
            $crud->field_type('estado', 'dropdown', array(
                '0' => 'Inactivo',
                '1' => 'Activo',
            ));
            $crud->set_field_upload('archivo', 'assets/uploads/audio','mp3|wav');

            $crud->columns('nombre', 'archivo', 'estado');
            $crud->fields('nombre', 'archivo', 'estado');
            $crud->required_fields('nombre', 'archivo', 'estado');

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;


            $data['header'] = $this->load->view('arquitectos/blocks/header', $dataHeader);
            $data['menu'] = $this->load->view('arquitectos/blocks/menu-admin', $dataContent );

            $data['content'] = $this->load->view('arquitectos/admin/admin-content', $output);
            $data['footer'] = $this->load->view('arquitectos/blocks/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }

    public function slideshow(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Presentacion";

            $crud = new grocery_CRUD();
            $crud->set_table("slideshow");
            $crud->set_subject( $titulo );
            $crud->display_as('titulo', 'Titulo');
            $crud->display_as('titulo_en', 'Titulo (Inglés)');
            $crud->display_as('caption', 'Descripcion del slideshow');
            $crud->display_as('caption_en', 'Descripcion del slideshow (Inglés)');
            $crud->display_as('enlace', 'Enlace exterior');
            $crud->display_as('imagen', 'Imagen (950px x 860px)');
            $crud->display_as('fondo', 'Fondo (1820px x 1142px)');
            $crud->display_as('estado','Estado');
            
            $crud->field_type('estado', 'dropdown', array(
                '0' => 'Inactivo',
                '1' => 'Activo',
            ));
            $crud->set_field_upload('imagen', 'assets/uploads/slideshow_img','jpg|png|jpeg');
            $crud->set_field_upload('fondo', 'assets/uploads/slideshow_fondo','jpg|png|jpeg');

            $crud->columns('titulo', 'titulo_en', 'caption', 'caption_en', 'imagen', 'fondo', 'enlace' , 'estado' );
            $crud->fields('titulo', 'titulo_en', 'caption', 'caption_en', 'imagen', 'fondo' , 'estado' );
            $crud->required_fields('titulo', 'imagen', 'fondo', 'enlace', 'estado');

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;


            $data['header'] = $this->load->view('arquitectos/blocks/header', $dataHeader);
            $data['menu'] = $this->load->view('arquitectos/blocks/menu-admin', $dataContent );

            $data['content'] = $this->load->view('arquitectos/admin/admin-content', $output);
            $data['footer'] = $this->load->view('arquitectos/blocks/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }

    public function estudio(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Información acerca del estudio";

            $crud = new grocery_CRUD();
            $crud->set_table("estudio");
            $crud->set_subject( $titulo );
            $crud->display_as('saludo','Saludo en Pagina Principal');
            $crud->display_as('saludo_en','Saludo en Pagina Principal (Inglés)');
            $crud->display_as('acerca','Quienes somos');
            $crud->display_as('acerca_en','Quienes somos (Inglés)');
            $crud->display_as('imagen_acerca','Imagen de Quienes somos (666px, 565px)');
            $crud->display_as('fondo_acerca','Fondo de Quienes somos (1347px, 989px)');
            $crud->display_as('vision','Vision');
            $crud->display_as('vision_en','Vision (Inglés)');
            $crud->display_as('imagen_vision','Imagen para Vision (666px, 565px)');
            $crud->display_as('fondo_vision','Fondo para Vision (1347px, 989px)');
            $crud->display_as('mision','Mision');
            $crud->display_as('mision_en','Mision (Inglés)');
            $crud->display_as('imagen_mision','Imagen para Mision (666px, 565px)');
            $crud->display_as('fondo_mision','Fondo para Mision (1347px, 989px)');
            $crud->display_as('ventajas','Texto Ventajas');
            $crud->display_as('ventajas_en','Texto Ventajas (Inglés)');
            $crud->display_as('imagen_ventajas','Imagen para Ventajas (666px, 565px)');
            $crud->display_as('fondo_ventajas','Fondo para Ventajas (1347px, 989px)');
            $crud->display_as('estado','Estado');
            
            $crud->field_type('estado', 'dropdown', array(
                '0' => 'Inactivo',
                '1' => 'Activo',
            ));
            $crud->set_field_upload('imagen_acerca', 'assets/uploads/imagen_acerca','jpg|png|jpeg');
            $crud->set_field_upload('fondo_acerca', 'assets/uploads/fondo_acerca','jpg|png|jpeg');
            $crud->set_field_upload('imagen_vision', 'assets/uploads/imagen_vision','jpg|png|jpeg');
            $crud->set_field_upload('fondo_vision', 'assets/uploads/fondo_vision','jpg|png|jpeg');
            $crud->set_field_upload('imagen_mision', 'assets/uploads/imagen_mision','jpg|png|jpeg');
            $crud->set_field_upload('fondo_mision', 'assets/uploads/fondo_mision','jpg|png|jpeg');
            $crud->set_field_upload('imagen_ventajas', 'assets/uploads/imagen_ventajas','jpg|png|jpeg');
            $crud->set_field_upload('fondo_ventajas', 'assets/uploads/fondo_ventajas','jpg|png|jpeg');

            $crud->columns('saludo','acerca','fondo_acerca','vision','fondo_vision','mision','fondo_mision','ventajas','fondo_ventajas', 'estado' );
            $crud->fields('saludo','saludo_en','acerca','acerca_en','imagen_acerca','fondo_acerca','vision','vision_en','imagen_vision','fondo_vision','mision','mision_en','imagen_mision','fondo_mision','ventajas','ventajas_en','imagen_ventajas','fondo_ventajas', 'estado'  );
            $crud->required_fields('saludo','acerca', 'imagen_acerca', 'vision', 'imagen_vision', 'mision', 'imagen_mision', 'ventajas', 'imagen_ventajas', 'estado');

            $crud->unset_texteditor('saludo','full_text');
            $crud->unset_texteditor('saludo_en','full_text');
            $crud->unset_texteditor('acerca','full_text');
            $crud->unset_texteditor('acerca_en','full_text');
            $crud->unset_texteditor('vision','full_text');
            $crud->unset_texteditor('vision_en','full_text');
            $crud->unset_texteditor('mision','full_text');
            $crud->unset_texteditor('mision_en','full_text');
            $crud->unset_texteditor('ventajas','full_text');
            $crud->unset_texteditor('ventajas_en','full_text');

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;


            $data['header'] = $this->load->view('arquitectos/blocks/header', $dataHeader);
            $data['menu'] = $this->load->view('arquitectos/blocks/menu-admin', $dataContent );

            $data['content'] = $this->load->view('arquitectos/admin/admin-content', $output);
            $data['footer'] = $this->load->view('arquitectos/blocks/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }

    public function asociado(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Asociados";

            $crud = new grocery_CRUD();
            $crud->set_table("asociado");
            $crud->set_subject( $titulo );
            $crud->display_as('titulo', 'Titulo');
            $crud->display_as('descripcion', 'Descripcion del portafolio');
            $crud->display_as('descripcion_en', 'Descripcion del portafolio (Inglés)');
            $crud->display_as('imagen', 'Foto (270px, 247px)');
            $crud->display_as('pdf', 'CV del asociado');
            $crud->display_as('estado','Estado');
            
            $crud->field_type('estado', 'dropdown', array(
                '0' => 'Inactivo',
                '1' => 'Activo',
            ));
            $crud->set_field_upload('imagen', 'assets/uploads/asociado','jpg|png|jpeg');
            $crud->set_field_upload('pdf', 'assets/uploads/asociado-pdf','pdf|doc|docx');

            $crud->columns('titulo', 'descripcion', 'descripcion_en', 'imagen' , 'pdf', 'estado' );
            $crud->fields('titulo', 'descripcion', 'descripcion_en', 'imagen' , 'pdf', 'estado' );
            $crud->required_fields('titulo', 'descripcion', 'imagen', 'estado');

            $crud->unset_texteditor('descripcion','full_text');
            $crud->unset_texteditor('descripcion_en','full_text');

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;


            $data['header'] = $this->load->view('arquitectos/blocks/header', $dataHeader);
            $data['menu'] = $this->load->view('arquitectos/blocks/menu-admin', $dataContent );

            $data['content'] = $this->load->view('arquitectos/admin/admin-content', $output);
            $data['footer'] = $this->load->view('arquitectos/blocks/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }

    public function footer(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Banners en footer";

            $crud = new grocery_CRUD();
            $crud->set_table("footer");
            $crud->set_subject( $titulo );
            $crud->display_as('titulo', 'Titulo');
            $crud->display_as('titulo_en', 'Titulo (Inglés)');
            $crud->display_as('descripcion', 'Descripcion del portafolio');
            $crud->display_as('descripcion_en', 'Descripcion del portafolio (Inglés)');
            $crud->display_as('fondo', 'Foto (1855px, 925px)');
            $crud->display_as('estado','Estado');
            
            $crud->field_type('estado', 'dropdown', array(
                '0' => 'Inactivo',
                '1' => 'Activo',
            ));
            $crud->set_field_upload('fondo', 'assets/uploads/footer','jpg|png|jpeg');

            $crud->columns('titulo', 'titulo_en', 'descripcion', 'descripcion_en', 'fondo' , 'estado' );
            $crud->fields('titulo', 'titulo_en', 'descripcion', 'descripcion_en', 'fondo' , 'estado' );
            $crud->required_fields('titulo', 'descripcion', 'fondo', 'estado');

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;


            $data['header'] = $this->load->view('arquitectos/blocks/header', $dataHeader);
            $data['menu'] = $this->load->view('arquitectos/blocks/menu-admin', $dataContent );

            $data['content'] = $this->load->view('arquitectos/admin/admin-content', $output);
            $data['footer'] = $this->load->view('arquitectos/blocks/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }

    public function servicio(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Servicios";

            $crud = new grocery_CRUD();
            $crud->set_table("servicio");
            $crud->set_subject( $titulo );
            $crud->display_as('titulo', 'Titulo');
            $crud->display_as('titulo_en', 'Titulo (Inglés)');
            $crud->display_as('descripcion', 'Descripcion del servicio');
            $crud->display_as('descripcion_en', 'Descripcion del servicio (Inglés)');
            $crud->display_as('nombre_icono', 'Icono');
            $crud->display_as('estado','Estado');
            
            $crud->field_type('estado', 'dropdown', array(
                '0' => 'Inactivo',
                '1' => 'Activo',
            ));
            $crud->set_field_upload('fondo', 'assets/uploads/servicio','jpg|png|jpeg');

            $crud->columns('titulo', 'titulo_en', 'descripcion', 'descripcion_en', 'nombre_icono' , 'estado' );
            $crud->fields('titulo', 'titulo_en', 'descripcion', 'descripcion_en', 'nombre_icono' , 'estado' );
            $crud->required_fields('titulo', 'nombre_icono', 'estado');

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $crud->unset_texteditor('descripcion','full_text');
            $crud->unset_texteditor('descripcion_en','full_text');

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;


            $data['header'] = $this->load->view('arquitectos/blocks/header', $dataHeader);
            $data['menu'] = $this->load->view('arquitectos/blocks/menu-admin', $dataContent );

            $data['content'] = $this->load->view('arquitectos/admin/admin-content', $output);
            $data['footer'] = $this->load->view('arquitectos/blocks/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }

    public function local(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Locales";

            $crud = new grocery_CRUD();
            $crud->set_table("local");
            $crud->set_subject( $titulo );
            $crud->display_as('direccion', 'Direccion');
            $crud->display_as('direccion_en', 'Direccion (Inglés)');
            $crud->display_as('celular', 'Celular del local');
            $crud->display_as('telefono', 'Telefono del local');
            $crud->display_as('email', 'Correo electronico');
            $crud->display_as('horario', 'Horario de atencion');
            $crud->display_as('latitude', 'Latitud');
            $crud->display_as('longitude', 'Longitud');
            $crud->display_as('estado','Estado');
            
            $crud->field_type('estado', 'dropdown', array(
                '0' => 'Inactivo',
                '1' => 'Activo',
            ));

            $crud->columns('direccion', 'direccion_en', 'celular', 'telefono', 'email', 'horario' , 'estado' );
            $crud->fields('direccion', 'direccion_en', 'celular', 'telefono', 'email', 'horario' , 'latitude', 'longitude', 'estado' );
            $crud->required_fields('direccion', 'celular', 'email', 'horario', 'latitude', 'longitude', 'estado');

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;


            $data['header'] = $this->load->view('arquitectos/blocks/header', $dataHeader);
            $data['menu'] = $this->load->view('arquitectos/blocks/menu-admin', $dataContent );

            $data['content'] = $this->load->view('arquitectos/admin/admin-content', $output);
            $data['footer'] = $this->load->view('arquitectos/blocks/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }


    public function razones(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Razones para elegirnos";

            $crud = new grocery_CRUD();
            $crud->set_table("razones");
            $crud->set_subject( $titulo );
            $crud->display_as('titulo', 'Titulo');
            $crud->display_as('titulo_en', 'Titulo (Inglés)');
            $crud->display_as('descripcion', 'Descripcion del portafolio');
            $crud->display_as('descripcion_en', 'Descripcion del portafolio (Inglés)');
            $crud->display_as('estado','Estado');
            
            $crud->field_type('estado', 'dropdown', array(
                '0' => 'Inactivo',
                '1' => 'Activo',
            ));

            $crud->columns('titulo', 'titulo_en', 'descripcion', 'descripcion_en' , 'estado' );
            $crud->fields('titulo', 'titulo_en', 'descripcion', 'descripcion_en' , 'estado' );
            $crud->required_fields('titulo', 'descripcion', 'estado');

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;


            $data['header'] = $this->load->view('arquitectos/blocks/header', $dataHeader);
            $data['menu'] = $this->load->view('arquitectos/blocks/menu-admin', $dataContent );

            $data['content'] = $this->load->view('arquitectos/admin/admin-content', $output);
            $data['footer'] = $this->load->view('arquitectos/blocks/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }

    public function callback_local_insert($post_array, $pk) {
        $dias = ["", "lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo"];
        $data = array();
        for ($i = 1; $i <= 7; $i++) {
            if (isset($post_array['hi_' . $i])) {
                $data[$dias[$i] . "_hi"] = $post_array['hi_' . $i];
            } else {
                $data[$dias[$i] . "_hi"] = NULL;
            }
            if (isset($post_array['hf_' . $i])) {
                $data[$dias[$i] . "_hf"] = $post_array['hf_' . $i];
            } else {
                $data[$dias[$i] . "_hf"] = NULL;
            }
        }
        $this->db->where('id', $pk);
        $this->db->update('local', $data);
    }


    public function usuario(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Usuario";

            $crud = new grocery_CRUD();
            $crud->set_table("usuario");
            $crud->set_subject( $titulo );
            $crud->display_as('email','Email');
            $crud->display_as('nombre','Nombre completo');
            $crud->display_as('clave','Contraseña');
            $crud->display_as('estado','Estado');

            $crud->field_type('clave', 'password');
            $crud->callback_after_insert(array($this,'encrypt_pw'));
            $crud->callback_after_update(array($this,'encrypt_pw'));
            
            $crud->display_as('fecha','Fecha de actualización del sistema');
            // $crud->display_as('idcliente','Cliente');
            // $crud->display_as('idrol','Rol');
            
            $crud->columns(/*'idcliente', 'idrol',*/ 'nombre', 'email', 'fecha', 'estado' );
            $crud->fields(/*'idcliente', 'idrol', */'nombre', 'email', 'clave', 'estado');
            $crud->required_fields('nombre', 'email', 'clave', 'idcliente', 'idrol', 'estado');
            
            // $crud->set_relation('idcliente', 'cliente', 'nombre');
            // $crud->set_relation('idrol', 'rol', 'nombre');

            $crud->field_type('fecha', 'hidden', date('Y-m-d H:i:s') );
            $crud->field_type('estado', 'dropdown', array(
                '1' => 'Activo',
                '0' => 'Inactivo'
            ));
            $crud->callback_before_update(array($this,'callback_email_activacion'));

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();
            
            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;

            $data['header'] = $this->load->view('arquitectos/blocks/header', $dataHeader);
            $data['menu'] = $this->load->view('arquitectos/blocks/menu-admin', $dataHeader );

            $data['content'] = $this->load->view('arquitectos/admin/admin-content', $output);
            $data['footer'] = $this->load->view('arquitectos/blocks/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }


    public function enviarEmailActivacion($userDestinatario = "fabricio@cajanegra.com.ec" ){


        //Create the Transport. I created it using the gmail configuration
        /*
        $transport = Swift_SmtpTransport::newInstance('gator3246.hostgator.com', 465,'ssl')
          ->setUsername('info@vamosgo.com')
          ->setPassword('VamosGoInfo2016');
        */

        $userName = "info@vamosgo.com";
        $userNombreCompleto = "Información";

        $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465,'ssl')
          ->setUsername('arquitectosec@gmail.com')
          ->setPassword('EcuadoRGO2016');

        // You could alternatively use a different transport such as Sendmail or Mail:
        //Sendmail:
        //$transport = Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -bs');
        //Mail
        //$transport = Swift_MailTransport::newInstance();

        //////////////////////////
        /*$postdata = http_build_query(
            array(
                'c' => $cliente,
                'n' => $userNombreCompleto,
                'u' => $userName,
                'p' => $userPass,
            )
        );*/

        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => ""
            )
        );

        $context  = stream_context_create($opts);

        $cuerpoMail = file_get_contents(base_url("assets/arquitectos/mail/mail-promocion.php"), false, $context);
        //////////////////////////

        //Create the message
        $message = Swift_Message::newInstance();

        //Give the message a subject
        $message->setSubject( $userName . ' :: Bienvenido a Guerrero Ferreccio')
          ->setFrom( array('info@gerreroferrecio.com' => 'Información Vamosarquitectos') )
          ->setTo( array( $userDestinatario => 'Usuario' ) )
          //->setBcc( array( "xavier@agenciazen.com" => "Xavier Luna") )
          ->setBody($cuerpoMail, 'text/html');
          //->addPart('<q>Here is the message sent with swiftmailer</q>', 'text/html');

        //Create the Mailer using your created Transport
        $mailer = Swift_Mailer::newInstance($transport);

        //Send the message
        $result = $mailer->send($message);

        if ($result) {
            $resultMail = array(
                'codigo' => 1,
                'mensaje' => "Correcto: El mensaje ha sido enviado",
            );
        }else{
            $resultMail = array(
                'codigo' => 0,
                'mensaje' => "Error: El mensaje no ha sido enviado",
            );
        }

        // header('Content-Type: application/json');
        // echo json_encode($resultMail);
    }


    function callback_email_activacion($userDestinatario) {
        if (!empty($userDestinatario) && $userDestinatario['estado'] == 1) {
            $this->enviarEmailActivacion( $userDestinatario );
        }
    }



    //////////////////////////////////////////////////////////////



    
}



/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */



//Funcion para validar el registro.
/*
public function username_check($str)
{
  $id = $this->uri->segment(4);
  if(!empty($id) && is_numeric($id))
  {
   $username_old = $this->db->where("id",$id)->get('users')->row()->username;
   $this->db->where("username !=",$username_old);
  }
  
  $num_row = $this->db->where('username',$str)->get('users')->num_rows();
  if ($num_row >= 1)
  {
   $this->form_validation->set_message('username_check', 'The username already exists');
   return FALSE;
  }
  else
  {
   return TRUE;
  }
}
*/