<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_portafolios extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Portafolio');
        $this->load->model('SecurityUser');
        date_default_timezone_set('America/Guayaquil');
    }

    public function crud_portafolios($pagina = FALSE)
    {
        if ($this->security_check_admin()) {
            // Nos permite determinar el índice del registro en la tabla de portafolios
            // a partir del cual recuperaremos los portafolios para la paginación
            $inicio = 0;
            // Nos pemrmite determinar la cantidad de portafolios que mostraremos por página
            $limite = 5;

            // Si el cliente requiere alguna página en específico, calculamos el índice de inicio
            if ($pagina){
                $inicio = ($pagina -1) * $limite; // corrige elemento repetido en ultima página
            }

            // Obtenemos el número de portafolios existentes
            $portafolios_num = $this->Portafolio->count_all_portafolios();

            // Especificamos la configuración de la paginación
            $this->load->library('pagination');
            $config['base_url'] = site_url('admin_portafolios/crud_portafolios');

            $config['use_page_numbers'] = TRUE;
            $config['total_rows'] = $portafolios_num;
            $config['per_page'] = $limite;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = '&raquo';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);

            // Obtenemos las instancias de los portafolios existentes de acuerdo a los parámetros de paginación
            // Recuperamos los registros de portafolios de la DB
            $pagination_portafolios = $this->Portafolio->fetch_portafolios_pagination($inicio, $limite);

            $data_header['titlePage'] = 'Portafolios';
            $data_body['pagination_portafolios'] = $pagination_portafolios;
            $data_body['pagination_links'] = $this->pagination->create_links();
            $data_body['portafolios_num'] = $portafolios_num;

            $this->load->view('arquitectos/blocks/header', $data_header);
            $this->load->view('arquitectos/blocks/menu-admin');
            $this->load->view('arquitectos/admin/crud_portafolios', $data_body);
            $this->load->view('arquitectos/blocks/footer');
        } else {
            redirect('admin/login');
        }
    }

    public function create_portafolio()
    {
        if ($this->security_check_admin()) {

            $data_header['titlePage'] = 'Crear Portafolio';
            $this->load->view('arquitectos/blocks/header', $data_header);
            $this->load->view('arquitectos/blocks/menu-admin');
            $this->load->view('arquitectos/admin/insert_portafolio');
            $this->load->view('arquitectos/blocks/footer');
        } else {
            redirect('admin/login');
        }
    }

    public function read_portafolio($id_portafolio)
    {
        if ($this->security_check_admin()) {
            // Obtenemos la instancia del portafolio
            $portafolio = new Portafolio();
            if ($portafolio->fetch_portafolio_by_id($id_portafolio)) {
                $data_body['portafolio'] = $portafolio;
                $data_header['titlePage'] = 'Visualizar Portafolio';

                $this->load->view('arquitectos/blocks/header', $data_header);
                $this->load->view('arquitectos/blocks/menu-admin');
                $this->load->view('arquitectos/admin/read_portafolio', $data_body);
                $this->load->view('arquitectos/blocks/footer');
            } else {
                redirect('admin_produtos/crud_portafolios');
            }
        } else {
            redirect('admin/login');
        }
    }

    public function update_portafolio($id_portafolio)
    {
        if ($this->security_check_admin()) {
            // Obtenemos la instancia del portafolio
            $portafolio = new Portafolio();
            if ($portafolio->fetch_portafolio_by_id($id_portafolio)) {

                $data_header['titlePage'] = 'Editar Portafolio';
                $data_body['portafolio'] = $portafolio;

                $this->load->view('arquitectos/blocks/header', $data_header);
                $this->load->view('arquitectos/blocks/menu-admin');
                $this->load->view('arquitectos/admin/insert_portafolio', $data_body);
                $this->load->view('arquitectos/blocks/footer');
            } else {
                redirect('admin_produtos/crud_portafolios');
            }
        } else {
            redirect('admin/login');
        }
    }

    public function process_data_portafolio()
    {
        if ($this->security_check_admin()) {
            if ($this->input->post()) {
                $this->form_validation->set_rules('titulo_es', 'Título', 'required|trim', array(
                    'required' => 'Debe escribir un título para el portafolio.'
                ));
                $this->form_validation->set_rules('titulo_en', 'Título en Inglés', 'required|trim', array(
                    'required' => 'Debe escribir un título en Inglés para el portafolio.'
                ));
                $this->form_validation->set_rules('estado', 'Estado', 'required|numeric|in_list[1,0]', array(
                    'required' => 'Debe especificar el estado del portafolio.',
                    'numeric' => 'Error de formato. El campo sólo admite números.',
                    'in_list' => 'Identificador de opción no válido.'
                ));
                $this->form_validation->set_rules('imagen_principal_portafolio', 'Imagen', 'required', array(
                    'required' => 'Debe incluir una foto preview para el portafolio.'
                ));

                if($this->form_validation->run() == true) {
                    $form_data = $this->input->post();

                    // Borramos exceso de espacios vacíos en el string del título en español
                    $titulo_es = $form_data['titulo_es'];
                    $titulo_es = preg_replace('/\s+/', ' ', $titulo_es);

                    // Borramos exceso de espacios vacíos en el string del título en inglés
                    $titulo_en = $form_data['titulo_en'];
                    $titulo_en = preg_replace('/\s+/', ' ', $titulo_en);

                    if (isset($form_data['descripcion_es'])) {
                        // Borramos exceso de espacios vacíos en el string de la descripción en español
                        $descripcion_es = $form_data['descripcion_es'];
                        $descripcion_es = preg_replace('/\s+/', ' ', $descripcion_es);
                    } else {
                        $descripcion_es = '';
                    }

                    if (isset($form_data['descripcion_en'])) {
                        // Borramos exceso de espacios vacíos en el string de la descripción en inglés
                        $descripcion_en = $form_data['descripcion_en'];
                        $descripcion_en = preg_replace('/\s+/', ' ', $descripcion_en);
                    } else {
                        $descripcion_en = '';
                    }

                    if (isset($form_data['id_portafolio'])) {
                        $portafolio = new Portafolio();
                        $portafolio->set_id($form_data['id_portafolio']);
                        $portafolio->set_titulo($titulo_es);
                        $portafolio->set_titulo_en($titulo_en);
                        $portafolio->set_descripcion($descripcion_es);
                        $portafolio->set_descripcion_en($descripcion_en);
                        $portafolio->set_estado($form_data['estado']);
                        $portafolio->set_imagen($form_data['imagen_principal_portafolio']);
                        if (isset($form_data['imagenes_portafolio'])) {
                            foreach ($form_data['imagenes_portafolio'] as $imagen) {
                                $portafolio->add_imagen_portafolio($imagen);
                            }
                        }
                        $portafolio->update_portafolio();

                        $result['error'] = 0;
                    } else {
                        $portafolio_nuevo = new Portafolio();
                        $portafolio_nuevo->set_titulo($titulo_es);
                        $portafolio_nuevo->set_titulo_en($titulo_en);
                        $portafolio_nuevo->set_descripcion($descripcion_es);
                        $portafolio_nuevo->set_descripcion_en($descripcion_en);
                        $portafolio_nuevo->set_estado($form_data['estado']);
                        $portafolio_nuevo->set_imagen($form_data['imagen_principal_portafolio']);
                        if (isset($form_data['imagenes_portafolio'])) {
                            foreach ($form_data['imagenes_portafolio'] as $imagen) {
                                $portafolio_nuevo->add_imagen_portafolio($imagen);
                            }
                        }
                        $portafolio_nuevo->save_new_portafolio();

                        $result['error'] = 0;
                    }
                } else {
                    $result['error'] = 2;
                    $result['messages'] = $this->form_validation->error_array();
                }
            } else {
                $result['error'] = 1;
                $result['message'] = 'Error. No ha enviado ningún dato.';
            }
            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function delete_portafolio()
    {
        if ($this->security_check_admin()) {
            $id_portafolio = $this->input->post('id_portafolio');
            if($this->Portafolio->delete_portafolio($id_portafolio)) {
                $result['error'] = 0;
            } else {
                $result['error'] = 1;
                $result['message'] = 'Error. No se pudo eliminar el portafolio.';
            }
            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function upload_imagenes_portafolio()
    {
        if ($this->security_check_admin()) {
            $this->load->library('upload');

            if (isset($_FILES['imagenes_portafolio'])) {
                $number_of_images_uploaded = count($_FILES['imagenes_portafolio']['name']);

                $result['error'] = 0;
                $result['upload_data'] = array();

                for ($i = 0; $i < $number_of_images_uploaded; $i++) {
                    $_FILES['userfile']['name']     = $_FILES['imagenes_portafolio']['name'][$i];
                    $_FILES['userfile']['type']     = $_FILES['imagenes_portafolio']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $_FILES['imagenes_portafolio']['tmp_name'][$i];
                    $_FILES['userfile']['error']    = $_FILES['imagenes_portafolio']['error'][$i];
                    $_FILES['userfile']['size']     = $_FILES['imagenes_portafolio']['size'][$i];

                    $config = array(
                        'file_name' => $this->generate_token() . '_' . $_FILES['userfile']['name'],
                        'allowed_types' => 'jpg|jpeg|png|gif',
                        'max_size' => 3000,
                        'overwrite' => FALSE,
                        'upload_path' => 'assets/uploads/portfolio_imagenes/'
                    );
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload()) {
                        $result['error'] = 1;
                    } else {
                        array_push($result['upload_data'], $this->upload->data());
                    }
                }
            } else {
                $config = array(
                    'encrypt_name' => TRUE,
                    'allowed_types' => 'jpg|jpeg|png|gif',
                    'max_size' => 3000,
                    'overwrite' => FALSE,
                    'upload_path' => 'assets/uploads/portfolio'
                );
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('imagen_principal')) {
                    $result['error'] = 1;
                    $result['messages'] = $this->upload->display_errors('<span>', '</span>');
                } else {
                    $result['error'] = 0;
                    $result['upload_data'] = $this->upload->data();
                }
            }
            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    public function delete_imagen_portafolio()
    {
        if ($this->security_check_admin()) {
            $post_img_name = $this->input->post('img_name');
            $post_img_type = $this->input->post('type');
            if (!is_null($post_img_name)) {
                $folder = '';
                if (is_null($post_img_type)) {
                    $folder = 'portfolio_imagenes';
                } else {
                    $folder = 'portfolio';
                }

                $post_img_raw_name = $this->input->post('img_raw_name');

                $this->load->helper('file');
                $file_path = FCPATH . 'assets/uploads/' . $folder . '/' . $post_img_name;
                $file_path = str_replace('\\', '/', $file_path);
                if (file_exists($file_path)) {
                    unlink($file_path);
                }
                $result['error'] = 0;
                $result['deleted_image'] = $post_img_name;
                $result['img_raw_name'] = $post_img_raw_name;
            } else {
                $result['error'] = 1;
                $result['message'] = 'Error. No se ha especificado una imagen';
            }
            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }

    private function generate_token ($len = 15)
    {
        // Array con los potenciales caracteres que puede tener el token, el cual vamos a desordenar
        $chars = array(
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
        );
        shuffle($chars);

        $num_chars = count($chars) - 1;
        $token = '';
        // Creamos un token aleatorio del tamaño especificado
        for ($i = 0; $i < $len; $i++)
        {
            $token .= $chars[mt_rand(0, $num_chars)];
        }
        return $token;
    }

    private function security_check_admin() {
        $securityUser = new SecurityUser();
        $usuario = $this->session->userdata('user_login');
        if ($usuario == '') {
            return false;
        } else {
            return true;
        }
    }
}