<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller{
	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, OPTIONS");


        $this->load->database();
        $this->load->model('SecurityUser');
        

        date_default_timezone_set("America/Guayaquil");
    }

    function switchLanguage($language = "") {
        $language = ($language != "") ? $language : "spanish";
        $this->session->set_userdata('site_lang', $language);
        redirect(base_url());
    }
    
	public function index(){
		$dataHeader['title'] = "Ferrecio::Home";

		$this->db->select("*");
        $this->db->from('servicio AS servicios');
        $this->db->where( array('servicios.estado' => 1) );

        $dataContent['servicios'] = $this->db->get()->result_array();

        $this->db->select("*");
        $this->db->from('footer AS footers');
        $this->db->where( array('footers.estado' => 1,'footers.seccion' => 1) );

        $dataContent['footers'] = $this->db->get()->result_array();
        
        $this->db->select("*");
        $this->db->from('estudio AS estudios');
        $this->db->where( array('estudios.estado' => 1) );

        $dataContent['estudios'] = $this->db->get()->result_array();

        $this->db->select("*");
        $this->db->from('slideshow AS slideshows');
        $this->db->where( array('slideshows.estado' => 1) );

        $dataContent['slideshows'] = $this->db->get()->result_array();

        $this->db->select("*");
        $this->db->from('portafolio AS portafolios');
        $this->db->where( array('portafolios.estado' => 1) );
        $this->db->limit( 4 );

        $dataContent['portafolios'] = $this->db->get()->result_array();


        $this->db->select("*");
        $this->db->from('audio AS a');
        $this->db->where( array('a.estado' => 1) );
        $this->db->order_by('id', 'RANDOM');
        $audiosData = $this->db->get()->result_array();
        $audioData = $audiosData[0];
        $dataHeader['audio'] = array(
            'id' => $audioData["id"],
            'nombre' => $audioData["nombre"],
            'archivo' => base_url('assets/uploads/audio') . "/" . $audioData["archivo"]
        );


		$this->load->view('arquitectos/web/templates/header', $dataHeader);
		$this->load->view('arquitectos/web/pages/index',  $dataContent );
		$this->load->view('arquitectos/web/templates/footer');
	}

	public function studio(){
		$dataHeader['title'] = "Ferreccio::Studio";
		
		$this->db->select("*");
        $this->db->from('estudio AS estudio');
        $this->db->where( array('estudio.estado' => 1) );

        $dataContent['estudios'] = $this->db->get()->result_array();

        $this->db->select("*");
        $this->db->from('asociado AS asociados');
        $this->db->where( array('asociados.estado' => 1) );

        $dataContent['asociados'] = $this->db->get()->result_array();

        $this->db->select("*");
        $this->db->from('footer AS footers');
        $this->db->where( array('footers.estado' => 1,'footers.seccion' => 3) );

        $dataContent['footers'] = $this->db->get()->result_array();

        $this->db->select("*");
        $this->db->from('audio AS a');
        $this->db->where( array('a.estado' => 1) );
        $this->db->order_by('id', 'RANDOM');
        $audiosData = $this->db->get()->result_array();
        $audioData = $audiosData[0];
        $dataHeader['audio'] = array(
            'id' => $audioData["id"],
            'nombre' => $audioData["nombre"],
            'archivo' => base_url('assets/uploads/audio') . "/" . $audioData["archivo"]
        );

		$this->load->view('arquitectos/web/templates/header', $dataHeader);
		$this->load->view('arquitectos/web/pages/studio',  $dataContent );
		$this->load->view('arquitectos/web/templates/footer');
	}

	public function portfolio(){
		$dataHeader['title'] = "Ferreccio::Portfolio";

		$this->db->select("*");
        $this->db->from('portafolio AS portafolios');
        $this->db->where( array('portafolios.estado' => 1) );
        $portafoliosDB = $this->db->get()->result_array();

        $portafoliosTotal = array();
        foreach ($portafoliosDB as $itemPortafolioDB) {
            $itemPortafolioSlidesDB = $this->db->get_where('imagenes_portafolio', array('id_portafolio' => $itemPortafolioDB['id']))->result_array();

            $itemPortafolio = array(
                'id' => $itemPortafolioDB['id'],
                'titulo' => $itemPortafolioDB['titulo'],
                'titulo_en' => $itemPortafolioDB['titulo_en'],
                'descripcion' => $itemPortafolioDB['descripcion'],
                'descripcion_en' => $itemPortafolioDB['descripcion_en'],
                'imagen' => $itemPortafolioDB['imagen'],
                'imagenes' => $itemPortafolioSlidesDB,
            );

            array_push($portafoliosTotal, $itemPortafolio);
        }

        $dataContent['portafolios'] = $portafoliosTotal;

        $this->db->select("*");
        $this->db->from('footer AS footers');
        $this->db->where( array('footers.estado' => 1,'footers.seccion' => 1) );

        $dataContent['footers'] = $this->db->get()->result_array();


        $this->db->select("*");
        $this->db->from('audio AS a');
        $this->db->where( array('a.estado' => 1) );
        $this->db->order_by('id', 'RANDOM');
        $audiosData = $this->db->get()->result_array();
        $audioData = $audiosData[0];
        $dataHeader['audio'] = array(
            'id' => $audioData["id"],
            'nombre' => $audioData["nombre"],
            'archivo' => base_url('assets/uploads/audio') . "/" . $audioData["archivo"]
        );

		$this->load->view('arquitectos/web/templates/header', $dataHeader);
		$this->load->view('arquitectos/web/pages/portfolio',  $dataContent );
		$this->load->view('arquitectos/web/templates/footer');
	}

	public function services(){
		$dataHeader['title'] = "Ferreccio::Services";

		$this->db->select("*");
        $this->db->from('footer AS footers');
        $this->db->where( array('footers.estado' => 1,'footers.seccion' => 2) );

        $dataContent['footers'] = $this->db->get()->result_array();
		
		$this->db->select("*");
        $this->db->from('servicio AS servicios');
        $this->db->where( array('servicios.estado' => 1) );

        $dataContent['servicios'] = $this->db->get()->result_array();


		$this->db->select("*");
        $this->db->from('razones AS razones');
        $this->db->where( array('razones.estado' => 1) );

        $dataContent['razones'] = $this->db->get()->result_array();


        $this->db->select("*");
        $this->db->from('audio AS a');
        $this->db->where( array('a.estado' => 1) );
        $this->db->order_by('id', 'RANDOM');
        $audiosData = $this->db->get()->result_array();
        $audioData = $audiosData[0];
        $dataHeader['audio'] = array(
            'id' => $audioData["id"],
            'nombre' => $audioData["nombre"],
            'archivo' => base_url('assets/uploads/audio') . "/" . $audioData["archivo"]
        );


		$this->load->view('arquitectos/web/templates/header', $dataHeader);
		$this->load->view('arquitectos/web/pages/services',  $dataContent);
		$this->load->view('arquitectos/web/templates/footer');
	}

	public function contact(){
		$dataHeader['title'] = "Ferreccio::Contactos";

		$this->db->select("*");
        $this->db->from('footer AS footers');
        $this->db->where( array('footers.estado' => 1,'footers.seccion' => 1) );

        $dataContent['footers'] = $this->db->get()->result_array();

        $this->db->select("*");
        $this->db->from('local AS locales');
        $this->db->where( array('locales.estado' => 1) );

        $dataContent['locales'] = $this->db->get()->result_array();



        $this->db->select("*");
        $this->db->from('audio AS a');
        $this->db->where( array('a.estado' => 1) );
        $this->db->order_by('id', 'RANDOM');
        $audiosData = $this->db->get()->result_array();
        $audioData = $audiosData[0];
        $dataHeader['audio'] = array(
            'id' => $audioData["id"],
            'nombre' => $audioData["nombre"],
            'archivo' => base_url('assets/uploads/audio') . "/" . $audioData["archivo"]
        );


		$this->load->view('arquitectos/web/templates/header', $dataHeader);
		$this->load->view('arquitectos/web/pages/contact',  $dataContent);
		$this->load->view('arquitectos/web/templates/footer');
	}

    public function enviarContactoWeb() {

        $nombre = $this->input->post("nombre");
        $email = $this->input->post("email");
        $mensaje = $this->input->post("mensaje");
        // $captcha = $this->input->post("g-recaptcha-response");

        if ($nombre && $email && $mensaje) {
            $destinatariosData = array(
                // "to" => array( $nombre => $email ),
                // "bcc" => array( "xavier@vamosgo.com", "mario@cajanegra.com.ec" )
            );
            $mailTemplateURL = "assets/mailings/mail-contacto.php";
            $mailAsunto = "Nuevo contacto desde Web :: " . $nombre . "(" . $email . ")";
            $mailPostData = array(
                'userNombre' => $nombre,
                'userMensaje' => $mensaje,
                'userMail' => $email,
            );

            try {
                /////////////////////////////////////////////////////////////////////////
                // Preparación de correo electrónico
                $this->load->library('email');

                $correo_config = $this->db->get_where('correo_config', array('estado'=>1))->row();
                $config['protocol']  = 'smtp';
                $config['smtp_host'] = $correo_config->servidor_correo; // URL DEL HOST SMTP
                $config['smtp_port'] = $correo_config->puerto; // PUERTO SMTP DEL CLIENTE (va sin comillas)
                $config['smtp_user'] = $correo_config->email; // CORREO SMTP
                $config['smtp_pass'] = $correo_config->password; // CONTRASEÑA DEL EMAIL
                $config['charset']   = 'utf-8';
                $config['mailtype']  = 'html';

                $this->email->initialize($config);
                $this->email->set_newline("\r\n");
                $this->email->set_crlf("\r\n");

                $opts = array(
                    'http' => array(
                            'method'  => 'POST',
                            'header'  => 'Content-type: application/x-www-form-urlencoded',
                            'content' => http_build_query( $mailPostData )
                        )
                );
                $cuerpoMail = file_get_contents( base_url( $mailTemplateURL ), false, stream_context_create( $opts ) );

                //$this->email->from('info@arquitectos.com', 'Información Arquitectos');
                //$this->email->to("xavier@vamosgo.com");
                $this->email->from('info@guerreroferreccio.com', 'Información Arquitectos');
                $this->email->to("carlos@guerreroferreccio.com");
                if( isset( $destinatariosData["cc"] ) && !empty( $destinatariosData["cc"] ) ){
                    $this->email->cc( $destinatariosData["cc"] );
                }
                if( isset( $destinatariosData["bcc"] ) && !empty( $destinatariosData["bcc"] ) ){
                    $this->email->bcc( $destinatariosData["bcc"] );
                }
                $this->email->subject( $mailAsunto );
                $this->email->message( $mensaje );

                if( $this->email->send() ){
                    $result = TRUE;
                    // return TRUE;
                }else{
                    $result = FALSE;
                    // show_error($this->email->print_debugger());
                    // return FALSE;
                }
                /////////////////////////////////////////////////////////////////////////
            } catch (Exception $e) {
                log_message( 'error', $e->getTraceAsString() );
                $result = FALSE;
                return false;
            }

            $result = $this->enviarMailConTemplate( $destinatariosData, $mailTemplateURL, $mailAsunto, $mailPostData );

            if ($result) {
                $resultMail = array(
                    'codigo' => 1,
                    'mensaje' => "Correcto: El mensaje ha sido enviado",
                );
                // redirect('/','refresh');
            }else{
                $resultMail = array(
                    'codigo' => 0,
                    'mensaje' => "Error: El mensaje no ha sido enviado",
                );
                // redirect('/','refresh');
            }
        } else {
            $resultMail = array(
                'codigo' => 0,
                'mensaje' => "Error: Faltan campos",
                'campos' => "N " . $nombre . "M " . $email . "Me" . $mensaje,
            );
            // redirect('/','refresh');
        }
        header('Content-type: application/json');
        echo json_encode($resultMail);

    }
    
    //////////////////////////////////////////////////////////
    // Envío de mail
    //////////////////////////////////////////////////////////
    public function enviarMailConTemplate( $destinatariosData, $mailTemplateURL, $mailAsunto, $mailPostData ){
        try {
            // Obtener instancia de CodeIgniter
            $instanciaCI =& get_instance();

            /////////////////////////////////////////////////////////////////////////
            // Preparación de correo electrónico
            $instanciaCI->load->library('email');

            $correo_config = $instanciaCI->db->get_where('correo_config', array('estado'=>1))->row();
            $config['protocol']  = 'smtp';
            $config['smtp_host'] = $correo_config->servidor_correo; // URL DEL HOST SMTP
            $config['smtp_port'] = $correo_config->puerto; // PUERTO SMTP DEL CLIENTE (va sin comillas)
            $config['smtp_user'] = $correo_config->email; // CORREO SMTP
            $config['smtp_pass'] = $correo_config->password; // CONTRASEÑA DEL EMAIL
            $config['charset']   = 'utf-8';
            $config['mailtype']  = 'html';

            $instanciaCI->email->initialize($config);
            $instanciaCI->email->set_newline("\r\n");
            $instanciaCI->email->set_crlf("\r\n");

            $opts = array(
                'http' => array(
                        'method'  => 'POST',
                        'header'  => 'Content-type: application/x-www-form-urlencoded',
                        'content' => http_build_query( $mailPostData )
                    )
            );
            $cuerpoMail = file_get_contents( base_url( $mailTemplateURL ), false, stream_context_create( $opts ) );

            $instanciaCI->email->from('info@guerreroferreccio.com', 'Información Arquitectos');
            $instanciaCI->email->to('carlos@guerreroferreccio.com');
            if( isset( $destinatariosData["cc"] ) && !empty( $destinatariosData["cc"] ) ){
                $instanciaCI->email->cc( $destinatariosData["cc"] );
            }
            if( isset( $destinatariosData["bcc"] ) && !empty( $destinatariosData["bcc"] ) ){
                $instanciaCI->email->bcc( $destinatariosData["bcc"] );
            }
            $instanciaCI->email->subject( $mailAsunto );
            $instanciaCI->email->message( $cuerpoMail );

            if( $instanciaCI->email->send() ){
                return TRUE;
            }else{
                show_error($instanciaCI->email->print_debugger());
                return FALSE;
            }
            /////////////////////////////////////////////////////////////////////////
        } catch (Exception $e) {
            log_message( 'error', $e->getTraceAsString() );
            return false;
        }
    }
}
