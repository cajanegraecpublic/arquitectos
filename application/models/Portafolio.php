<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    /*
        Tabla:
            portafolio
        Campos:
            id (Primary Key)
            titulo (varchar)
            titulo_en (varchar)
            descripcion (text)
            descripcion_en (text)
            imagen (varchar)
            estado (tinyint)
    */

    class Portafolio extends CI_Model
    {
        private $id;
        private $titulo;
        private $titulo_en;
        private $descripcion;
        private $descripcion_en;
        private $imagen;
        private $imagenes_portafolio;
        private $estado;


        function __construct() {
            parent::__construct();

            // Helpers
            $this->load->database();

            $this->imagenes_portafolio = array();
        }

        ///////////////////////////////////
        // Getters
        ///////////////////////////////////
        public function get_id()
        {
            return $this->id;
        }

        public function get_titulo()
        {
            return $this->titulo;
        }

        public function get_titulo_en()
        {
            return $this->titulo_en;
        }

        public function get_descripcion()
        {
            return $this->descripcion;
        }

        public function get_descripcion_en()
        {
            return $this->descripcion_en;
        }

        public function get_imagen()
        {
            return $this->imagen;
        }

        public function get_imagenes_portafolio()
        {
            return $this->imagenes_portafolio;
        }

        public function get_estado()
        {
            return $this->estado;
        }

        ///////////////////////////////////
        // Setters
        ///////////////////////////////////
        public function set_id($id)
        {
            $this->id = $id;
        }

        public function set_titulo($titulo)
        {
            $this->titulo = $titulo;
        }

        public function set_titulo_en($titulo_en)
        {
            $this->titulo_en = $titulo_en;
        }

        public function set_descripcion($descripcion)
        {
            $this->descripcion = $descripcion;
        }

        public function set_descripcion_en($descripcion_en)
        {
            $this->descripcion_en = $descripcion_en;
        }

        public function set_imagen($imagen)
        {
            $this->imagen = $imagen;
        }

        public function set_imagenes_portafolio($imagenes_portafolio)
        {
            $this->imagenes_portafolio = $imagenes_portafolio;
        }

        public function set_estado($estado)
        {
            $this->estado = $estado;
        }


        ///////////////////////////////////
        // Métodos
        ///////////////////////////////////
        // Metodo para recuperar un portafolio de la DB usando el ID
        public function fetch_portafolio_by_id($portafolio_id)
        {
            if (!is_null($portafolio_id)) {
                // Validamos que el ID de portafolio proporcionado sea valido
                if ($this->portafolio_id_exists($portafolio_id)) {
                    // Obtentemos el portafolio de la DB
                    $portafolio_db = $this->db->get_where('portafolio', array('id' => $portafolio_id))->last_row();

                    // Guardamos en la instancia los datos de portafolio traidos de la DB
                    $this->id = $portafolio_db->id;
                    $this->titulo = $portafolio_db->titulo;
                    $this->titulo_en = $portafolio_db->titulo_en;
                    $this->descripcion = $portafolio_db->descripcion;
                    $this->descripcion_en = $portafolio_db->descripcion;
                    $this->imagen = $portafolio_db->imagen;
                    $this->estado = $portafolio_db->estado;

                    // Guardamos en los registros de todas las imagenes asociadas al portafolio
                    $imagenes_db = $this->db->get_where('imagenes_portafolio', array('id_portafolio' => $portafolio_id))->result();
                    if (!empty($imagenes_db)) {
                        foreach ($imagenes_db as $imagen) {
                            array_push($this->imagenes_portafolio, $imagen->nombre_archivo);
                        }
                    }

                    return true;
                } else {
                    return false;
                }
            } else {
                return null;
            }
        }

        // Funcion para comprobar si un Id de portafolio existe en la DB
        public function portafolio_id_exists($portafolio_id)
        {
            if (!is_null($portafolio_id)) {
                // Intentamos obtener el portafolio de la DB
                $portafolio_db = $this->db->get_where('portafolio', array('id' => $portafolio_id))->last_row();

                // Validamos si se pudo obtener un portafolio con el ID proporcionado
                if (!is_null($portafolio_db)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return null;
            }
        }

        // Métoddo para añadir una imagen nueva al portafolio
        public function add_imagen_portafolio($imagen_portafolio)
        {
            array_push($this->imagenes_portafolio, $imagen_portafolio);
        }

        // Método para guardar un nuevo portafolio
        public function save_new_portafolio()
        {
            $this->id = $this->get_last_portafolio_id() + 1;
            $this->db->insert('portafolio', array(
                'id' => $this->id,
                'titulo' => $this->titulo,
                'titulo_en' => $this->titulo_en,
                'descripcion' => $this->descripcion,
                'descripcion_en' => $this->descripcion_en,
                'imagen' => $this->imagen,
                'estado' => $this->estado
            ));
            if (!empty($this->imagenes_portafolio)) {
                foreach ($this->imagenes_portafolio as $imagen) {
                    $this->db->insert('imagenes_portafolio', array(
                        'id_portafolio' => $this->id,
                        'nombre_archivo' => $imagen
                    ));
                }
            }
        }

        // Método para actualizar un portafolio ya existente
        public function update_portafolio()
        {
            $this->db->replace('portafolio', array(
                'id' => $this->id,
                'titulo' => $this->titulo,
                'titulo_en' => $this->titulo_en,
                'descripcion' => $this->descripcion,
                'descripcion_en' => $this->descripcion_en,
                'imagen' => $this->imagen,
                'estado' => $this->estado
            ));
            $this->db->delete('imagenes_portafolio', array(
                'id_portafolio' => $this->id
            ));
            if (!empty($this->imagenes_portafolio)) {
                foreach ($this->imagenes_portafolio as $imagen) {
                    $this->db->insert('imagenes_portafolio', array(
                        'id_portafolio' => $this->id,
                        'nombre_archivo' => $imagen
                    ));
                }
            }
        }

        // Método para contar los portafolios existentes
        public static function count_all_portafolios()
        {
            // Obtener instancia de CodeIgniter para manejo de la DB
            $instancia_ci =& get_instance();

            $instancia_ci->db->select('*');
            $instancia_ci->db->from('portafolio');
            return $instancia_ci->db->count_all_results();
        }

        // Devuelve un array con los portafolios según los parametros de paginación
        public static function fetch_portafolios_pagination($start, $limit)
        {
            // Obtener instancia de CodeIgniter para manejo de la DB
            $instancia_ci =& get_instance();

            // Creamos el arreglo donde guardaremos los portafolios
            $portafolios_arr = array();
            // Recuperamos los registros de portafolios de la DB
            $instancia_ci->db->select('id');
            $instancia_ci->db->from('portafolio');
            $instancia_ci->db->limit($limit, $start);
            $portafolios_db = $instancia_ci->db->get()->result();
            // Creamos las instancias de los portafolios y los metemos en el arreglo
            foreach ($portafolios_db as $row) {
                $portafolio = new Portafolio();
                $portafolio->fetch_portafolio_by_id($row->id);
                array_push($portafolios_arr, $portafolio);
            }

            return $portafolios_arr;
        }

        // Método para obtener el ID del útlimo portafolio insertado en la DB
        public static function get_last_portafolio_id()
        {
            // Obtener instancia de CodeIgniter para manejo de la DB
            $instancia_ci =& get_instance();

            $instancia_ci->db->select('id');
            $instancia_ci->db->from('portafolio');
            $instancia_ci->db->order_by('id', 'DESC');
            $instancia_ci->db->limit(1);
            $result = $instancia_ci->db->get()->row();
            if (!is_null($result)) {
                $last_id = $result->id;
            } else {
                $last_id = null;
            }

            return $last_id;
        }

        // Método para eliminar un portafolio dado su ID
        public static function delete_portafolio($id_portafolio)
        {
            if (!is_null($id_portafolio)) {
                // Obtener instancia de CodeIgniter para manejo de la DB
                $instancia_ci =& get_instance();

                $instancia_ci->load->helper('file');

                // Borramos del servidor la imagen principal del portafolio
                $imagen_portafolio_db = $instancia_ci->db->get_where('portafolio', array('id' => $id_portafolio))->row();
                $file_path = FCPATH . 'assets/uploads/portfolio/' . $imagen_portafolio_db->imagen;
                $file_path = str_replace('\\', '/', $file_path);
                if (file_exists($file_path)) {
                    unlink($file_path);
                }

                // Borramos del servidor todas las imágenes asociadas al portafolio
                $imagenes_portafolio_db = $instancia_ci->db->get_where('imagenes_portafolio', array('id_portafolio' => $id_portafolio))->result();
                if (!empty($imagenes_portafolio_db)) {
                    foreach ($imagenes_portafolio_db as $imagen) {
                        $file_path = FCPATH . 'assets/uploads/portfolio_imagenes/' . $imagen->nombre_archivo;
                        $file_path = str_replace('\\', '/', $file_path);
                        if (file_exists($file_path)) {
                            unlink($file_path);
                        }
                    }

                    // Borramos de la DB los registros de las imágenes asociadas al portafolio
                    $instancia_ci->db->delete('imagenes_portafolio', array('id_portafolio' => $id_portafolio));
                }

                // Borramos de la DB el registro del portafolio
                $instancia_ci->db->delete('portafolio', array('id' => $id_portafolio));

                return true;
            } else {
                return null;
            }
        }

        // Método que devuelve todos los portafolios activos
        public static function fetch_all_portafolios_activos ()
        {
            // Obtener instancia de CodeIgniter para manejo de la DB
            $instancia_ci =& get_instance();

            $instancia_ci->db->select('id');
            $instancia_ci->db->from('portafolio');
            $instancia_ci->db->where(array(
                'estado' => 1
            ));
            $instancia_ci->db->order_by('id', 'DESC');
            $portafolios_activos_db = $instancia_ci->db->get()->result();

            $portafolios_activos_arr = array();

            if (!empty($portafolios_activos_db)) {
                foreach ($portafolios_activos_db as $portafolio_db) {
                    $portafolio = new Portafolio();
                    $portafolio->fetch_portafolio_by_id($portafolio_db->id);
                    array_push($portafolios_activos_arr, $portafolio);
                }
            }

            return $portafolios_activos_arr;
        }
    }
?>