		<div class="col-lg-10 col-sm-10">
			<div class="row">
				<div class="col-md-12">
					<div class="box-inner">
						<?php if ( isset($arrClientes) ): ?>
							<div class="box-header well" data-original-title="">
								<h2>
									<i class="glyphicon glyphicon-pushpin"></i>&nbsp;&nbsp;Asignación de Contenidos
								</h2>
							</div>
							<div class="box-content">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="control-group">
                                                <label class="control-label" for="selectAdminCliente">Cliente: </label>
                                                <div class="controls">
                                                    <select id="selectAdminCliente" name="selectAdminCliente" class="form-control input-xlarge" placeholder="Seleccionar Cliente">
                                                        <option value="">Seleccionar cliente</option>
                                                        <?php foreach ($arrClientes as $cliente): ?>
                                                            <option value="<?php echo $cliente['id']; ?>"><?php echo $cliente["nombre"]; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3" style="display:none;">
                                            <div class="control-group">
                                                <label class="control-label" for="selectAdminPlantilla">Plantilla: </label>
                                                <div class="controls">
                                                    <select id="selectAdminPlantilla" name="selectAdminPlantilla" class="form-control input-xlarge" placeholder="Seleccionar Plantilla">
                                                        <option value="">Seleccionar plantilla</option>
                                                        <?php foreach ($arrPlantillas as $plantilla): ?>
                                                            <option value="<?php echo $plantilla['id']; ?>"><?php echo $plantilla["nombre"]; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3" style="display:none;">
                                            <div class="control-group">
                                                <label class="control-label" for="selectAdminGrupo">Grupo: </label>
                                                <div class="controls">
                                                    <select id="selectAdminGrupo" name="selectAdminGrupo" class="form-control input-xlarge" placeholder="Seleccionar Local">
                                                        <!-- Generado -->
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3" style="display:none;">
                                            <div class="control-group">
                                                <label class="control-label" for="inputNombreNuevo">Nombre Grupo Nuevo: </label>
                                                <div class="controls">
                                                   <input type="text" id="nombre_nuevo" name="inputNombreNuevo" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <div class="container-fluid" id="divisionesContenidos">
                                </div>
                                <div class="clearfix"></div>
                            </div>
						<?php else: ?>
							<div class="box-header well" data-original-title="">
								<h2>
									<i class="glyphicon glyphicon-pushpin"></i>&nbsp;&nbsp;Atención
								</h2>
							</div>
							<div class="box-content text-center">
								<h3>No se han encontrado contenidos.</h3>
							</div>
						<?php endif ?>
					</div>
				</div>
			</div><!--/row-->
		</div>
	</div>
</div> <!-- inicio ch-container -->