		<div class="col-lg-10 col-sm-10">
			<div class="row">
				<div class="col-md-12">
					<div class="box-inner">
						<?php if ( isset($promosActivas) && ($promosActivas > 0) ): ?>
							<div class="box-header well" data-original-title="">
								<h2>
									<i class="glyphicon glyphicon-folder-open"></i>&nbsp;&nbsp;<?php echo $titlePage; ?>
								</h2>
							</div>
							<div class="box-content">
								<div class="contenedor-mapa">
									<iframe id="mapaIframe" src="<?php echo site_url('admin/mapaCompleto'); ?>"></iframe>
								</div>
							</div>
						<?php else: ?>
							<div class="box-header well" data-original-title="">
								<h2>
									<i class="glyphicon glyphicon-warning-sign"></i>&nbsp;&nbsp;Atención
								</h2>
							</div>
							<div class="box-content text-center">
								<h3>No se encontró contenido</h3>
							</div>
						<?php endif ?>
					</div>
				</div>
			</div><!--/row-->
		</div>
	</div>
</div> <!-- inicio ch-container -->