        <div class="col-lg-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <div class="box-inner">
                        <?php if (isset($cronogramas)): ?>
                            <div class="box-header well" data-original-title="">
                                <h2>
                                    <i class="glyphicon glyphicon-calendar"></i> <?php echo $titlePage; ?>
                                </h2>
                            </div>
                            <div class="box-content">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="control-group">
                                                <label class="control-label" for="selectCliente">Cliente: </label>
                                                <div class="controls">
                                                    <select id="selectCliente" name="selectCliente" class="form-control input-xlarge" placeholder="Seleccionar Cliente">
                                                        <option value="">Seleccionar cliente</option>
                                                        <?php foreach ($clientes as $cliente): ?>
                                                            <option value="<?php echo $cliente['id']; ?>"><?php echo $cliente["nombre"]; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="control-group">
                                                <label class="control-label" for="selectLocal">Local: </label>
                                                <div class="controls">
                                                    <select id="selectLocal" name="selectLocal" class="form-control input-xlarge" placeholder="Seleccionar Local">
                                                        <!-- Generado -->
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="control-group">
                                                <label class="control-label" for="selectDispositivo">Dispositivos: </label>
                                                <div class="controls">
                                                    <select id="selectDispositivo" name="selectDispositivo" class="form-control input-xlarge" placeholder="Seleccionar Local">
                                                        <!-- Generado -->
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <div id="calendario"></div>
                                <div class="clearfix"></div>
                            </div>
                        <?php else: ?>
                            <div class="box-header well" data-original-title="">
                                <h2>
                                    <i class="glyphicon glyphicon-warning-sign"></i>&nbsp;&nbsp;Atención
                                </h2>
                            </div>
                            <div class="box-content text-center">
                                <h3>No se encontró contenido cronogramas activos</h3>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div><!--/row-->
        </div>
    </div>
</div> <!-- inicio ch-container -->