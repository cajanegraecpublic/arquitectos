        <?php foreach ($pagination_portafolios as $portafolio): ?>
            <div class="modal fade" id="imagen_portafolio<?php echo $portafolio->get_id(); ?>" tabindex="-1" role="dialog">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  </div>
                  <div class="modal-body">
                    <img class="img-responsive img-center" src="<?php echo base_url('assets/uploads/portfolio/' . $portafolio->get_imagen()); ?>">
                  </div>
                </div>
              </div>
            </div>
        <?php endforeach; ?>
        
        <!-- Page Content -->
        <div class="col-lg-10 col-sm-10">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2>
                        <i class="glyphicon glyphicon-folder-open"></i>&nbsp;&nbsp;Portafolios
                    </h2>
                </div>
                <div class="box-content pr-0 pl-0 pt-0">
                    <div class="crud-header">
                        <a class="btn btn-default" href="<?php echo base_url('index.php/admin_portafolios/create_portafolio'); ?>"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Añadir Portafolio</a>
                    </div>
                    <div class="crud-body">
                        <table id="crud-portafolios" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Título (ES)</th>
                                    <th>Título (EN)</th>
                                    <th class="imagen-principal-th">Imagen Principal</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($pagination_portafolios as $portafolio): ?>
                                    <tr>
                                        <td><?php echo $portafolio->get_titulo(); ?></td>
                                        <td><?php echo $portafolio->get_titulo_en(); ?></td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#imagen_portafolio<?php echo $portafolio->get_id(); ?>">
                                                <img class="imagen-principal-thumb" src="<?php echo base_url('assets/uploads/portfolio/' . $portafolio->get_imagen()); ?>">
                                            </a>
                                        </td>
                                        <td>
                                            <?php echo ($portafolio->get_estado() == 1)  ? 'Sí' : 'No' ; ?>
                                        </td>
                                        <td class="acciones-td fit">
                                            <a href="<?php echo base_url('index.php/admin_portafolios/update_portafolio/' . $portafolio->get_id()); ?>" class="btn btn-default"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Editar</a>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                    Más <span class="caret"></span></button>
                                                <ul class="dropdown-menu crud-acciones-dropdown-menu" role="menu">
                                                    <li><a href="<?php echo base_url('index.php/admin_portafolios/read_portafolio/' . $portafolio->get_id()); ?>"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Ver</a></li>
                                                    <li><button data-id-portafolio="<?php echo $portafolio->get_id(); ?>" class="red delete-portafolio"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Borrar</button></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-10 col-sm-offset-2 text-center">
            <?php echo $pagination_links; ?>
        </div>