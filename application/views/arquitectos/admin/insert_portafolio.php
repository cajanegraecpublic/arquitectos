        <!-- Page Content -->
        <div class="col-lg-10 col-sm-10">
            <div class="row">
                <div class="box-header well" data-original-title="">
                    <h2>
                        <i class="glyphicon glyphicon-folder-open"></i>&nbsp;&nbsp;Portafolios
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 pl-0 pr-0">
                            <div class="form-label pl-5 pb-5 pt-5 form-label">Añadir Portafolios</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 form-container">
                            <?php echo form_open('' , array('id' => 'form-create-portafolio', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')); ?>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="titulo_es">Título*:</label>
                                    <div class="col-sm-6">
                                        <?php if (!isset($portafolio)): ?>
                                            <input type="text" class="form-control" id="titulo_es" name="titulo_es">
                                        <?php else: ?>
                                            <input type="text" class="form-control" id="titulo_es" name="titulo_es" value="<?php echo $portafolio->get_titulo(); ?>">
                                        <?php endif; ?>
                                    </div>
                                    <label class="error-label col-sm-3 pt-xs-10" data-error-input="titulo_es"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <span class="error-text"></span></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="titulo_en">Título en Inglés*:</label>
                                    <div class="col-sm-6">
                                        <?php if (!isset($portafolio)): ?>
                                            <input type="text" class="form-control" id="titulo_en" name="titulo_en">
                                        <?php else: ?>
                                            <input type="text" class="form-control" id="titulo_en" name="titulo_en" value="<?php echo $portafolio->get_titulo_en(); ?>">
                                        <?php endif; ?>
                                    </div>
                                    <label class="error-label col-sm-3 pt-xs-10" data-error-input="titulo_en"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <span class="error-text"></span></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="descripcion">Descripción:</label>
                                    <div class="col-sm-6">
                                        <?php if (!isset($portafolio)): ?>
                                            <textarea class="form-control" rows="5" id="descripcion_es" name="descripcion_es"></textarea>
                                        <?php else: ?>
                                            <textarea class="form-control" rows="5" id="descripcion_es" name="descripcion_es"><?php echo $portafolio->get_descripcion(); ?></textarea>
                                        <?php endif; ?>
                                    </div>
                                    <label class="error-label col-sm-3 pt-xs-10" data-error-input="descripcion_es"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <span class="error-text"></span></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="descripcion_en">Descripción en Inglés:</label>
                                    <div class="col-sm-6">
                                        <?php if (!isset($portafolio)): ?>
                                            <textarea class="form-control" rows="5" id="descripcion_en" name="descripcion_en"></textarea>
                                        <?php else: ?>
                                            <textarea class="form-control" rows="5" id="descripcion_en" name="descripcion_en"><?php echo $portafolio->get_descripcion_en(); ?></textarea>
                                        <?php endif; ?>
                                    </div>
                                    <label class="error-label col-sm-3 pt-xs-10" data-error-input="descripcion_en"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <span class="error-text"></span></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="estado">Estado*:</label>
                                    <div class="col-sm-3">
                                        <select class="form-control" id="estado" name="estado">
                                            <?php if (!isset($portafolio)): ?>
                                                <option selected value="1">Activo</option>
                                                <option value="0">Inactivo</option>
                                            <?php else: ?>
                                                <option <?php echo ($portafolio->get_estado() == 1) ? 'selected' : ''; ?> value="1">Activo</option>
                                                <option <?php echo ($portafolio->get_estado() == 2) ? 'selected' : ''; ?> value="0">Inactivo</option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                    <label class="error-label col-sm-3 pt-xs-10" data-error-input="estado"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <span class="error-text"></span></label>
                                </div>
                                <?php if (!isset($portafolio)): ?>
                                    <input type="hidden" id="imagen-principal-portafolio-input" name="imagen_principal_portafolio" value="">
                                <?php else: ?>
                                    <input type="hidden" id="imagen-principal-portafolio-input" name="imagen_principal_portafolio" value="<?php echo $portafolio->get_imagen(); ?>">
                                <?php endif; ?>
                                <?php if (isset($portafolio)): ?>
                                    <input type="hidden" name="id_portafolio" id="id_portafolio" value="<?php echo $portafolio->get_id(); ?>">
                                    <?php if (!empty($portafolio->get_imagenes_portafolio())): ?>
                                        <?php foreach ($portafolio->get_imagenes_portafolio() as $imagen): ?>
                                            <input type="hidden" id="imagen-portafolio-input-<?php echo substr($imagen, 0, strpos($imagen, '.')); ?>" class="imagen-portafolio-input" name="imagenes_portafolio[]" value="<?php echo $imagen; ?>">
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php echo form_close(); ?>
                            <?php echo form_open('' , array('id' => 'imagen-principal-form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')); ?>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="imagen_principal_input">Foto Preview (680x770 px)*:</label>
                                    <div class="col-sm-3">
                                        <?php if (!isset($portafolio)): ?>
                                            <input type="file" class="form-control" id="imagen_principal_input" name="imagen_principal">
                                            <button type="button" class="btn btn-default" id="imagen_principal_boton">Subir un Archivo</button>
                                        <?php else: ?>
                                            <input type="file" class="form-control" id="imagen_principal_input" name="imagen_principal" disabled>
                                            <button type="button" class="btn btn-default" id="imagen_principal_boton" disabled>Subir un Archivo</button>
                                        <?php endif; ?>
                                        <img class="loading-gif" src="<?php echo base_url('public/backend/img/loading.gif'); ?>">
                                    </div>
                                    <label class="error-label col-sm-3 pt-xs-10" data-error-input="imagen_principal_portafolio"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <span class="error-text"></span></label>
                                </div>
                            <?php echo form_close(); ?>
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3 mt-10 mb-20">
                                    <div id="imagen-principal-preview" class="row">
                                        <?php if (isset($portafolio)): ?>
                                            <div class="col-sm-3 portafolio-img-box pb-10">
                                                <a href="#" data-toggle="modal" data-target="#imagen-principal-portafolio-modal-<?php echo substr($portafolio->get_imagen(), 0, strpos($portafolio->get_imagen(), '.')); ?>">
                                                    <img class="img-responsive img-center" src="<?php echo base_url('assets/uploads/portfolio/' . $portafolio->get_imagen()); ?>">
                                                </a>
                                                <button class="btn-erase" data-img-name="<?php echo $portafolio->get_imagen(); ?>" data-img-raw-name="<?php echo substr($portafolio->get_imagen(), 0, strpos($portafolio->get_imagen(), '.')); ?>">
                                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                </button>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_open('' , array('id' => 'imagenes-portafolio-form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')); ?>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="imagenes_portafolio_input">Fotos del Portafolio:</label>
                                    <div class="col-sm-3">
                                        <input type="file" class="form-control" id="imagenes_portafolio_input" name="imagenes_portafolio[]" multiple>
                                        <button type="button" class="btn btn-default" id="imagenes_portafolio_boton">Subir un Archivo</button>
                                        <img class="loading-gif" src="<?php echo base_url('public/backend/img/loading.gif'); ?>">
                                    </div>
                                    <label class="error-label col-sm-3 pt-xs-10" data-error-input="imagenes_portafolio"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <span class="error-text"></span></label>
                                </div>
                            <?php echo form_close(); ?>
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3 mt-10 mb-20">
                                    <div id="imagenes-portafolio-preview" class="row">
                                        <?php if (isset($portafolio)): ?>
                                            <?php foreach ($portafolio->get_imagenes_portafolio() as $imagen): ?>
                                                <div class="col-sm-3 portafolio-img-box pb-10"">
                                                    <a href="#" data-toggle="modal" data-target="#imagen-portafolio-modal-<?php echo substr($imagen, 0, strpos($imagen, '.')); ?>">
                                                        <img class="img-responsive img-center" src="<?php echo base_url('assets/uploads/portfolio_imagenes/' . $imagen); ?>">
                                                    </a>
                                                    <button class="btn-erase" data-img-name="<?php echo $imagen; ?>" data-img-raw-name="<?php echo substr($imagen, 0, strpos($imagen, '.')); ?>">
                                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                    </button>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-20">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <button class="btn btn-success" type="button" id="form-button-save">
                                        <i class="fa fa-check"></i> Guardar
                                    </button>
                                    <button class="btn btn-danger" type="button" id="form-button-cancel">
                                        <i class="fa fa-warning"></i> Cancelar y regresar a la lista
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>

        <?php if (isset($portafolio)): ?>
            <div class="modal fade" id="imagen-principal-portafolio-modal-<?php echo substr($portafolio->get_imagen(), 0, strpos($portafolio->get_imagen(), '.')); ?>" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <img class="img-responsive img-center" src="<?php echo base_url('assets/uploads/portfolio/' . $portafolio->get_imagen()); ?>">
                        </div>
                    </div>
                </div>
            </div>
            <?php foreach ($portafolio->get_imagenes_portafolio() as $imagen): ?>
                <div class="modal fade" id="imagen-portafolio-modal-<?php echo substr($imagen, 0, strpos($imagen, '.')); ?>" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <img class="img-responsive img-center" src="<?php echo base_url('assets/uploads/portfolio_imagenes/' . $imagen); ?>">
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>