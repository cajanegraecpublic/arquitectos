<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Go :: <?php echo $titlePage; ?> </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- The fav icon -->
        <link rel="icon" type="image/jpeg" href="<?php echo base_url('public/img/goapp/favicon/favicon.ico'); ?>">

        <script type="text/javascript">
            var base_url = '<?php echo base_url(); ?>'; 

            var js_site_url = function( urlText ){
                var urlTmp = "<?php echo site_url('" + urlText + "'); ?>";
                return urlTmp;
            }

            var js_base_url = function( urlText ){
                var urlTmp = "<?php echo base_url('" + urlText + "'); ?>";
                return urlTmp;
            }
        </script>

        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Dosis:300,400,500">
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/styles-map.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/custom.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/owl.carousel.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/owl.theme.green.css'); ?>">
    </head>
    <body>
        <div id="map"></div>
        <img id="img_triangule" src="<?php echo base_url('public/img/triangule.png'); ?>">
        <!-- jQuery -->
        <script type="text/javascript" src="<?php echo base_url('public/js/jquery-1.11.2.min.js'); ?>"></script>


        <script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=geometry&key=AIzaSyAQ3ZX3i3kWnnFWUucKdmlY4b72fZAp7Jk"></script>
        <script type="text/javascript" src="<?php echo base_url('public/js/owl.carousel.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/js/snackbar.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/js/markerclusterer.js'); ?>"></script>

        <?php
            if( $txtLongitud==null || $txtLatitud==null ){
                $txtLatitud=-2.2073318;
                $txtLongitud=-79.8933472;
            }       
            if (strpos($txtLongitud, '.') == false || strpos($txtLatitud, '.') == false) {
                $txtLatitud=-2.2073318;
                $txtLongitud=-79.8933472;
            }

            if ( $txtCategoria == "" ) {
                $txtCategoria = 0;
            }
        ?>

        <script type="text/javascript">
            //var URL_BASE = "http://107.170.105.224:8880/";
            //var URL_PROMOS = "assets/qhh/promos/";

            var radius;
            var kmByZoom={};
            var infowindow;
            var markers = [];
            var i;
            var centerCamera = true;
            var localesGlobales;
            var caruselHeight = 0;
            var onlyPromotions = false;
            var markerCluster;

            fillZoomLevelByScreenWidth();
            animateMarkers();

            var styles = 
            [
              {
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#ebe3cd"
                  }
                ]
              },
              {
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#523735"
                  }
                ]
              },
              {
                "elementType": "labels.text.stroke",
                "stylers": [
                  {
                    "color": "#f5f1e6"
                  }
                ]
              },
              {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                  {
                    "color": "#c9b2a6"
                  }
                ]
              },
              {
                "featureType": "administrative.land_parcel",
                "elementType": "geometry.stroke",
                "stylers": [
                  {
                    "color": "#dcd2be"
                  }
                ]
              },
              {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#ae9e90"
                  }
                ]
              },
              {
                "featureType": "landscape.natural",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#dfd2ae"
                  }
                ]
              },
              {
                "featureType": "poi",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#dfd2ae"
                  }
                ]
              },
              {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#93817c"
                  }
                ]
              },
              {
                "featureType": "poi.park",
                "elementType": "geometry.fill",
                "stylers": [
                  {
                    "color": "#a5b076"
                  }
                ]
              },
              {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#447530"
                  }
                ]
              },
              {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#f5f1e6"
                  }
                ]
              },
              {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#fdfcf8"
                  }
                ]
              },
              {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#f8c967"
                  }
                ]
              },
              {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                  {
                    "color": "#e9bc62"
                  }
                ]
              },
              {
                "featureType": "road.highway.controlled_access",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#e98d58"
                  }
                ]
              },
              {
                "featureType": "road.highway.controlled_access",
                "elementType": "geometry.stroke",
                "stylers": [
                  {
                    "color": "#db8555"
                  }
                ]
              },
              {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#806b63"
                  }
                ]
              },
              {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#dfd2ae"
                  }
                ]
              },
              {
                "featureType": "transit.line",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#8f7d77"
                  }
                ]
              },
              {
                "featureType": "transit.line",
                "elementType": "labels.text.stroke",
                "stylers": [
                  {
                    "color": "#ebe3cd"
                  }
                ]
              },
              {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#dfd2ae"
                  }
                ]
              },
              {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [
                  {
                    "color": "#b9d3c2"
                  }
                ]
              },
              {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#92998d"
                  }
                ]
              }
            ]; 

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15, 
                center: new google.maps.LatLng(<?php echo $txtLatitud; ?>, <?php echo $txtLongitud; ?>),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                streetViewControl: false,
                zoomControl: false,
                clickableIcons: false,
                styles: styles        
            });

            //zoomControlOptions: { position: google.maps.ControlPosition.RIGHT_TOP }

            var imageIcon = new google.maps.MarkerImage(
                    js_base_url('public/img/my_location.png'),
                    null,null,null,
                    new google.maps.Size(45, 56)
            );
            var myMarker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(<?php echo $txtLatitud; ?>, <?php echo $txtLongitud; ?>),
                icon: imageIcon
            });
            
            map.addListener('projection_changed', function() {
                setMarkersByKmRadious();
            });

            map.addListener('dragend', function() {
                setMarkersByKmRadious();
            });

            map.addListener('zoom_changed', function() {
                setMarkersByKmRadious();
            });     

            map.addListener('bounds_changed', function() {
                var bounds = map.getBounds();
                var center = map.getCenter();
                if (bounds && center) {
                  var ne = bounds.getNorthEast();
                  // Calculate radius (in meters).
                  radius = google.maps.geometry.spherical.computeDistanceBetween(center, ne)/1000;
                  //console.log("radio1:"+radius);
                }
            });
            
            function fillZoomLevelByScreenWidth(){
                var equatorLength = 6378140; // in meters
                var widthInPixels = $(window).width();
                var metersPerPixel = equatorLength / 256;
                var zoomLevel = 1;       
                
                kmByZoom[zoomLevel]=metersPerPixel;
                while ((metersPerPixel * widthInPixels) > 50) {
                    metersPerPixel /= 2;                
                    ++zoomLevel;
                    kmByZoom[zoomLevel]=metersPerPixel;
                    //console.log("zoomLevel:"+zoomLevel+" metersPerPixel:"+metersPerPixel);
                }
                return zoomLevel; 
            }
            
            function setMarkersByKmRadious(){
                if(radius==null)
                    radius=kmByZoom[map.getZoom()];
                console.log(js_site_url("data/getLocalesPromosXUbicacion/" + map.getCenter().lat() + "/" + map.getCenter().lng() + "/" + radius + "/"+ <?php echo $txtCategoria; ?> ));
                $.ajax({
                    type:"GET",
                    url: js_site_url("data/getLocalesPromosXUbicacion/"+map.getCenter().lat()+"/"+map.getCenter().lng()+"/"+radius+"/"+<?php echo $txtCategoria; ?>),
                    dataType:"json",
                    complete:function (data) {
                        if(data==null){
                            console.log("Problemas técnicos intente mas tarde");
                            return;
                        }
                        var json = data.responseJSON;
                        if(json.codigo==1){
                            //QUITO EL SNACKBAR
                            $(".paper-snackbar").remove();
                            //QUITO LOS MARCADORES
                            clearMarkers();
                            localesGlobales = json.data.locales;
                            console.log("Obtuvo: "+localesGlobales.length+" locales");
                            addMarkers(localesGlobales);
                            addCarrusel(localesGlobales);
                            if(markerCluster != null)
                                markerCluster.clearMarkers();
                            markerCluster = new MarkerClusterer(map, markers, {imagePath: js_base_url('public/img/m')});
                        } 
                        else{
                          console.log(json.mensaje);
                          //QUITO EL CARUSEL Y LOS MARCADORES
                          $("#owl-demo").remove();
                          $("#img_triangule").hide();
                          clearMarkers();
                          //AGREGO EL MENSAJE PAR QUE EL USUARIO ALEJE
                          if($(".paper-snackbar").length==0){
                              createSnackbar("No se encontraron locales cercanos.", 'ALEJAR', function() { 
                                map.setZoom(map.getZoom()-1); 
                              });
                          }
                        }
                    },
                    failure:function (errMsg) {
                      console.log(errMsg);
                    }
                }); 
            }

            function animateMarkers() {
                var marker, _i, _len;
                for (_i = 0, _len = markers.length; _i < _len; _i++) {
                  marker = markers[_i];
                  if (marker.opacities.length > 0) {
                    marker.setOpacity(marker.opacities[0]);
                    marker.opacities.shift();
                  }
                }
                return setTimeout(function() {
                  return animateMarkers();
                }, 100);
            }

            function changeMarkersOpacity(itemCliente, clientes){

                var cliente = clientes[itemCliente];
                for (var i = 0; i < markers.length; i++) {
                    var themarker=markers[i];
                    if(themarker.idCliente && themarker.idCliente == cliente.clienteId)
                        themarker.opacities = themarker.opacities.concat([.5, .6, .8, 1]);
                    else if(themarker.idLocal == cliente.localId)
                        themarker.opacities = themarker.opacities.concat([.5, .6, .8, 1]); 
                    else
                        themarker.opacities = themarker.opacities.concat([.5]);
                }
            }

            function addMarkers(locales) {
      
                for (j = 0; j < locales.length; j++) {  
                    addMarker(locales[j], j, j == 0);                    
                }
                
            }

            function addMarker(local, item, selected) {

                var rutaIcono = local.categoriaIcono;
                var promo = getLocalPromoActiva(local);
                var tienePromo = promo!=null ? true : false;
                if(promo != null){
                   rutaIcono = local.categoriaIconoPromo;
                }
                if(!tienePromo && onlyPromotions){
                    return;
                }

                var imageIcon = new google.maps.MarkerImage(
                    rutaIcono,
                    null,null,null,
                    new google.maps.Size(50, 63));

                if(!existMarker(local)){

                    var idPromoOCliente = "L"+local.localId;
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(local.localLatitud, local.localLongitud),
                        map: map,
                        icon: imageIcon,
                        opacity: 0,
                        opacities: [],
                        idCliente : local.clienteId,
                        idLocal : local.localId,
                        tienePromo: tienePromo
                    });

                    if(tienePromo){
                        idPromoOCliente = "P"+promo.promoId;
                    }
                    if(local.localTuristico == 1){
                       idPromoOCliente = "T"+local.localId; 
                       selected = true;
                    }

                    if(selected)
                        marker.opacities = marker.opacities.concat([.1, .4, .6, .8, 1]);
                    else
                        marker.opacities = marker.opacities.concat([.1, .2, .3, .4, .5]);

                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                            
                            if(infowindow)
                                infowindow.close();
                            var contentString = '<div>'+
                                                    '<div style="float:left; clear:none; margin-right: 10px;">'+
                                                     '<img style="margin-top: 13px;" src="'+local.localFoto+'" width=80 />'+
                                                    '</div>'+
                                                    '<div style="float:left; clear:none; max-width: 150px;">'+
                                                      '<p><b>'+local.localNombre+'</b><br><br>'+
                                                      '<b>Dir:</b> '+local.localDireccion+'<br>'+
                                                      (local.localTuristico ? '' : '<b>Telf:</b> '+(local.localTelefono ? local.localTelefono : "NA")+'<br>')+
                                                      "<br><img width=\"90\" onclick=\"verClienteOPromo('"+idPromoOCliente+"')\" src=\"http://vamosgo.com/public/img/ver_mas.png\"/>"+
                                                    '</div>'+
                                                  '</div>';
                            infowindow = new google.maps.InfoWindow({
                                content: contentString
                            });
                            infowindow.open(map, marker);

                            //MUEVO EL CARUSEL A ESA POSICION
                            $('#owl-demo').trigger('to.owl.carousel', [item,0,true])
                            
                        }
                    })(marker, i));
                    markers.push(marker); 
                }           
            }

            function addCarrusel(locales){
                $("#owl-demo").remove(); 
                $("#img_triangule").hide();
                $("body").append('<div id="owl-demo" class="owl-carousel"></div>');
                for (var i = 0; i < locales.length; i++) {
                    var promo = tienePromoActiva(locales[i]);
                    var tienePromo = promo != null ? true : false;
                    var idPromoOCliente = "L"+locales[i].localId;
                    var promoImagen = js_base_url('public/img/test-800.jpg');
                    var clienteBanner = js_base_url('public/img/test-200.jpg');
                    var clienteLogo = js_base_url('public/img/test-1000.jpg');

                    if(!tienePromo && onlyPromotions){
                        continue;
                    }
                    if(locales[i].localTuristico == 1){
                        continue;
                    }
                    var count_elements = 1;
                    if (tienePromo) {
                      count_elements = parseInt(promo.length);
                    }
                    for (var k = 0; k < count_elements; k++) {
                      if(tienePromo){
                          idPromoOCliente = "P"+promo[k].promoId;
                          promoImagen = promo[k].promoImagen;
                      }
                      //if(locales[i].localTuristico == 1){
                      //    idPromoOCliente = "T"+locales[i].localId; 
                      //}
                      if(locales[i].clienteBanner && locales[i].clienteBanner.length > 0){
                          clienteBanner = locales[i].clienteBanner;
                      }
                      if(locales[i].clienteLogo && locales[i].clienteLogo.length > 0){ 
                          clienteLogo = locales[i].clienteLogo;
                      }
                      if(locales[i].localLogo && locales[i].localLogo.length > 0){ 
                          clienteLogo = locales[i].localLogo; 
                      }

                      $("#owl-demo").append('<div class="item activo" data-hash="'+idPromoOCliente+'" onclick="verClienteOPromo(\''+idPromoOCliente+'\');">'+
                          (tienePromo ?
                              '<div class="imagen_banner_cliente">\
                                  <img src="'+clienteBanner+'" alt="'+locales[i].clienteNombre+'">\
                              </div>\
                              <div class="imagen_promo">\
                                  <img src="'+promoImagen+'" alt="'+locales[i].clienteNombre+'">\
                              </div>' 
                              :
                              '<div class="imagen_cliente">\
                                  <img src="'+clienteLogo+'" alt="'+locales[i].clienteNombre+'">\
                              </div>')+
                      '</div>');
                    }
                }       

                $("#owl-demo").on('initialized.owl.carousel', function(event) {
                    
                    $(".owl-item.active.center").find("img").load(function(){
                        setTimeout(function() {
                            $("#img_triangule").show();
                            caruselHeight = $("#owl-demo").height();
                            $("#img_triangule").css("bottom",caruselHeight);
                        }, 1000);                        
                    });

                });

                $("#owl-demo").owlCarousel({
                    nav : false,
                    dots : false,
                    loop:false,
                    center: true, 
                    items: 2,
                    URLhashListener: true,
                    responsive:{
                        0:{
                            items:2
                        },
                        600:{
                            items:4
                        },
                        1000:{
                            items:8
                        }
                    }
                });

                $("#owl-demo").on('changed.owl.carousel', function(event) {
                    changeMarkersOpacity(event.item.index, localesGlobales);
                });
                
                $("#owl-demo").on('resized.owl.carousel', function(event) {
                    $("#img_triangule").css("bottom",$("#owl-demo").height());
                    caruselHeight = $("#owl-demo").height();
                });

                $("#owl-demo").on('updated.owl.carousel', function(event) {
                    $("#img_triangule").css("bottom",$("#owl-demo").height());
                    caruselHeight = $("#owl-demo").height();
                });

                $("#img_triangule").off();
                $("#img_triangule").click(function(){
                    
                    var theHeight = caruselHeight - 15;
                    var theHeightTriangule = 15;
                    var degrees = 0;
                    if($("#owl-demo").css("bottom").indexOf("-") >= 0){
                        theHeight = 0;
                        theHeightTriangule = caruselHeight;
                        degrees = 180;
                    }

                    $("#owl-demo").animate({ 
                        bottom: theHeight * -1,
                    }, 250 );

                    $('#img_triangule').animate({  
                        bottom: theHeightTriangule,
                        borderSpacing: degrees
                    }, {
                        step: function(now,fx) {
                          $(this).css('-webkit-transform','rotate('+now+'deg)'); 
                          $(this).css('-moz-transform','rotate('+now+'deg)');
                          $(this).css('transform','rotate('+now+'deg)');
                        },
                        duration: 250
                    }, 250);

                });
            }

            function verClienteOPromo(idPromoOCliente){
                window.location.href= window.location.href.split("#")[0]+"#"+idPromoOCliente+"#"+map.getCenter().lat() + "#" + map.getCenter().lng() + "#" + radius;
            }

            function setMapOnAll(map) {
                console.log("seteando "+markers.length+" marcadores en el mapa")
              for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
              }
            }

            function clearMarkers() {
              setMapOnAll(null);
              markers = [];
            }

            function existMarker(local){
                for (var i = 0; i < markers.length; i++) {
                    var themarker=markers[i];
                    if(themarker.idLocal==local.localId){
                        return true;
                    }
                }
                return false;
            }

            function onChangeMyPosition(lat,lon){
                if(map!=null && myMarker!=null){
                    var latlng = new google.maps.LatLng(lat, lon);
                    myMarker.setPosition(latlng);
                    if(centerCamera)
                        map.panTo(latlng);
                    return "1";
                }
                return "0";
            }

            function getLocalPromoActiva(local){

                var promo = null;
                var promos = local.localPromociones;
                if(!promos || promos.length==0){
                    return promo;
                }
                for (var j = 0; j < promos.length; j++) { 
                    if(promos[j].promoEstado == 1){
                        promo = promos[j];
                        break;
                    }
                }            

                return promo;

            }

            function tienePromoActiva(local){

                var promo = null;
                var promos = local.localPromociones;
                if(promos) {
                  if (promos.length > 0) {
                    promo = [];
                    for (var j = 0; j < promos.length; j++) { 
                        if(promos[j].promoEstado == 1){
                            promo.push(promos[j]);
                        }
                    }
                  }
                }
                return promo;
            }

            /****FUNCIONES LLAMADAS DESDE EL APP****/

            function setPosition(latitude, longitude){
                var imgX = '0';
                var animationInterval = setInterval(function(){
                    if(imgX == '-18') imgX = '0';
                    else imgX = '-18';
                }, 500);
                var latlng = new google.maps.LatLng(latitude, longitude);
                myMarker.setPosition(latlng);
                map.panTo(latlng);
                clearInterval(animationInterval);
                centerCamera=true;
            }

            function showHidePromotions(){
                onlyPromotions = !onlyPromotions;
                setMarkersByKmRadious();
            }

        </script> 
    </body>
</var>