		<!-- Page Content -->
        <div class="col-lg-10 col-sm-10">
            <div class="row">
                <div class="box-header well" data-original-title="">
                    <h2>
                        <i class="glyphicon glyphicon-folder-open"></i>&nbsp;&nbsp;Portafolios
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 pl-0 pr-0">
                    <div class="form-label pl-5 pb-5 pt-5 form-label">Visualizar Portafolios</div>
                </div>
            </div>
            <div class="row">
            	<div class="col-xs-12 data-container">
            		<div class="row pt-10">
            			<div class="col-sm-3 text-right text-bold">
            				Título:
            			</div>
            			<div class="col-sm-6">
            				<p><?php echo $portafolio->get_titulo(); ?></p>
            			</div>
            		</div>
            		<div class="row pt-10">
            			<div class="col-sm-3 text-right text-bold">
            				Título en Inglés:
            			</div>
            			<div class="col-sm-6">
            				<p><?php echo $portafolio->get_titulo_en(); ?></p>
            			</div>
            		</div>
            		<div class="row colored-row pt-10">
            			<div class="col-sm-3 text-right text-bold">
            				Descripción:
            			</div>
            			<div class="col-sm-6">
            				<p><?php echo $portafolio->get_descripcion(); ?></p>
            			</div>
            		</div>
            		<div class="row pt-10">
            			<div class="col-sm-3 text-right text-bold">
            				Descripción en Inglés:
            			</div>
            			<div class="col-sm-6">
            				<p><?php echo $portafolio->get_descripcion_en(); ?></p>
            			</div>
            		</div>
            		<div class="row colored-row pt-10">
            			<div class="col-sm-3 text-right text-bold">
            				Estado:
            			</div>
            			<div class="col-sm-6">
            				<p><?php echo ($portafolio->get_estado() == 1) ? 'Activo' : 'Inactivo'; ?></p>
            			</div>
            		</div>
            		<div class="row pt-10">
            			<div class="col-sm-3 text-right text-bold">
            				Foto Preview:
            			</div>
            		</div>
            		<div class="row">
            			<div class="col-sm-9 col-sm-offset-3 mt-10 mb-20">
            			    <div class="row">
        			            <div class="col-sm-2 portafolio-img-box pb-10">
        			                <a href="#" data-toggle="modal" data-target="#imagen-principal-modal">
        			                    <img class="img-responsive img-center" src="<?php echo base_url('assets/uploads/portfolio/' . $portafolio->get_imagen()); ?>">
        			                </a>
        			            </div>
            			    </div>
        			    </div>
            		</div>
            		<?php if (!empty($portafolio->get_imagenes_portafolio())): ?>
            			<div class="row colored-row pt-10">
            				<div class="col-sm-3 text-right text-bold">
            					Imágenes del portafolio:
            				</div>
            			</div>
                		<div class="row colored-row pt-10">
                			<div class="col-sm-9 col-sm-offset-3 mt-10 mb-20">
                			    <div class="row">
                			    	<?php foreach ($portafolio->get_imagenes_portafolio() as $imagen): ?>
                			            <div class="col-sm-2 portafolio-img-box pb-10">
                			                <a href="#" data-toggle="modal" data-target="#imagen-portafolio-modal-<?php echo substr($imagen, 0, strpos($imagen, '.')); ?>">
                			                    <img class="img-responsive img-center" src="<?php echo base_url('assets/uploads/portfolio_imagenes/' . $imagen); ?>">
                			                </a>
                			            </div>
            			        	<?php endforeach; ?>
                			    </div>
            			    </div>
                		</div>
        			<?php endif; ?>
        			<div class="row mt-20">
        			    <div class="col-sm-3 text-right">
        			        <a class="btn btn-danger" href="<?php echo base_url('index.php/admin_portafolios/crud_portafolios'); ?>">
        			            <i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar a la lista
        			        </a>
        			    </div>
        			</div>
        	    </div>
            </div>
        </div>
        <div class="modal fade" id="imagen-principal-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <img class="img-responsive img-center" src="<?php echo base_url('assets/uploads/portfolio/' . $portafolio->get_imagen()); ?>">
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($portafolio->get_imagenes_portafolio())): ?>
        	<?php foreach ($portafolio->get_imagenes_portafolio() as $imagen): ?>
        		<div class="modal fade" id="imagen-portafolio-modal-<?php echo substr($imagen, 0, strpos($imagen, '.')); ?>" tabindex="-1" role="dialog">
        		    <div class="modal-dialog" role="document">
        		        <div class="modal-content">
        		            <div class="modal-header">
        		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		            </div>
        		            <div class="modal-body">
        		                <img class="img-responsive img-center" src="<?php echo base_url('assets/uploads/portfolio_imagenes/' . $imagen); ?>">
        		            </div>
        		        </div>
        		    </div>
        		</div>
    		<?php endforeach; ?>
    	<?php endif; ?>