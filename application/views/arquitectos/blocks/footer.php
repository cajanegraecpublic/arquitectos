    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" fclass="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <hr>
        <footer class="row">
            <p class="col-md-9 col-sm-9 col-xs-12 copyright">

                &copy; <a href="#" target="_blank">Guerrero Ferreccio</a> <?php echo date('Y') ?>
            </p>

            <p class="col-md-3 col-sm-3 col-xs-12 powered-by">
                Desarrollado por: <a href="#">Guerrero Ferreccio</a>
            </p>
        </footer>
    </div><!--/.fluid-container-->


    <?php if (isset($js_files)): ?>

        
        <!-- grocerycrud -->
        <?php foreach($js_files as $file): ?>
            <script type="text/javascript" src="<?php echo $file; ?>"></script>
        <?php endforeach; ?>
        <!-- grocerycrud -->


        <?php if (isset($js_files_dd)): ?>
            <!-- Dependent Dropdown -->
            <?php echo $js_files_dd; ?>
            <!-- Dependent Dropdown -->            
        <?php endif ?>
    <?php else: ?>
        <!-- jQuery -->
        <script type="text/javascript" src="<?php echo base_url('public/backend/js/jquery-1.11.2.min.js'); ?>"></script>

        <?php /*
        <script type="text/javascript" src="<?php echo base_url('public/backend/bower_components/jquery/jquery.min.js'); ?>"></script>
        */ ?>
    <?php endif ?>
    
    
    <!-- Bootstrap -->
    <script type="text/javascript" src="<?php echo base_url('public/backend/bower_components/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>

    <!-- Moment -->
    <script type="text/javascript" src="<?php echo base_url('public/backend/bower_components/moment/min/moment.min.js'); ?>"></script>
    

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- library for cookie management -->
    <script type="text/javascript" src="<?php echo base_url('public/backend/js/jquery.cookie.js'); ?>"></script>

    <!-- calender plugin -->
    <script type="text/javascript" src="<?php echo base_url('public/backend/bower_components/fullcalendar/dist/fullcalendar.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/backend/bower_components/fullcalendar/dist/lang-all.js'); ?>"></script>
    
    <!-- data table plugin -->
    <script type="text/javascript" src="<?php echo base_url('public/backend/js/jquery.dataTables.min.js'); ?>"></script>
    
    <!-- select or dropdown enhancer -->
    <script type="text/javascript" src="<?php echo base_url('public/backend/bower_components/chosen/chosen.jquery.min.js'); ?>"></script>
    
    <!-- plugin for gallery image view -->
    <script type="text/javascript" src="<?php echo base_url('public/backend/bower_components/colorbox/jquery.colorbox-min.js'); ?>"></script>
    
    <!-- notification plugin -->
    <script type="text/javascript" src="<?php echo base_url('public/backend/js/jquery.noty.js'); ?>"></script>

    <!-- library for making tables responsive -->
    <script type="text/javascript" src="<?php echo base_url('public/backend/bower_components/responsive-tables/responsive-tables.js'); ?>"></script>
    
    <!-- tour plugin -->
    <script type="text/javascript" src="<?php echo base_url('public/backend/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js'); ?>"></script>

    <!-- star rating plugin -->
    <script type="text/javascript" src="<?php echo base_url('public/backend/js/jquery.raty.min.js'); ?>"></script>

    <!-- for iOS style toggle switch -->
    <script type="text/javascript" src="<?php echo base_url('public/backend/js/jquery.iphone.toggle.js'); ?>"></script>
    
    <!-- autogrowing textarea plugin -->
    <script type="text/javascript" src="<?php echo base_url('public/backend/js/jquery.autogrow-textarea.js'); ?>"></script>
    
    <!-- multiple file upload plugin -->
    <script type="text/javascript" src="<?php echo base_url('public/backend/js/jquery.uploadify-3.1.min.js'); ?>"></script>

    
    <!-- history.js for cross-browser state change on ajax -->
    <script type="text/javascript" src="<?php echo base_url('public/backend/js/jquery.history.js'); ?>"></script>

    <!-- application script for Charisma demo -->
    <script type="text/javascript" src="<?php echo base_url('public/backend/js/charisma.js'); ?>"></script>

    <!-- application script for placehorder in crud -->
    <script type="text/javascript">
        $(document).ready(function () {
            // Registro
            $("#field-usuario_nombre").attr("placeholder", "Ej: Henry Tumbaco.");
            //$("#field-celular").attr("placeholder", "Ej: 0925485648");
            //$("#field-usuario_mail").attr("placeholder", "Ej: user@ejemplo.com");
            //$("#field-usuario_clave").attr("placeholder", "Contraseña");
            //$("#field-contrasena").attr("placeholder", "Repetir Contraseña");
            $("#field-web").attr("placeholder", "ej: www.ejemplo.com");

            // Registro local
        });
    </script>


    <?php /*
    <script type="text/javascript" src="<?php echo base_url('public/backend/js/script-edwin.js'); ?>"></script>
    */ ?>

    <!-- application script for captcha -->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <!-- Script -->
    <?php if ($this->router->fetch_class() == "cliente" && $this->router->fetch_method() == "guia"): ?>
        <script type="text/javascript" src="<?php echo base_url('public/backend/js/script-registro.js'); ?>"></script>
    <?php elseif($this->router->method == 'crud_portafolios'): ?>
        <script src="<?php echo base_url('public/backend/js/jquery-confirm.min.js'); ?>"></script>
        <script src="<?php echo base_url('public/backend/js/crud_portafolios.js'); ?>"></script>
    <?php elseif($this->router->method == 'create_portafolio' || $this->router->method == 'update_portafolio'): ?>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.0/jquery.matchHeight-min.js"></script>
        <script src="<?php echo base_url('public/backend/js/jquery-confirm.min.js'); ?>"></script>
        <script src="<?php echo base_url('public/backend/js/jquery.validate.min.js'); ?>"></script>
        <script src="<?php echo base_url('public/backend/js/insert_portafolio.js'); ?>"></script>
    <?php endif ?>

    <?php if ($this->router->fetch_class() == "cliente" && $this->router->fetch_method() == "registrar"): ?>
        <script>
            var flag_pass = false;
            var flag_mail = false;
            $(document).ready(function() {
                $("#field-usuario_clave").keyup(function(event) {
                    if ($(this).val().length == 0) {
                        ocultarErrorPasswords();
                    }
                });         
                $("#field-contrasena").keyup(function(event) {
                    if (validar_password($("#field-usuario_clave").val(), $("#field-contrasena").val())) {
                        mostrarErrorPasswords();
                    } else {
                        ocultarErrorPasswords();
                    }
                });
                $("#field-usuario_mail").change(function(event) {
                    checkMailUnique($(this).val());
                });
            });

            function habilitarDeshabilitarGuardado() {
                if (flag_mail || flag_pass) {
                    $("#form-button-save").attr("disabled", "disabled");
                } else {
                    $("#form-button-save").removeAttr('disabled');
                }
            }

            function validar_password(pass1, pass2) {
                var flag = false;
                if (pass1.length != pass2.length) {
                    flag = true;
                } else {
                    for (var i = 0; i < pass1.length; i++) {
                        if (pass1[i] != pass2[i]) {
                            flag = true;
                            break;
                        }
                    }
                }
                return flag;
            }

            function mostrarErrorPasswords() {
                flag_pass = true;
                $("#field-contrasena").attr("style", "border: 1px solid red;");
                if ($("#mensajecontrasena").length == 0) {
                    $("#field-contrasena").after('<p id=\"mensajecontrasena\">Las contraseñas no coinciden.</p>');
                }
                habilitarDeshabilitarGuardado();
                // $("#form-button-save").attr("disabled", "disabled");
            }

            function ocultarErrorPasswords() {
                flag_pass = false;
                $("#field-contrasena").removeAttr("style");
                $("#mensajecontrasena").remove();
                habilitarDeshabilitarGuardado();
                // $("#form-button-save").removeAttr('disabled');
            }

            function mostrarErrorMailUnique() {
                flag_mail = true;
                $("#field-usuario_mail").attr("style", "border: 1px solid red;");
                if ($("#mensajemail").length == 0) {
                    $("#field-usuario_mail").after('<p id=\"mensajemail\">Este correo ya existe en nuestros registros.</p>');
                }
                habilitarDeshabilitarGuardado();
                // $("#form-button-save").attr("disabled", "disabled");
            }

            function ocultarErrorMailUnique() {
                flag_mail = false;
                $("#field-usuario_mail").removeAttr("style");
                $("#mensajemail").remove();
                habilitarDeshabilitarGuardado();
                // $("#form-button-save").removeAttr('disabled');
            }

            function checkMailUnique(mail) {
                mail = mail.replace("@", "gnatarroba");
                $.ajax({
                    url: js_site_url("cliente/check_usuario_mail_unique/" + mail),
                })
                .done(function(data) {
                    if (data.codigo == 0) {
                        mostrarErrorMailUnique();
                    } else {
                        ocultarErrorMailUnique();
                    }
                })
                .fail(function() {
                    console.log("error");
                });                
            }
        </script>
    <?php endif; ?>

    <?php if ($this->router->fetch_class() == "admin" || $this->router->fetch_class() == "cliente"): ?>
        <script type="text/javascript" src="<?php echo base_url('public/backend/js/script-admin.js'); ?>"></script>        
    <?php endif ?>

    <?php if (($this->router->fetch_class() == "cliente" || $this->router->fetch_class() == "admin" ) && ($this->router->fetch_method() == "local") && isset($crud_state) && $crud_state == "add"): ?>
        <script>
            var flag_categorias = true;
            var flag_forma_pago = true;
            var flag_horarios_atencion = true;
            var arr_dias = ["", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo y feriados"];
            $(document).ready(function() {
                $("#form-button-save, #save-and-go-back-button").attr("disabled", "disabled");
                $("#categorias_display_as_box").html("Categorías* :");
                $("#forma_pago_display_as_box").html("Forma de Pago* :");
                $("#horarios_atencion_display_as_box").html("Horarios de Atención* :");
                $("#field-forma_pago").change(function(event) {
                    if ($(this).val() == null) {
                        flag_forma_pago = true;
                    } else {
                        flag_forma_pago = false;
                    }
                    habilitar_deshabilitar_guardado();
                });
                $("#field-categorias").change(function(event) {
                    if ($("#field-categorias option[selected]").length <= 0) {
                        flag_categorias = true;
                        $("#categorias_input_box").attr("style", "border: 1px solid red");
                    } else {
                        flag_categorias = false;
                        $("#categorias_input_box").removeAttr('style');
                    }
                    habilitar_deshabilitar_guardado();
                });
                $("#field-horarios_atencion").change(function(event) {
                    var valores = $("#field-horarios_atencion").val() == null ? [] : $("#field-horarios_atencion").val();
                    if ($("#field-horarios_atencion").val() == null) {
                        flag_horarios_atencion = true;
                    } else {
                        flag_horarios_atencion = false;
                    }
                    for (var i = 1; i <= 7; i++) {
                        if (valores.indexOf(i.toString()) == -1) {
                            $("#horarios_" + i).remove();
                        }
                    }
                    var flag_hi_hf = true;
                    if (!flag_horarios_atencion) {
                        flag_hi_hf = false;
                    }
                    for (var i = 0; i < valores.length; i++) {
                        if ($("#horarios_" + valores[i]).length == 0) {
                            $("#field-horarios_atencion").after("<div class=\"row\" id=\"horarios_" + valores[i] + "\"><div class=\"col-xs-4\">" + arr_dias[parseInt(valores[i])] + "</div><div class=\"col-xs-4\">H. Inicio: <input type=\"time\" id=\"hi_" + valores[i] + "\" name=\"hi_" + valores[i] + "\" /></div><div class=\"col-xs-4\">H. Fin: <input type=\"time\" id=\"hf_" + valores[i] + "\" name=\"hf_" + valores[i] + "\" /></div></div>");
                            $("#hi_" + valores[i] + ", #hf_" + valores[i]).change(function(event) {
                                loop_horarios_flag();
                            });
                        }
                        if ($("#hi_" + valores[i]).val() == "" || $("#hf_" + valores[i]).val() == "") {
                            flag_hi_hf = true;
                        }
                    }
                    if (flag_hi_hf) {
                        flag_horarios_atencion = true;
                    } else {
                        flag_horarios_atencion = false;
                    }
                    habilitar_deshabilitar_guardado();
                });
                $("#field-horarios_atencion").after("<div><input type=\"checkbox\" name=\"siempre\" id=\"input-field-siempre_abierto\" /> <label for=\"siempre\" style=\"font-weight: normal;\">Siempre abierto</label></div>");
                $("#input-field-siempre_abierto").click(function() {
                    var checked = $(this).is(":checked");
                    if (checked) {
                        $("#field-horarios_atencion").val([1, 2, 3, 4, 5, 6, 7]);
                        $("#field-horarios_atencion").change();
                        $("#field_horarios_atencion_chosen >ul.chosen-choices").hide();
                        for (var i = 1; i <= 7; i++) {
                            $("#hi_" + i).val("00:00");
                            $("#hf_" + i).val("23:59");
                            $("#hi_" + i + ", #hf_" + i).change();
                        }
                    } else {
                        $("#field-horarios_atencion").val([]);
                        $("#field-horarios_atencion").change();
                        $.each($("#field_horarios_atencion_chosen >ul.chosen-choices >li.search-choice"), function(index, val) {
                            $(val).children('a.search-choice-close').click();
                        });
                        $("#field_horarios_atencion_chosen >ul.chosen-choices").show();
                    }
                });
            });

            function loop_horarios_flag() {
                var flag_hi_hf = false;
                var valores = $("#field-horarios_atencion").val() == null ? [] : $("#field-horarios_atencion").val();
                for (var i = 0; i < valores.length; i++) {
                    if ($("#hi_" + valores[i]).val() == "" || $("#hf_" + valores[i]).val() == "") {
                        flag_hi_hf = true;
                    }
                }
                if (flag_hi_hf) {
                    flag_horarios_atencion = true;
                } else {
                    flag_horarios_atencion = false;
                }
                habilitar_deshabilitar_guardado();
            }

            function habilitar_deshabilitar_guardado() {
                if (flag_categorias || flag_forma_pago || flag_horarios_atencion) {
                    $("#form-button-save, #save-and-go-back-button").attr("disabled", "disabled");
                } else {
                    $("#form-button-save, #save-and-go-back-button").removeAttr('disabled');
                }
            }
        </script>
    <?php endif; ?>

    <?php if (($this->router->fetch_class() == "cliente" || $this->router->fetch_class() == "admin" ) && ($this->router->fetch_method() == "local") && isset($crud_state) && $crud_state == "edit"): ?>
        <script>
            var flag_categorias = false;
            var flag_forma_pago = false;
            var flag_horarios_atencion = false;
            var arr_dias = ["", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo y feriados"];
            $(document).ready(function() {
                $("#categorias_display_as_box").html("Categorías* :");
                $("#forma_pago_display_as_box").html("Forma de Pago* :");
                $("#horarios_atencion_display_as_box").html("Horarios de Atención* :");
                $("#field-forma_pago").change(function(event) {
                    if ($(this).val() == null) {
                        flag_forma_pago = true;
                    } else {
                        flag_forma_pago = false;
                    }
                    habilitar_deshabilitar_guardado();
                });
                $("#field-categorias").change(function(event) {
                    if ($("#field-categorias option[selected]").length <= 0) {
                        flag_categorias = true;
                        $("#categorias_input_box").attr("style", "border: 1px solid red");
                    } else {
                        flag_categorias = false;
                        $("#categorias_input_box").removeAttr('style');
                    }
                    habilitar_deshabilitar_guardado();
                });
                $("#field-horarios_atencion").change(function(event) {
                    var valores = $("#field-horarios_atencion").val() == null ? [] : $("#field-horarios_atencion").val();
                    if ($("#field-horarios_atencion").val() == null) {
                        flag_horarios_atencion = true;
                    } else {
                        flag_horarios_atencion = false;
                    }
                    for (var i = 1; i <= 7; i++) {
                        if (valores.indexOf(i.toString()) == -1) {
                            $("#horarios_" + i).remove();
                        }
                    }
                    var flag_hi_hf = true;
                    if (!flag_horarios_atencion) {
                        flag_hi_hf = false;
                    }
                    for (var i = 0; i < valores.length; i++) {
                        if ($("#horarios_" + valores[i]).length == 0) {
                            $("#field-horarios_atencion").before("<div class=\"row\" id=\"horarios_" + valores[i] + "\"><div class=\"col-xs-4\">" + arr_dias[parseInt(valores[i])] + "</div><div class=\"col-xs-4\">H. Inicio: <input type=\"time\" id=\"hi_" + valores[i] + "\" name=\"hi_" + valores[i] + "\" /></div><div class=\"col-xs-4\">H. Fin: <input type=\"time\" id=\"hf_" + valores[i] + "\" name=\"hf_" + valores[i] + "\" /></div></div>");
                            $("#hi_" + valores[i] + ", #hf_" + valores[i]).change(function(event) {
                                loop_horarios_flag();
                            });
                        }
                        if ($("#hi_" + valores[i]).val() == "" || $("#hf_" + valores[i]).val() == "") {
                            flag_hi_hf = true;
                        }
                    }
                    if (flag_hi_hf) {
                        flag_horarios_atencion = true;
                    } else {
                        flag_horarios_atencion = false;
                    }
                    habilitar_deshabilitar_guardado();
                });
                getHorariosAtencion();
                $("#field-horarios_atencion").after("<div><input type=\"checkbox\" name=\"siempre\" id=\"input-field-siempre_abierto\" /> <label for=\"siempre\" style=\"font-weight: normal;\">Siempre abierto</label></div>");
                $("#input-field-siempre_abierto").click(function() {
                    var checked = $(this).is(":checked");
                    if (checked) {
                        $("#field-horarios_atencion").val([1, 2, 3, 4, 5, 6, 7]);
                        $("#field-horarios_atencion").change();
                        $("#field_horarios_atencion_chosen >ul.chosen-choices").hide();
                        for (var i = 1; i <= 7; i++) {
                            $("#hi_" + i).val("00:00");
                            $("#hf_" + i).val("23:59");
                            $("#hi_" + i + ", #hf_" + i).change();
                        }
                    } else {
                        $("#field-horarios_atencion").val([]);
                        $("#field-horarios_atencion").change();
                        $.each($("#field_horarios_atencion_chosen >ul.chosen-choices >li.search-choice"), function(index, val) {
                            $(val).children('a.search-choice-close').click();
                        });
                        $("#field_horarios_atencion_chosen >ul.chosen-choices").show();
                    }
                });
            });

            function loop_horarios_flag() {
                var flag_hi_hf = false;
                var valores = $("#field-horarios_atencion").val() == null ? [] : $("#field-horarios_atencion").val();
                for (var i = 0; i < valores.length; i++) {
                    if ($("#hi_" + valores[i]).val() == "" || $("#hf_" + valores[i]).val() == "") {
                        flag_hi_hf = true;
                    }
                }
                if (flag_hi_hf) {
                    flag_horarios_atencion = true;
                } else {
                    flag_horarios_atencion = false;
                }
                habilitar_deshabilitar_guardado();
            }

            function habilitar_deshabilitar_guardado() {
                if (flag_categorias || flag_forma_pago || flag_horarios_atencion) {
                    $("#form-button-save, #save-and-go-back-button").attr("disabled", "disabled");
                } else {
                    $("#form-button-save, #save-and-go-back-button").removeAttr('disabled');
                }
            }

            function getHorariosAtencion() {
                var str = window.location.href.substring(window.location.href.indexOf("local/edit"));
                var arr_str = str.split("\/");
                var id_elemento = arr_str[arr_str.length - 1];
                var arr_vals = [];
                $.ajax({
                    url: js_site_url("admin/get_horarios_atencion/" + id_elemento),
                })
                .done(function(data) {
                    $.each(data, function(key, elem) {
                        arr_vals.push(elem);
                        $("#field-horarios_atencion").before("<div class=\"row\" id=\"horarios_" + key + "\"><div class=\"col-xs-4\">" + arr_dias[parseInt(key)] + "</div><div class=\"col-xs-4\">H. Inicio: <input type=\"time\" id=\"hi_" + key + "\" name=\"hi_" + key + "\" /></div><div class=\"col-xs-4\">H. Fin: <input type=\"time\" id=\"hf_" + key + "\" name=\"hf_" + key + "\" /></div></div>");
                        $("#hi_" + key).val(elem.hora_inicio);
                        $("#hf_" + key).val(elem.hora_fin);
                        $("#hi_" + key + ", #hf_" + key).change(function(event) {
                            loop_horarios_flag();
                        });
                    });
                })
                .fail(function() {
                });
            }
        </script>
    <?php endif; ?>
    <!-- Script -->


    <!-- GoogleMaps -->
    <?php if ( ($this->router->fetch_class() == "cliente" || $this->router->fetch_class() == "admin" ) && ($this->router->fetch_method() == "local" || $this->router->fetch_method() == "turismo") && isset($crud_state) && ( $crud_state == "add" || $crud_state == "edit" || $crud_state == "copy" ) ): ?>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBwJkg4iARxbf2pKiMUDCE9sQIvnzVxCFs&sensor=false" async defer></script>
        <script type="text/javascript" src="<?php echo base_url('public/backend/js/map.js'); ?>"></script>
    <?php endif ?>
    <!-- GoogleMaps -->

    <?php
        if(isset($dropdown_setup)) {
            $this->load->view('dependent_dropdown', $dropdown_setup);
        }
    ?>
    
    </body>
</html>