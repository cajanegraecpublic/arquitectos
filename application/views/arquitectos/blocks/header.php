<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ferreccio :: <?php echo $titlePage; ?> </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">


        <!-- The styles -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/bower_components/fullcalendar/dist/fullcalendar.css'); ?>" >
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/bower_components/fullcalendar/dist/fullcalendar.print.css'); ?>"  media='print'>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/bower_components/chosen/chosen.min.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/bower_components/colorbox/example3/colorbox.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/bower_components/responsive-tables/responsive-tables.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/css/jquery.noty.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/css/noty_theme_default.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/css/elfinder.min.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/css/elfinder.theme.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/css/jquery.iphone.toggle.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/css/uploadify.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/css/animate.min.css'); ?>">

        <!-- Fuentes -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/fonts/fuentes.css'); ?>">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Dosis:300,400,500">
        
        <?php if (isset($css_files)): ?>
            <!-- grocerycrud -->
            <?php foreach($css_files as $file): ?>
                <link rel="stylesheet" type="text/css" href="<?php echo $file; ?>" />
            <?php endforeach; ?>
            <!-- grocerycrud -->
        <?php endif ?>

        <!-- Template -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/css/charisma-app.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/css/custom-bootstrap-margin-padding.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/backend/css/estilo.css'); ?>">

        <!-- The fav icon -->
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('public/backend/img/goapp/favicon/apple-icon-57x57.png'); ?>">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('public/backend/img/goapp/favicon/apple-icon-60x60.png'); ?>">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('public/backend/img/goapp/favicon/apple-icon-72x72.png'); ?>">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('public/backend/img/goapp/favicon/apple-icon-76x76.png'); ?>">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('public/backend/img/goapp/favicon/apple-icon-114x114.png'); ?>">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('public/backend/img/goapp/favicon/apple-icon-120x120.png'); ?>">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('public/backend/img/goapp/favicon/apple-icon-144x144.png'); ?>">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('public/backend/img/goapp/favicon/apple-icon-152x152.png'); ?>">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('public/backend/img/goapp/favicon/apple-icon-180x180.png'); ?>">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url('public/backend/img/goapp/favicon/android-icon-192x192.png'); ?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('public/backend/img/goapp/favicon/favicon-32x32.png'); ?>">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('public/backend/img/goapp/favicon/favicon-96x96.png'); ?>">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('public/backend/img/goapp/favicon/favicon-16x16.png'); ?>">
        <link rel="manifest" href="<?php echo base_url('public/backend/img/goapp/favicon/manifest.json'); ?>">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php echo base_url('public/backend/img/goapp/favicon/ms-icon-144x144.png'); ?>">
        <meta name="theme-color" content="#ffffff">

        <?php if($this->router->method == 'crud_portafolios'): ?>
            <link href="<?php echo base_url('public/backend/css/jquery-confirm.min.css'); ?>" rel="stylesheet" type="text/css">
            <link href="<?php echo base_url('public/backend/css/crud_portafolios.css'); ?>" rel="stylesheet" type="text/css">
        <?php elseif($this->router->method == 'create_portafolio' || $this->router->method == 'update_portafolio'): ?>
            <link href="<?php echo base_url('public/backend/css/jquery-confirm.min.css'); ?>" rel="stylesheet" type="text/css">
            <link href="<?php echo base_url('public/backend/css/insert_portafolio.css'); ?>" rel="stylesheet" type="text/css">
        <?php elseif($this->router->method == 'read_portafolio'): ?>
            <link href="<?php echo base_url('public/backend/css/read_portafolio.css'); ?>" rel="stylesheet" type="text/css">
        <?php endif; ?>

        <script type="text/javascript">
            var base_url = '<?php echo base_url(); ?>';

            var js_site_url = function( urlText ){
                var urlTmp = "<?php echo site_url('" + urlText + "'); ?>";
                return urlTmp;
            }

            var js_base_url = function( urlText ){
                var urlTmp = "<?php echo base_url('" + urlText + "'); ?>";
                return urlTmp;
            }
        </script>

        <script src='https://www.google.com/recaptcha/api.js'></script>

    </head>

<body>