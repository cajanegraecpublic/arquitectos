<!-- topbar starts -->
<div class="navbar navbar-goapp" role="navigation">

    <div class="navbar-inner">
        <button type="button" class="navbar-toggle pull-left animated flip">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand text-center link-goapp goapp-navbar-brand" href="<?php echo site_url('admin/index')?>">
            <img alt="Ferreccio" src="<?php echo base_url('public/frontend/images/logo-header.png'); ?>" class="img-brand-goapp obj-centrar hidden-xs"/>
        </a>

        <!-- user dropdown starts -->
        <div class="btn-group pull-right">
            <button class="btn btn-plano dropdown-toggle" data-toggle="dropdown">
                <i class="glyphicon glyphicon-user"></i>
                <span class="hidden-sm hidden-xs mayusculas"> <?php echo $this->session->user_nombre; ?></span>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <?php /*
                <li>
                    <a href="#">Profile</a>
                </li>
                <li class="divider"></li>
                */ ?>
                <li>
                    <?php echo anchor('admin/logout', 'Cerrar sesión'); ?>
                </li>
            </ul>
        </div>
        <!-- user dropdown ends -->

        <?php /*
        <ul class="collapse navbar-collapse nav navbar-nav top-menu">
            <li>
                <a href="#"><i class="glyphicon glyphicon-globe"></i> Visit Site</a>
            </li>
            <li class="dropdown">
                <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i> Dropdown <span
                        class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                    <li class="divider"></li>
                    <li><a href="#">One more separated link</a></li>
                </ul>
            </li>
            <li>
                <form class="navbar-search pull-left">
                    <input placeholder="Search" class="search-query form-control col-md-10" name="query"
                           type="text">
                </form>
            </li>
        </ul>
        */ ?>

    </div>
</div>
<!-- topbar ends -->

<div class="ch-container"> <!-- inicio ch-container -->
    <div class="row">
        <!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav goapp-sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu main-menu-wb">
                        <li>
                            <a class="" href="<?php echo site_url('admin/index'); ?>">
                                <i class="glyphicon glyphicon-home"></i><span> Inicio</span>
                            </a>
                        </li>
                        <li class="nav-header">Mantenimientos</li>
                        <li>
                            <!-- <a class="" href="<?php //echo site_url('admin/portfolio'); ?>"> -->
                            <a class="" href="<?php echo site_url('admin_portafolios/crud_portafolios'); ?>">
                                <i class="glyphicon glyphicon-list-alt"></i><span> Portfolios</span>
                            </a>
                        </li>
                        <li>
                            <a class="" href="<?php echo site_url('admin/audio'); ?>">
                                <i class="glyphicon glyphicon-list-alt"></i><span> Audio</span>
                            </a>
                        </li>
                        <li>
                            <a class="" href="<?php echo site_url('admin/slideshow'); ?>">
                                <i class="glyphicon glyphicon-list-alt"></i><span> Presentaciones</span>
                            </a>
                        </li>
                        <li>
                            <a class="" href="<?php echo site_url('admin/estudio'); ?>">
                                <i class="glyphicon glyphicon-star"></i><span> Info Estudio</span>
                            </a>
                        </li>
                        <li>
                            <a class="" href="<?php echo site_url('admin/asociado'); ?>">
                                <i class="glyphicon glyphicon-user"></i><span> Lista de asociados</span>
                            </a>
                        </li>
                        <?php /*
                        <li>
                            <a class="" href="<?php echo site_url('admin/razones'); ?>">
                                <i class="glyphicon glyphicon-user"></i><span> Razones</span>
                            </a>
                        </li>
                        */ ?>
                        <li>
                            <a class="" href="<?php echo site_url('admin/footer'); ?>">
                                <i class="glyphicon glyphicon-picture"></i><span> Banners en footer</span>
                            </a>
                        </li>
                        <li>
                            <a class="" href="<?php echo site_url('admin/servicio'); ?>">
                                <i class="glyphicon glyphicon-briefcase"></i><span> Servicios</span>
                            </a>
                        </li>
                        <li>
                            <a class="" href="<?php echo site_url('admin/local'); ?>">
                                <i class="glyphicon glyphicon-map-marker"></i><span> Locales</span>
                            </a>
                        </li>
                        <li>
                            <a class="ajax-link" href="<?php echo site_url('admin/usuario'); ?>">
                                <i class="glyphicon glyphicon-user"></i><span> Administrar Usuarios</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--/span-->
        <!-- left menu ends -->

        <noscript>
            <div class="alert alert-block col-md-12">
                <h4 class="alert-heading">¡Atención!</h4>
                <p>
                    Necesitas tener habilitado <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> para usar este sitio.
                </p>
            </div>
        </noscript>
    