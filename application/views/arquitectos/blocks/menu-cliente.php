<!-- topbar starts -->
<div class="navbar navbar-goapp" role="navigation">

    <div class="navbar-inner">
        <button type="button" class="navbar-toggle pull-left animated flip">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand text-center link-goapp goapp-navbar-brand" href="<?php echo site_url('cliente/index')?>">
            <img alt="Go APP" src="<?php echo base_url('public/img/goapp/logo-blanco.png'); ?>" class="img-brand-goapp obj-centrar hidden-xs"/>
        </a>

        <!-- user dropdown starts -->
        <div class="btn-group pull-right">
            <button class="btn btn-plano dropdown-toggle" data-toggle="dropdown">
                <i class="glyphicon glyphicon-user"></i>
                <span class="hidden-sm hidden-xs mayusculas"> <?php echo $this->session->user_nombre; ?></span>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <?php /*
                <li>
                    <a href="#">Profile</a>
                </li>
                <li class="divider"></li>
                */ ?>
                <li>
                    <?php echo anchor('cliente/logout', 'Cerrar sesión'); ?>
                </li>
            </ul>
        </div>
        <!-- user dropdown ends -->

        <?php /*
        <ul class="collapse navbar-collapse nav navbar-nav top-menu">
            <li>
                <a href="#"><i class="glyphicon glyphicon-globe"></i> Visit Site</a>
            </li>
            <li class="dropdown">
                <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i> Dropdown <span
                        class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                    <li class="divider"></li>
                    <li><a href="#">One more separated link</a></li>
                </ul>
            </li>
            <li>
                <form class="navbar-search pull-left">
                    <input placeholder="Search" class="search-query form-control col-md-10" name="query"
                           type="text">
                </form>
            </li>
        </ul>
        */ ?>

    </div>
</div>
<!-- topbar ends -->

<div class="ch-container"> <!-- inicio ch-container -->
    <div class="row">
        <!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav goapp-sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu main-menu-wb">
                        <li>
                            <a class="" href="<?php echo site_url('cliente/index'); ?>">
                                <i class="glyphicon glyphicon-home"></i><span> Inicio</span>
                            </a>
                        </li>
                        <li>
                            <a class="ajax-link" href="<?php echo site_url('cliente/usuario'); ?>">
                                <i class="glyphicon glyphicon-user"></i><span> Usuarios</span>
                            </a>
                        </li>
                        <li class="nav-header">Clientes</li>
                        <li>
                            <a class="ajax-link" href="<?php echo site_url('cliente/cliente'); ?>">
                                <i class="glyphicon glyphicon-star"></i><span> Datos del Cliente</span>
                            </a>
                        </li>
                        <li>
                            <a class="ajax-link" href="<?php echo site_url('cliente/local'); ?>">
                                <i class="glyphicon glyphicon-map-marker"></i><span> Administrar Locales</span>
                            </a>
                        </li>
                        <li class="nav-header">Promociones</li>
                        <li>
                            <a class="" href="<?php echo site_url('cliente/promo'); ?>">
                                <i class="glyphicon glyphicon-gift"></i><span> Promociones</span>
                            </a>
                        </li>
                        <li class="nav-header">Guía</li>
                        <li>
                            <a class="" href="<?php echo site_url('cliente/guia'); ?>">
                                <i class="glyphicon glyphicon-book"></i><span> Guía de Usuario</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--/span-->
        <!-- left menu ends -->

        <noscript>
            <div class="alert alert-block col-md-12">
                <h4 class="alert-heading">¡Atención!</h4>
                <p>
                    Necesitas tener habilitado <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> para usar este sitio.
                </p>
            </div>
        </noscript>
    