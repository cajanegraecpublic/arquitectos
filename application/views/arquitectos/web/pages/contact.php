<!--========================================================
                              CONTENT
    =========================================================-->
    <main>

    <!-- Contact Info -->
    <section class="well-sm bg-contrast-variant-2 text-sm-left">
        <div class="container light-blue">
            <h3 class="border-primary light-blue-text">
            <?php if ( $this->session->site_lang == "english" ): ?>
                <?php echo "Contact information"; ?>
            <?php else: ?>
                <?php echo "Información de Contacto"; ?>
            <?php endif ?>
            </h3>
            <div class="row">
            <?php 
            foreach ($locales as $local) {?>
                <div class="col-sm-6 col-md-6">
                    <address class="contact-info">
                        <span class="heading-5 light-blue-text">
                        <?php if ( $this->session->site_lang == "english" ): ?>
                            <?php echo $local["direccion_en"]; ?>
                        <?php else: ?>
                            <?php echo $local["direccion"]; ?>
                        <?php endif ?>
                        </span>
                        <dl class="light-gray-text">
                            <dt>
                            <?php if ( $this->session->site_lang == "english" ): ?>
                                <?php echo "Mobile:"; ?>
                            <?php else: ?>
                                <?php echo "Celular:"; ?>
                            <?php endif ?>
                            </dt>
                            <dd>
                                <?php echo $local["celular"]; ?>
                            </dd>
                            <dt>
                            <?php if ( $this->session->site_lang == "english" ): ?>
                                <?php echo "Phone:"; ?>
                            <?php else: ?>
                                <?php echo "Teléfono:"; ?>
                            <?php endif ?>
                            </dt>
                            <dd>
                                <?php echo $local["telefono"]; ?>
                            </dd>
                            <dt>Horario:</dt>
                            <dd>
                                <?php echo $local["horario"]; ?>
                            </dd>
                            <dt class="offset-4">
                            <?php if ( $this->session->site_lang == "english" ): ?>
                                <?php echo "Email:"; ?>
                            <?php else: ?>
                                <?php echo "Correo:"; ?>
                            <?php endif ?>
                            </dt>
                            <dd>
                                <a href="mailto:<?php echo $local["email"]; ?>"><?php echo $local["email"]; ?></a>
                            </dd>
                        </dl>
                    </address>
                </div>
            <?php }
             ?>
            </div>
        </div>    
    </section>
    <!-- END Contact Info -->

    <!-- Google map -->
    <section id="map" class="map" >
        <div id="google-map" class="map_model" data-zoom="12" data-x="<?php echo $locales[0]['longitude']; ?>" data-y="<?php echo $locales[0]['latitude']; ?>">
        </div>
        <ul class="map_locations">
            <?php foreach ($locales as $local): ?>
            <li data-x="<?php echo $local['longitude']; ?>" data-y="<?php echo $local['latitude']; ?>">
                <p><?php echo $local["direccion"]; ?></p>
            </li>
            <?php endforeach; ?>
        </ul>
    </section>
    <!-- END Google map -->

    <!-- RD Mailform -->
    <section class="well-sm well-sm--inset-4 text-md-left">
        <div class="container light-blue">
            <h3 class="border-primary light-blue-text">
            <?php if ( $this->session->site_lang == "english" ): ?>
                <?php echo "How can we help you?"; ?>
            <?php else: ?>
                <?php echo "Escríbenos"; ?>
            <?php endif ?>
            </h3>
            <div class="row inset-5">
                <form class='mailform rd-mailform' method="post" action="">
                    <input type="hidden" name="form-type" value="contact"/>
                    <fieldset>
                        <div class="col-sm-6 col-md-6">
                            <label data-add-placeholder="">
                                <input id="nombre"
                                       type="text"
                                       name="name"
                                       placeholder="Nombre"
                                       data-constraints="@LettersOnly @NotEmpty"/>
                            </label>     
                        </div>

                        <div class="col-sm-6 col-md-6">
                            <label data-add-placeholder="">
                                <input id="email" 
                                       type="text"
                                       name="email"
                                       placeholder="Email"
                                       data-constraints="@Email @NotEmpty"/>
                            </label>
                        </div>
                        
                        <div class="col-xs-12">
                            <label data-add-placeholder="">
                                <textarea id="mensaje" name="message" placeholder="Mensaje"
                                          data-constraints="@NotEmpty"></textarea>
                            </label>
                        </div>

                        <div class="col-xs-12">
                            <div class="mfControls">
                                <button id="btn_submit" class="btn btn-lg btn-default-variant-1" type="button">
                                <?php if ( $this->session->site_lang == "english" ): ?>
                                    <?php echo "Send"; ?>
                                <?php else: ?>
                                    <?php echo "Enviar"; ?>
                                <?php endif ?>
                                </button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </section>
    <!-- END RD Mailform -->

    </main>