<!--========================================================
                              CONTENT
    =========================================================-->
    <main>

        <!-- Camera -->
        <?php //print_r($slideshows); ?>
        <div class="camera_container text-lg-left">
            <div id="camera" class="camera_wrap">
                <?php foreach ($slideshows as $slideshow): ?>
                    <div data-src="<?php echo base_url('assets/uploads/slideshow_fondo' . '/' . $slideshow['fondo']); ?>">
                        <div class="camera_caption fadeIn">
                            <div class="container true-position">
                                <div class="row">
                                    <div class="col-lg-5 pl-60">
                                        <h1 class="max-width-1">
                                        <?php if ( $this->session->site_lang == "english" ): ?>
                                            <?php echo $slideshow["titulo_en"]; ?>
                                        <?php else: ?>
                                            <?php echo $slideshow["titulo"]; ?>
                                        <?php endif ?>
                                        </h1>
                                        <p class="small text-contrast">
                                        <?php if ( $this->session->site_lang == "english" ): ?>
                                            <?php echo $slideshow["caption_en"]; ?>
                                        <?php else: ?>
                                            <?php echo $slideshow["caption"]; ?>
                                        <?php endif ?>
                                        </p>
                                        <!-- <a href="#" class="btn btn-md btn-contrast">Saber más</a> -->
                                        <!-- <p class="current-slider">1 <span>/</span> 3</p> -->
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="divider bg-primary"></div>
                                    </div>
                                    <div class="col-lg-5 relative">
                                        <img class="image-wrap image-responsive" src="<?php echo base_url('assets/uploads/slideshow_img' . '/' .  $slideshow['imagen']); ?>" alt="">
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
        <!-- Camera -->

        <!-- Hello! -->
        <section class="bg-width text-sm-left offset-6">
            <div class="well-md well-md--inset-1">
                <div class="container pl-60">
                    <h3 class="border-contrast">
                        <?php if ( $this->session->site_lang == "english" ): ?>
                            <?php echo "HELLO!"; ?>
                        <?php else: ?>
                            <?php echo "¡Saludos!"; ?>
                        <?php endif ?>
                    </h3>
                    <div class="row">
                        <div class="col-sm-6 col-md-5">
                            <?php if ( $this->session->site_lang == "english" ): ?>
                                <?php echo $estudios[0]["saludo_en"] ?>
                            <?php else: ?>
                                <?php echo $estudios[0]["saludo"] ?>
                            <?php endif ?>
                        </div>
                        <div class="col-sm-6 col-md-7">
                            <div class="owl-carousel offset-2">
                                <div class="item">
                                    <img src="<?php echo base_url('public/frontend/images/page-1_img04.jpg'); ?>" alt=""/>
                                </div>
                                <div class="item">
                                    <img src="<?php echo base_url('public/frontend/images/page-1_img14.jpg'); ?>" alt=""/>
                                </div>
                                <div class="item">
                                    <img src="<?php echo base_url('public/frontend/images/page-1_img15.jpg'); ?>" alt=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END Hello! -->

        <!-- Portfolio -->
        
        <section>
        <div class="row row-no-gutter">
            <?php foreach ($portafolios as $portafolio): ?>
                <div class="col-md-3 col-lg-3">
                    <a class="product" href="<?php echo site_url('web/portfolio'); ?>">
                        <img src="<?php echo base_url('assets/uploads/portfolio' . '/' . $portafolio['imagen']); ?>" alt=""/>
                        <span class="product_overlay_tall"></span>
                    </a>
                </div>
            <?php endforeach ?>
        </div>
        </section>
        <!-- END Portfolio -->

        <!-- RD Parallax -->
        <section class="parallax well-lg" data-url="<?php echo base_url('assets/uploads/footer' . '/' . $footers[0]["fondo"]);?>" data-mobile="true">
            <div class="container-fluid text-sm-left">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-6">
                        <div class="fondo-gris-transparente p-20">
                            <h1 class="pseudo-big">
                                <?php if ( $this->session->site_lang == "english" ): ?>
                                    <?php echo $footers[0]["titulo_en"]; ?>
                                <?php else: ?>
                                    <?php echo $footers[0]["titulo"]; ?>
                                <?php endif ?>
                            </h1>
                            <p class="small text-contrast">
                                <?php if ( $this->session->site_lang == "english" ): ?>
                                    <?php echo $footers[0]["descripcion_en"]; ?>
                                <?php else: ?>
                                    <?php echo $footers[0]["descripcion"]; ?>
                                <?php endif ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- END RD Parallax -->
        <!-- Solutions -->
        <section class="well-sm bg-contrast-variant-1 text-md-left">
            <div class="container blue">
                <h3 class="border-primary blue-text">
                <?php if ( $this->session->site_lang == "english" ): ?>
                    <?php echo "Solutions"; ?>
                <?php else: ?>
                    <?php echo "Soluciones"; ?>
                <?php endif ?>
                </h3>

                <div class="row">
                    <?php foreach ($servicios as $servicio): ?>
                        <div class="col-md-6 col-lg-4">
                            <div class="box-md">
                                <div class="box_left inset-7">
                                    <span class="icon icon-md icon-primary <?php echo $servicio["nombre_icono"] ?> blue-text"></span>
                                </div>
                                <div class="box_cnt">
                                    <!-- 
                                    <a href="#" class="heading-5 link-primary blue-text">Environmental Solution</a> -->
                                    <h5 class="blue-text">
                                        <?php if ( $this->session->site_lang == "english" ): ?>
                                            <?php echo $servicio["titulo_en"] ?>
                                        <?php else: ?>
                                            <?php echo $servicio["titulo"] ?>
                                        <?php endif ?>
                                    </h5>

                                    <p class="inset-5">
                                        <?php if ( $this->session->site_lang == "english" ): ?>
                                            <?php echo $servicio["descripcion_en"] ?>
                                        <?php else: ?>
                                            <?php echo $servicio["descripcion"] ?>
                                        <?php endif ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </section>
        <!-- END Solutions -->

        <!-- RD Mailform -->
        <section class="well-xs text-md-left">
            <div class="container light-blue">
                <h3 class="border-primary light-blue-text">
                <?php if ( $this->session->site_lang == "english" ): ?>
                    <?php echo "Contact us"; ?>
                <?php else: ?>
                    <?php echo "contáctenos"; ?>
                <?php endif ?>
                </h3>
                <div class="row inset-5"> 
                    <?php
                        $placeholderNombre = "";
                        $placeholderEmail = "";
                        $placeholderMensaje = "";

                        if ( $this->session->site_lang == "english" ) {
                            $placeholderNombre = "Name";
                            $placeholderEmail = "Email";
                            $placeholderMensaje = "Message";
                        }else{
                            $placeholderNombre = "Nombre";
                            $placeholderEmail = "Email";
                            $placeholderMensaje = "Mensaje";
                        }
                    ?>
                    <form class='mailform rd-mailform' method="post" action="">
                        <input type="hidden" name="form-type" value="contact"/>
                        <fieldset>
                            <div class="col-sm-6 col-md-6">
                                <label data-add-placeholder="">
                                    <input id="nombre"
                                           type="text"
                                           name="name"
                                           placeholder="<?php echo $placeholderNombre; ?>"
                                           data-constraints="@LettersOnly @NotEmpty"/>
                                </label>     
                            </div>

                            <div class="col-sm-6 col-md-6">
                                <label data-add-placeholder="">
                                    <input id="email"
                                           type="text"
                                           name="email"
                                           placeholder="<?php echo $placeholderEmail; ?>"
                                           data-constraints="@Email @NotEmpty"/>
                                </label>
                            </div>
                            
                            <div class="col-xs-12">
                                <label data-add-placeholder="">
                                    <textarea id="mensaje" name="message" placeholder="<?php echo $placeholderMensaje; ?>"
                                              data-constraints="@NotEmpty"></textarea>
                                </label>
                            </div>

                            <div class="col-xs-12">
                                <div class="mfControls">
                                    <button id="btn_submit" class="btn btn-lg btn-default-variant-1" type="button">
                                    <?php if ( $this->session->site_lang == "english" ): ?>
                                        <?php echo "Send"; ?>
                                    <?php else: ?>
                                        <?php echo "Enviar"; ?>
                                    <?php endif ?>
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </section>
        <!-- END RD Mailform -->

        <?php /*
        <!-- Google map
        <section id="map" class="map" >
            <div id="google-map" class="map_model" data-zoom="12" data-x="-74.008592" data-y="40.682597">
            </div>
            <ul class="map_locations">
                <li data-x="-73.973584" data-y="40.684763">
                    <p>8901 Marmora Road, Glasgow</p>
                </li>
            </ul>
        </section>
         END Google map -->
        */ ?>

    </main>