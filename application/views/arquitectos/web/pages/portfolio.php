<!--========================================================
                              CONTENT
    =========================================================-->
    <main>

        <!-- Portfolio -->
        <section class="well-sm well-sm--inset-1 text-sm-left">
            <div class="container blue">
                <h3 class="border-primary blue-text">
                <?php if ( $this->session->site_lang == "english" ): ?>
                    <?php echo "Our portfolio"; ?>
                <?php else: ?>
                    <?php echo "Nuestro portafolio"; ?>
                <?php endif ?>
                </h3>
            </div>
            <div class="row row-no-gutter">
                <?php foreach ($portafolios as $portafolioItem): ?>
                    <div class="col-lg-4">
                        <?php
                            $textoTitulo = "";
                            $textoDescripcion = "";
                            if ( $this->session->site_lang == "english" ){
                                $textoTitulo = $portafolioItem['titulo_en'];
                                $textoDescripcion = $portafolioItem['descripcion_en'];
                            }else{
                                $textoTitulo = $portafolioItem['titulo'];
                                $textoDescripcion = $portafolioItem['descripcion'];
                            }
                        ?>
                        <div class="thumbnail">
                            <img src="<?php echo base_url('assets/uploads/portfolio' . '/' . $portafolioItem['imagen']); ?>" alt="<?php echo $textoTitulo; ?>" class="img-portfolio-preview" />
                            <div class="thumbnail_overlay">
                                <div class="caption">
                                    <div class="arq-galeria">
                                        <?php foreach ($portafolioItem['imagenes'] as $indice => $portafolioImagen): ?>
                                            <?php $displayIcono = ($indice == 0) ? '' : 'display: none;' ; ?>
                                            <a
                                                href="<?php echo base_url('assets/uploads/portfolio_imagenes' . '/' . $portafolioImagen['nombre_archivo']); ?>"
                                                data-fancybox-group='<?php echo $portafolioItem['id']; ?>'
                                                data-gallery="<?php echo $portafolioItem['id']; ?>"
                                                class="thumb icon icon-sm icon-contrast material-design-rounded55 arq-galeria-item"
                                                style="<?php echo $displayIcono; ?>"></a>
                                        <?php endforeach ?>
                                    </div>
                                    <h4 class="mb-0">
                                        <?php echo $textoTitulo; ?>
                                    </h4>
                                    <?php if ($textoDescripcion != ""): ?>
                                        <p class="portfolio-p">
                                            <?php echo $textoDescripcion; ?>
                                        </p>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </section>
        <!-- END Portfolio -->

         <!-- RD Parallax -->
         <section class="parallax well-lg" data-url="<?php echo base_url('assets/uploads/footer/'); echo $footers[0]["fondo"];?>" data-mobile="true">
            <div class="container text-sm-left">
                <div class="row">
                    <div class="col-lg-8">
                        <h1 class="pseudo-big">
                        <?php if ( $this->session->site_lang == "english" ): ?>
                            <?php echo $footers[0]["titulo_en"]; ?>
                        <?php else: ?>
                            <?php echo $footers[0]["titulo"]; ?>
                        <?php endif ?>
                        </h1>
                        <p class="small text-contrast">
                        <?php if ( $this->session->site_lang == "english" ): ?>
                            <?php echo $footers[0]["descripcion_en"]; ?>
                        <?php else: ?>
                            <?php echo $footers[0]["descripcion"]; ?>
                        <?php endif ?>
                        </p>
                    </div>
                </div>
            </div>
        </section>
         <!-- END RD Parallax -->

    </main>