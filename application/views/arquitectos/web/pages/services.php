<!--========================================================
                              CONTENT
    =========================================================-->
    <main>

        <!-- Boxes -->
        <section class="well-sm bg-contrast-variant-2 text-sm-left">
            <div class="container light-blue">
                <h3 class="border-primary light-blue-text">nuestros servicios</h3>
                <div class="offset-5 row flow-offset-2">
                <?php 
                foreach ($servicios as $servicio) {?>
                    <div class="col-xs-6 col-md-4">
                        <div class="box-md">
                            <div class="box_left inset-7">
                                <span class="icon icon-md icon-primary <?php echo $servicio["nombre_icono"] ?> light-blue-text"></span>
                            </div>
                            <div class="box_cnt">
                                <h5 class="light-blue-text"><?php echo $servicio["titulo"] ?></h5>
                                <p><?php echo $servicio["descripcion"] ?></p>
                            </div>
                        </div>
                    </div>
                <?php }

                 ?>
                    <!-- <div class="col-xs-6 col-md-4">
                        <div class="box-md">
                            <div class="box_left inset-7">
                                <span class="icon icon-md icon-primary material-icons-account_balance light-blue-text"></span>
                            </div>
                            <div class="box_cnt">
                                <h5 class="light-blue-text">Construcción</h5>
                                <p>Lorem Ipsum Lorem Ipsum</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <div class="box-md">
                            <div class="box_left inset-7">
                                <span class="icon icon-md icon-primary material-icons-extension light-blue-text"></span>
                            </div>
                            <div class="box_cnt">
                                <h5 class="light-blue-text">Renovación</h5>
                                <p>Lorem Ipsum Lorem Ipsum</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <div class="box-md">
                            <div class="box_left inset-7">
                                <span class="icon icon-md icon-primary material-icons-event_seat light-blue-text"></span>
                            </div>
                            <div class="box_cnt">
                                <h5 class="light-blue-text">Arquitectura Interior</h5>
                                <p>Lorem Ipsum Lorem Ipsum</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <div class="box-md">
                            <div class="box_left inset-7">
                                <span class="icon icon-md icon-primary material-icons-layers light-blue-text"></span>
                            </div>
                            <div class="box_cnt">
                                <h5 class="light-blue-text">Diseño Funcional</h5>
                                <p>Lorem Ipsum Lorem Ipsum</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-xs-clear col-sm-release col-md-4">
                        <div class="box-md">
                            <div class="box_left inset-7">
                                <span class="icon icon-md icon-primary material-icons-laptop_windows light-blue-text"></span>
                            </div>
                            <div class="box_cnt">
                                <h5 class="light-blue-text">Diseño Experimental</h5>
                                <p>Lorem Ipsum Lorem Ipsum</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <div class="box-md">
                            <div class="box_left inset-7">
                                <span class="icon icon-md icon-primary material-icons-card_travel light-blue-text"></span>
                            </div>
                            <div class="box_cnt">
                                <h5 class="light-blue-text">Planificación</h5>
                                <p>Lorem Ipsum Lorem Ipsum</p>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>
        <!-- END Boxes -->

        <!-- Why choose us? -->
        <section class="bg-width-2 light-blue-bg-width text-sm-left bg-image bg-image-left bg-image-left-2">
            <div class="well-md well-md--inset-1">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-5 col-md-offset-2">
                            <img class="offset-3 custom-offset-5" src="<?php echo base_url('public/images/page-4_img01.jpg'); ?>" alt=""/>
                        </div>
                        <div class="col-sm-6 col-md-5">
                            <h3 class="border-contrast custom-font-size-43">¿Por qué elegirnos?</h3>
                            <ul class="marked-list marked-list-contrast">
                                <?php 
                                foreach ($razones as $razon) { ?>
                                    <li><span class="fa-check"></span> <?php echo $razon["titulo"]; ?></li>                          
                                <?php } ?>
                                <!-- <li><span class="fa-check"></span> Lorem Ipsum</li>
                                <li><span class="fa-check"></span> Lorem Ipsum</li>
                                <li><span class="fa-check"></span> Lorem Ipsum</li>
                                <li><span class="fa-check"></span> Lorem Ipsum</li>
                                <li><span class="fa-check"></span> Lorem Ipsum</li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END Why choose us? -->

        <!-- RD Parallax -->
        <section class="parallax well-lg" data-url="<?php echo base_url('assets/uploads/footer/'); echo $footers[0]["fondo"];?>" data-mobile="true">
            <div class="container text-sm-left">
                <div class="row">
                    <div class="col-lg-8">
                        <h1 class="pseudo-big"><?php echo $footers[0]["titulo"]; ?></h1>
                        <p class="small text-contrast"><?php echo $footers[0]["descripcion"]; ?></p>
                    </div>
                </div>
            </div>
        </section>
         <!-- END RD Parallax -->

    </main>