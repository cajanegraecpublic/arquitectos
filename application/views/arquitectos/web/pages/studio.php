<!--========================================================
                          CONTENT
=========================================================-->
<main>
<?php 
// $estudios
$estudio=$estudios[0];
// print_r($estudio);
 ?>
    <style type="text/css">
        .fondo-acerca::after {
          background: url("<?php echo base_url('assets/uploads/fondo_acerca' . '/' . $estudio['fondo_acerca']); ?>") no-repeat scroll 0% 0%/cover;
        }
        .fondo-vision::after {
          background: url("<?php echo base_url('assets/uploads/fondo_vision' . '/' . $estudio['fondo_vision']); ?>") no-repeat scroll 0% 0%/cover;
        }
        .fondo-mision::after {
          background: url("<?php echo base_url('assets/uploads/fondo_mision' . '/' . $estudio['fondo_mision']); ?>") no-repeat scroll 0% 0%/cover;
        }
        .fondo-ventajas::after {
          background: url("<?php echo base_url('assets/uploads/fondo_ventajas' . '/' . $estudio['fondo_ventajas']); ?>") no-repeat scroll 0% 0%/cover;
        }
    </style>
    <!-- In a Nutshell About Us -->
    <section class="well-sm well-sm--inset-1 text-sm-left bg-contrast-variant-2">
        <div class="container blue">
            <h3 class="border-primary blue-text">
            <?php if ( $this->session->site_lang == "english" ): ?>
                <?php echo "About us"; ?>
            <?php else: ?>
                <?php echo "Una pizca sobre nosotros"; ?>
            <?php endif ?>
            </h3>
            <div class="row">
                <div class="col-md-5">
                <?php  /*
                    <img src="<?php //echo base_url('public/frontend/images/page-2_img01.png'); ?>" alt="">
                    */?>
                    <img src="<?php echo base_url('assets/uploads/imagen_acerca' . '/' . $estudio['imagen_acerca']); ?>" alt="">
                </div>
                <div class="col-md-7">
                    <p>
                        <?php if ( $this->session->site_lang == "english" ): ?>
                            <?php echo $estudio["acerca_en"]; ?>
                        <?php else: ?>
                            <?php echo $estudio["acerca"]; ?>
                        <?php endif ?>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- END In a Nutshell About Us -->

    <!-- Our mission -->
    <section class="bg-width light-blue-bg-width text-sm-left bg-image bg-image-left fondo-mision">
        <div class="well-md well-md--inset-1">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-5">
                        <h3 class="border-contrast">
                            <?php if ( $this->session->site_lang == "english" ): ?>
                                <?php echo "Our mision"; ?>
                            <?php else: ?>
                                <?php echo "nuestra misión"; ?>
                            <?php endif ?>
                        </h3>
                        <p class="mt-20">
                            <?php if ( $this->session->site_lang == "english" ): ?>
                                <?php echo $estudio["mision_en"]; ?>
                            <?php else: ?>
                                <?php echo $estudio["mision"]; ?>
                            <?php endif ?>
                        </p>
                    </div>
                    <div class="col-sm-6 col-md-7">
                        <?php  /*
                        <img class="offset-2" src="<?php //echo base_url('public/frontend/images/page-2_img02.jpg'); ?>" alt=""/>
                        */?>
                        <img class="offset-2" src="<?php echo base_url('assets/uploads/imagen_mision' . '/' .  $estudio['imagen_mision']); ?>" alt=""/>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END Our mission -->

    <!-- Our Goal -->
    <section class="bg-width-2 light-blue-bg-width text-sm-left bg-image bg-image-left fondo-vision">
        <div class="well-md well-md--inset-1">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-5 col-md-offset-2">
                        <?php  /*
                        <img class="offset-3 custom-offset-5" src="<?php //echo base_url('public/frontend/images/page-2_img03.jpg'); ?>" alt=""/>
                        */?>
                        <img class="offset-2" src="<?php echo base_url('assets/uploads/imagen_vision' . '/' .  $estudio['imagen_vision']); ?>" alt="" />
                    </div>
                    <div class="col-sm-6 col-md-5">
                        <h3 class="border-contrast">
                            <?php if ( $this->session->site_lang == "english" ): ?>
                                <?php echo "Our vision"; ?>
                            <?php else: ?>
                                <?php echo "nuestra visión"; ?>
                            <?php endif ?>
                        </h3>
                        <p  class="mt-20">
                            <?php if ( $this->session->site_lang == "english" ): ?>
                                <?php echo $estudio["vision_en"]; ?>
                            <?php else: ?>
                                <?php echo $estudio["vision"]; ?>
                            <?php endif ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END Our Goal -->
<?php //print_r($footers[0]); ?>
    <!-- RD Parallax -->
    <section class="parallax well-lg" data-url="<?php echo base_url('assets/uploads/footer' . '/' . $footers[0]['fondo']);?>" data-mobile="true">
        <div class="container text-sm-left">
            <div class="row">
                <div class="col-lg-8">
                    <h1 class="pseudo-big">
                    <?php if ( $this->session->site_lang == "english" ): ?>
                        <?php echo $footers[0]["titulo_en"]; ?>
                    <?php else: ?>
                        <?php echo $footers[0]["titulo"]; ?>
                    <?php endif ?>
                    </h1>
                    <p class="small text-contrast">
                    <?php if ( $this->session->site_lang == "english" ): ?>
                        <?php echo $footers[0]["descripcion_en"]; ?>
                    <?php else: ?>
                        <?php echo $footers[0]["descripcion"]; ?>
                    <?php endif ?>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- END RD Parallax -->

    <!-- Work team -->
     <section class="well-sm well-sm--inset-2 text-sm-left">
        <div class="container blue">
            <h3 class="border-primary blue-text">
            <?php if ( $this->session->site_lang == "english" ): ?>
                <?php echo "Associates"; ?>
            <?php else: ?>
                <?php echo "Asociados"; ?>
            <?php endif ?>
            </h3>
        </div>
        <div class="separator"></div>
        <div class="container-fluid">
            <div class="row">
                <?php foreach ($asociados as $asociado): ?>
                    <div class="col-xs-6 col-md-3 text-right wow fadeIn" data-wow-delay="0.8s" data-wow-duration="2s">
                        <img src="<?php echo base_url('assets/uploads/asociado' . '/' . $asociado['imagen']); ?>" alt="">
                        <h5 class="label" >
                            <?php if ( $this->session->site_lang == "english" ): ?>
                                <?php echo $asociado["titulo"]; ?>
                            <?php else: ?>
                                <?php echo $asociado["titulo"]; ?>
                            <?php endif ?>
                        </h5>
                        <p class="studio-mark">
                        <?php if ( $this->session->site_lang == "english" ): ?>
                            <?php echo $asociado["descripcion_en"]; ?>
                        <?php else: ?>
                            <?php echo $asociado["descripcion"]; ?>
                        <?php endif ?>
                        </p>
                        <hr class = 'hr-asociado'>
                        <?php if ( $asociado["pdf"] != "" ): ?>
                            <a target="_blank" class = 'cv-asociado' href="<?php echo base_url('assets/uploads/asociado-pdf' . '/' . $asociado['pdf'] ); ?>">
                                <?php if ( $this->session->site_lang == "english" ): ?>
                                   Check the C.V
                                <?php else: ?>
                                     Mira su C.V
                                <?php endif ?>
                            </a>
                        <?php endif ?>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
     </section> 
     <!-- END Work team -->

    <!-- Advantages -->
    <section class="bg-width text-sm-left bg-image bg-image-right fondo-ventajas">
        <div class="well-md well-md--inset-1">
            <div class="container">
                <h3 class="border-contrast">Ventajas</h3>
                <div class="row">
                    <div class="col-sm-6 col-md-5">
                        <?php if ( $this->session->site_lang == "english" ): ?>
                            <?php echo $estudio["ventajas_en"]; ?>
                        <?php else: ?>
                            <?php echo $estudio["ventajas"]; ?>
                        <?php endif ?>
                    </div>
                    <div class="col-sm-6 col-md-5">
                        <?php  /*
                        <img class="offset-3 custom-offset-5" src="<?php echo base_url('public/frontend/images/page-2_img10.jpg'); ?>" alt=""/>
                        */?>
                        <img class="offset-3 custom-offset-5" src="<?php echo base_url('assets/uploads/imagen_ventajas' . '/' . $estudio['imagen_ventajas']); ?>" alt="" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END Advantages -->

</main>