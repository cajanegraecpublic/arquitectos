        <!--========================================================
                                  FOOTER
        =========================================================-->
        <footer>
            <div class="container text-sm-left custom-footer light-gray-text">
                <div class="copyright">
                    Ferreccio © <span id="copyright-year"></span>&middot; 
                    <?php if ( $this->session->site_lang == "english" ): ?>
                        <?php echo "All rights reserved"; ?>
                    <?php else: ?>
                        <?php echo "Derechos Reservados"; ?>
                    <?php endif ?>
                </div>
            </div>
        </footer>
    </div>

    <script type="text/javascript" src="<?php echo base_url('public/frontend/js/lightaudio/audioplayer.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/frontend/js/script.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/frontend/js/script-custom.js'); ?>"></script>
    <?php if ($this->router->fetch_class() == "web" && ($this->router->fetch_method() == "index" || $this->router->fetch_method() == "contact")): ?>
    <script>
        $("#btn_submit").click(function(event) {
            $.ajax({
                url: js_site_url('web/enviarContactoWeb'),
                type: 'POST',
                data: {nombre: $("#nombre").val(), email: $("#email").val(), mensaje: $("#mensaje").val()},
            })
            .done(function(data) {
                if (data.codigo == 1) {
                    alert("Mensaje enviado exitosamente");
                    location.reload(true);
                } else {
                    alert("Hubo un error al enviar el mensaje");
                }
            })
            .fail(function() {
                alert("Hubo un error al enviar el mensaje");
            });
        });
    </script>
    <?php endif; ?>
</body>
</html>