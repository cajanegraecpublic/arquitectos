<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title ?></title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no"/>
    <link rel="icon" href="<?php echo base_url('public/frontend/images/favicon.ico'); ?>" type="image/x-icon">

    <!-- Links -->
    <link rel="stylesheet" href="<?php echo base_url('public/frontend/css/grid.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/frontend/css/lightaudio/reset.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/frontend/css/lightaudio/style.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/frontend/css/lightaudio/audioplayer.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/frontend/css/style.css'); ?>">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,300,500">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Montserrat">
    <link rel="stylesheet" href="<?php echo base_url('public/frontend/css/font-awesome.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/frontend/css/material-icons.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/frontend/css/material-design.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/frontend/css/cn-ferreccio-icons.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/frontend/css/custom-bootstrap-margin-padding.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/frontend/css/camera.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/frontend/css/owl-carousel.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/frontend/css/mailform.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/frontend/css/google-map.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/frontend/css/search.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/frontend/css/style-custom.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/frontend/css/touch-touch.css'); ?>">
    <!-- END Links -->

    <!-- JS -->
    <script src="<?php echo base_url('public/frontend/js/jquery.js'); ?>"></script>
    <script src="<?php echo base_url('public/frontend/js/jquery-migrate-1.2.1.js'); ?>"></script>
    <!-- END JS -->

    <!--[if lt IE 9]>
    <html class="lt-ie9">
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
            <img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"
                 alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
        </a>
    </div>
    <script src="js/html5shiv.js"></script>
    <![endif]-->

    <script src='<?php echo base_url('public/frontend/js/device.min.js'); ?>'></script>

    <script type="text/javascript">
        var base_url = '<?php echo base_url(); ?>';

        var js_site_url = function( urlText ){
            var urlTmp = "<?php echo site_url('" + urlText + "'); ?>";
            return urlTmp;
        }

        var js_base_url = function( urlText ){
            var urlTmp = "<?php echo base_url('" + urlText + "'); ?>";
            return urlTmp;
        }
    </script>
</head>
<body>
<div class="page text-center">
    <!--========================================================
                              HEADER
    =========================================================-->

    <header>
        <div id="stuck_container" class="stuck_container">
            <?php /*
            <!-- Brand -->
            <!-- <div class="rd-navbar-brand">
                <div class="rd-navbar-brand__name">
                    <a href="./">ferreccio</a>
                </div>
            </div> -->
            */ ?>
            <div class="rd-navbar-brand">
                
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="logo img-responsive">
                                <img src="<?php echo base_url('public/frontend/images/logo-header.png') ?>">
                            </div>
                        </div>
                    </div>
               
            </div>
            <!-- END Brand -->

            <!-- RD Navbar -->
            <nav class="nav">
                <ul class="sf-menu" data-type="navbar">
                    <li class="<?php echo ($this->router->fetch_method() == 'index') ? 'active' : '' ; ?>">
                        <a href="<?php echo site_url('web/index'); ?>">
                            <?php if ( $this->session->site_lang == "english" ): ?>
                                <?php echo "Home"; ?>
                            <?php else: ?>
                                <?php echo "Inicio"; ?>
                            <?php endif ?>
                        </a>
                    </li>
                    <li class="<?php echo ($this->router->fetch_method() == 'studio') ? 'active' : '' ; ?>">
                        <a href="<?php echo site_url('web/studio'); ?>">
                            <?php if ( $this->session->site_lang == "english" ): ?>
                                <?php echo "Studio"; ?>
                            <?php else: ?>
                                <?php echo "Estudio"; ?>
                            <?php endif ?>
                        </a>
                    </li>
                    <li class="<?php echo ($this->router->fetch_method() == 'portfolio') ? 'active' : '' ; ?>">
                        <a href="<?php echo site_url('web/portfolio'); ?>">
                            <?php if ( $this->session->site_lang == "english" ): ?>
                                <?php echo "Portfolio"; ?>
                            <?php else: ?>
                                <?php echo "Portafolio"; ?>
                            <?php endif ?>
                        </a>
                    </li>
                    <li class="<?php echo ($this->router->fetch_method() == 'contact') ? 'active' : '' ; ?>">
                        <a href="<?php echo site_url('web/contact') ?>">
                        <?php if ( $this->session->site_lang == "english" ): ?>
                                <?php echo "Contact us"; ?>
                            <?php else: ?>
                                <?php echo "Contacto"; ?>
                            <?php endif ?>
                        </a>
                    </li>
                    <li>
                        <?php if ( $this->session->site_lang == "english" ): ?>
                          <a class="btn btn-default" role="button" href='<?php echo site_url('web/switchLanguage/spanish')?>'><i class="fa fa-language" aria-hidden="true"></i> Español</a>
                        <?php else: ?>
                          <a class="btn btn-default" role="button" href='<?php echo site_url('web/switchLanguage/english')?>'><i class="fa fa-language" aria-hidden="true"></i> English</a>
                        <?php endif ?>
                    </li>
                </ul>
            </nav>
            <!-- END RD Navbar -->
        </div>
    </header>
    <?php if (isset($audio)): ?>
    <div class="row flotante">
        <div class="col-sm-4 col-md-3 sidebar sideMusic">
            <div class="mini-submenu">
                <i class="fa fa-music" aria-hidden="true"></i>
            </div>
            <div class="list-group">
                <span href="#" class="list-group-item active">
                    <?php echo $audio["nombre"] ?>
                    <span class="pull-right" id="slide-submenu">
                        <i class="fa fa-times"></i>
                    </span>
                </span>
                <div id="wrapper">
                    <audio id="audioArq" src="<?php echo $audio['archivo']; ?>" preload="auto" controls ></audio>
                    <?php /*
                    <!-- <script src="js/jquery.min.js"></script> -->
                    <!-- <script src="js/audioplayer.js"></script> -->                    
                    */ ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif ?>
