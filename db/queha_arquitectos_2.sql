-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 21-06-2018 a las 13:26:04
-- Versión del servidor: 5.5.51-38.2
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `queha_arquitectos_2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `apellido` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`username`, `password`, `email`, `nombre`, `apellido`) VALUES
('admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'madelyne@cajanegra.com.ec', 'Madelyne', 'Velasco'),
('admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'madelyne@cajanegra.com.ec', 'Madelyne', 'Velasco'),
('admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'madelyne@cajanegra.com.ec', 'Madelyne', 'Velasco');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asociado`
--

CREATE TABLE `asociado` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `descripcion` text NOT NULL,
  `descripcion_en` text NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `pdf` varchar(200) DEFAULT NULL,
  `estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `asociado`
--

INSERT INTO `asociado` (`id`, `titulo`, `descripcion`, `descripcion_en`, `imagen`, `pdf`, `estado`) VALUES
(2, 'Milton Rojas', 'Arquitecto. Magister en Planeamiento Urbano\r\n', 'Arquitecto. Magister en Planeamiento Urbano', 'd2e51-rojas.jpg', '5a2a2-curriculum-actualizado-arq.-milton.pdf', 1),
(3, 'Álvaro Valladares', '<p>\r\n	Arquitecto, especializado en construcciones en barro.</p>\r\n', '<p>\r\n	Arquitecto, especializado en construcciones en barro.</p>\r\n', 'd086a-alvaro-valladares.jpg', NULL, 0),
(4, 'Carlos Guerrero Ferreccio', 'Arquitecto. Magister de Proyectos Arquitectónicos', 'Architect. Magister de Proyectos Arquitectónicos', '3c366-ferreccio.jpg', '7ac97-curriculum-actualizado-carlos-guerrero.pdf', 1),
(5, 'Robinson Vega', 'Arquitecto. Magister en proyectos arquitectónicos, especialista en estructuras de bambú.\r\n', 'Arquitecto. Magister en proyectos arquitectónicos, especialista en estructuras de bambú.', '73aaf-vega.jpg', 'c2203-curriculum-actualizado-arq.-robinson-vega.pdf', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audio`
--

CREATE TABLE `audio` (
  `id` int(11) NOT NULL,
  `nombre` varchar(400) NOT NULL,
  `archivo` varchar(500) NOT NULL,
  `estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `audio`
--

INSERT INTO `audio` (`id`, `nombre`, `archivo`, `estado`) VALUES
(1, 'Cancion Uno', '107a6-musica-ligera.mp3', 1),
(3, 'New Beginning', 'b6e71-05-another-day-new-beginning.mp3', 1),
(4, 'Let\'s Spend The Night Together', '7e393-02-let-s-spend-the-night-together.mp3', 1),
(5, 'Track 02', '97ccb-02-track-02.mp3', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correo_config`
--

CREATE TABLE `correo_config` (
  `id` int(11) NOT NULL,
  `puerto` varchar(5) NOT NULL,
  `servidor_correo` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `webmaster_email` varchar(200) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `correo_config`
--

INSERT INTO `correo_config` (`id`, `puerto`, `servidor_correo`, `email`, `password`, `webmaster_email`, `estado`) VALUES
(1, '465', 'ssl://gator3246.hostgator.com', 'info@guerreroferreccio.com', 'infoinfo2018', 'carlos@guerreroferreccio.com ', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudio`
--

CREATE TABLE `estudio` (
  `id` int(11) NOT NULL,
  `saludo` text NOT NULL,
  `saludo_en` text NOT NULL,
  `acerca` text NOT NULL,
  `acerca_en` text NOT NULL,
  `imagen_acerca` varchar(50) NOT NULL,
  `fondo_acerca` varchar(50) NOT NULL,
  `vision` text NOT NULL,
  `vision_en` text NOT NULL,
  `imagen_vision` varchar(50) NOT NULL,
  `fondo_vision` varchar(50) NOT NULL,
  `mision` text NOT NULL,
  `mision_en` text NOT NULL,
  `imagen_mision` varchar(50) NOT NULL,
  `fondo_mision` varchar(50) NOT NULL,
  `ventajas` text NOT NULL,
  `ventajas_en` text NOT NULL,
  `imagen_ventajas` varchar(50) NOT NULL,
  `fondo_ventajas` varchar(50) NOT NULL,
  `estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estudio`
--

INSERT INTO `estudio` (`id`, `saludo`, `saludo_en`, `acerca`, `acerca_en`, `imagen_acerca`, `fondo_acerca`, `vision`, `vision_en`, `imagen_vision`, `fondo_vision`, `mision`, `mision_en`, `imagen_mision`, `fondo_mision`, `ventajas`, `ventajas_en`, `imagen_ventajas`, `fondo_ventajas`, `estado`) VALUES
(1, 'Carlos Guerrero Ferreccio Arquitectos, es un estudio centrado en la Arquitectura, el Interiorismo, el Diseño Interior Corporativo.\r\nDesarrolla proyectos de investigación, diseño e infraestructura.\r\nCon 18 años de experiencia en centros culturales, recreativos y residenciales, así como desarrollos urbanos, plazas, parques y mobiliario.', 'Carlos Guerrero Ferreccio Architects, is a study focused on Architecture, Interior Design and Corporate .\r\nDevelops research, design and infrastructure projects.\r\nWith 18 years of experience in cultural, recreational and residential centers, and urban developments, parks, squares , and furnitures.', 'La oficina con sede en la ciudad de Guayaquil, ofrece un diseño dirigido y adaptado para cada tipo de cliente, contexto y presupuesto.\r\nEn nuestros diseños conjugamos función y orden, combinando lo tradicional, creativo e innovador.\r\nEmpleando materiales reducidos a su mínima esencia y expresados con sumo cuidado, consiguiendo así una sensación de confort que atiende por igual al cuerpo y al espíritu.\r\nUtilizamos elementos de la arquitectura tradicional, combinados con lo contemporáneo.', 'The office based in Guayaquil, offers a design tailored and adapted for each type of client, context and budget. In our designs we combine function and order, combining the traditional, creative and innovative. Using materials reduced to their minimum essence and expressed with great care, achieving a sense of comfort that attends equally to the body and spirit. We use elements of traditional architecture, combined with the contemporary.', '9c9ff-quienes-somos.png', '', 'Nuestro objetivo es dar una respuesta eficaz y oportuna a las necesidades que plantea el desarrollo sustentable de los espacios que habitamos, ofreciendo soluciones basadas en el diseño y economía en cada proyecto, aplicando estrategias, métodos y tecnologías eficientes con un enfoque de conservación y uso sustentable de los recursos.', 'Our objective is to provide an effective and timely response to the needs of the sustainable development of the spaces we inhabit, offering solutions based on design and economy in each project, applying efficient strategies, methods and technologies with a conservation and sustainable use approach of the resources.', '0df64-proyecto-guayaquil-definitivo-_view-0_4-01.j', '89d13-imagen_vision_2.jpg', 'Brindamos servicios integrales de consultoría, diseño, ingeniería y construcción para toda tipología y escala. En el año 2008 iniciamos actividades con una profunda vocación con el diseño arquitectónico a través de un estrecho vínculo con sus clientes y el entorno.\r\nEn cada etapa de planificación y durante la construcción nos comprometemos con una minuciosa calidad especial y constructiva que permitan un lenguaje arquitectónico coherente y creativo que busque los altos valores de nuestra profesión.', 'We provide integral service of consultancy, design, engineering and construction for all typology and scale. In 2008 we started activities with a deep vocation with the architectural design through a close bond with our clients and the environment. At each planning stage and during construction we are committed to a meticulous spatial and constructive quality that allows a coherent and creative architectural language that seeks the high values ​​of our profession.', 'ad5cd-page-2_img02.jpg', '', 'Oficina', 'Office', '33e3b-oficina-1-02.png', 'c5a57-oficina2-02.png', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `footer`
--

CREATE TABLE `footer` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `titulo_en` varchar(50) NOT NULL,
  `descripcion` text NOT NULL,
  `descripcion_en` text NOT NULL,
  `fondo` varchar(200) NOT NULL,
  `seccion` int(11) NOT NULL,
  `estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `footer`
--

INSERT INTO `footer` (`id`, `titulo`, `titulo_en`, `descripcion`, `descripcion_en`, `fondo`, `seccion`, `estado`) VALUES
(1, 'GUERRERO FERRECCIO', 'GUERRERO FERRECCIO', '<p>\r\n	<style type=\"text/css\">\r\n@page { margin: 2cm }\r\n		p { margin-bottom: 0.25cm; direction: ltr; line-height: 120%; text-align: left; orphans: 2; widows: 2 }	</style>\r\n</p>\r\n<blockquote>\r\n	<p style=\"text-align: justify;\">\r\n		&nbsp; &quot;La abeja averg&uuml;enza con la construcci&oacute;n de sus celdillas&nbsp;a m&aacute;s de un arquitecto, pero lo que distingue al peor de los arquitectos de la mejor abeja, es que aquel, ha construido una celda en su cerebro, antes de construirla en cera.&quot; <small><cite title=\"Karl, Marx\">Karl, Marx</cite></small></p>\r\n</blockquote>\r\n<p>\r\n	<small>\r\n	<style type=\"text/css\">\r\n@page { margin: 2cm }\r\n		p { margin-bottom: 0.25cm; direction: ltr; line-height: 120%; text-align: left; orphans: 2; widows: 2 }	</style>\r\n	</small></p>\r\n', '<p>\r\n	<style type=\"text/css\">\r\n@page { margin: 2cm }\r\n		p { margin-bottom: 0.25cm; direction: ltr; line-height: 120%; text-align: left; orphans: 2; widows: 2 }	</style>\r\n</p>\r\n<blockquote>\r\n	<p style=\"text-align: justify;\">\r\n		&quot;The bee embarrasses the construction of its cells to more than one architect, but what distinguishes the worst of the architects of the best bee is that he has built a cell in his brain before building it in wax.&quot; <small><cite title=\"Karl, Marx\">Karl, Marx</cite> </small></p>\r\n</blockquote>\r\n<p>\r\n	<small>\r\n	<style type=\"text/css\">\r\n@page { margin: 2cm }\r\n		p { margin-bottom: 0.25cm; direction: ltr; line-height: 120%; text-align: left; orphans: 2; widows: 2 }	</style>\r\n	</small></p>\r\n', 'efc0d-arqweb2-15.png', 1, 1),
(2, 'GUERRERO FERRECCIO', 'GUERRERO FERRECCIO', '<p>\r\n	Estudio de arquitectos.</p>\r\n', '<p>\r\n	Architecture Studio.</p>\r\n', '24f30-parallax3.jpg', 2, 1),
(3, 'OFRECEMOS', 'WHAT WE OFFER', '<p>\r\n	CALIDAD DE DISE&Ntilde;O</p>\r\n<p>\r\n	FUNCIONALIDAD</p>\r\n<p>\r\n	EFICIENCIA EN COSTES</p>\r\n', '<p>\r\n	QUALITY OF DESIGN</p>\r\n<p>\r\n	FUNCTIONALITY</p>\r\n<p>\r\n	<style type=\"text/css\">\r\n@page { margin: 2cm }\r\n		p { margin-bottom: 0.25cm; direction: ltr; line-height: 120%; text-align: left; orphans: 2; widows: 2 }	</style>\r\n</p>\r\n<p>\r\n	COST EFFICIENCY</p>\r\n', 'd6b20-parallax4.jpg', 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes_portafolio`
--

CREATE TABLE `imagenes_portafolio` (
  `id_portafolio` int(11) NOT NULL,
  `nombre_archivo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `imagenes_portafolio`
--

INSERT INTO `imagenes_portafolio` (`id_portafolio`, `nombre_archivo`) VALUES
(2, '0cHVtTRVYNOorSU_8b232-sacachum-completa.jpg'),
(3, 'vPY6bhtKBmHL1HX_museografico-completa.jpg'),
(3, 'B00EaKEk3fmemf2_museografico-completa1.jpg'),
(3, 'omX2FKx4aI25hTm_museografico-completa2.jpg'),
(3, 'gfNAI7lNIx24RAk_museografico-completa3.jpg'),
(4, 'iSYZmAkBhmGjqAp_parqueferrocarril-completa.jpg'),
(4, 'yobc4KV6nLvoe1H_parqueferrocarril-completa1.jpg'),
(4, 'ft1rJkGPB3a8J9V_parqueferrocarril-completa2.jpg'),
(4, '5jkCm3KTedbFDkR_parqueferrocarril-completa3.jpg'),
(4, 'DmECn5mmlRDzBCW_parqueferrocarril-completa4.jpg'),
(5, 'XFqQCg7vVNdR6t9_casaolmedo-completa.jpg'),
(5, '55FbPCfHTlmWA8U_casaolmedo-completa1.jpg'),
(5, 'tXqgMudZ9hRwEmB_casaolmedo-completa2.jpg'),
(6, 'pSmdrX0eBuecaWc_universidadartes-completa.jpg'),
(6, 'N8hW7WQxhnC6qQx_universidadartes-completa1.jpg'),
(6, 'SVGzGYphriv263i_universidadartes-completa2.jpg'),
(6, 'jHCNQmxRzX1plt7_universidadartes-completa3.jpg'),
(7, 'BwfVhWA15vG2pzP_casacastagneto-completa.jpg'),
(7, 'O1bvE5B3RtxUjPj_casacastagneto-completa1.jpg'),
(7, 'bmopEh83uids6oi_casacastagneto-completa2.jpg'),
(8, 'KlTmh1PkjyVe6mK_duranyaguachi-completa.jpg'),
(8, 'RAAhOTO5TCPtjTk_duranyaguachi-completa1.jpg'),
(8, 'On1UCZZu2R6HsSU_duranyaguachi-completa2.jpg'),
(8, 'GjY2LazzX4F6q4K_duranyaguachi-completa3.jpg'),
(8, 'yRXb5JUHoxUV97O_duranyaguachi-completa4.jpg'),
(9, '1wec58LnbAQYx75_empanadaschabuca-completa.jpg'),
(9, 'eI41f3yYmIzwpcK_empanadaschabuca-completa1.jpg'),
(9, 'pCoDCDuJZ9U2vSf_empanadaschabuca-completa2.jpg'),
(9, 'I2tvp3vpuGX1FSZ_empanadaschabuca-completa3.jpg'),
(10, 'Xz248ZMi501ZoNr_sumesa-completa.jpg'),
(10, 'dN8RATXwTS7qoaq_sumesa-completa1.jpg'),
(10, 'MJ6TVpZyVJyHhUP_sumesa-completa2.jpg'),
(10, 'QuTXmwwFlXHmRhY_sumesa-completa3.jpg'),
(10, 'PnNlHepFgAhYV11_sumesa-completa4.jpg'),
(11, 'a2SFdif5HkcrUTZ_lascamaras-completa1.jpg'),
(11, 'XvTknaIb149h5KU_lascamaras-completa2.jpg'),
(11, 'JTQbcTO8HsHKROc_lascamaras-completa3.jpg'),
(12, 'zMh5xyv6BRnMh2N_dummy-completa.jpg'),
(1, '4pb2pQA0pMD5rWG_parquehistorico-completa.jpg'),
(1, '1W9v3TR8WlJhgif_parquehistorico-completa1.jpg'),
(1, '7dFrR7ywkGpB38A_parquehistorico-completa2.jpg'),
(1, 'sBjH7FCzH52P0at_parquehistorico-completa3.jpg'),
(13, 'pLG4DILo1IXAKzs_Untitled-2_Parque_Historico_Edit.jpg'),
(13, '3yYUQ1EWd22TCjy_Untitled-2_Parque_Historico_Edit_3.jpg'),
(13, 'dxjoBFIPkmoLVI1_Untitled-2_Parque_Historico_Edit_6.jpg'),
(13, 'ECZxaG7nkht0N2O_Untitled-2_Parque_Historico_Edit_Axonometria.jpg'),
(13, '7Aho8NIDCgI4hLM_Untitled-2_Parque_Historico_zona_urbana_edit.jpg'),
(13, 'TBjndV2SBtmlQ42_Untitled-2_Plaza_Artes_y_Oficio_Edit.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `local`
--

CREATE TABLE `local` (
  `id` int(11) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `direccion_en` varchar(200) NOT NULL,
  `celular` varchar(50) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `horario` varchar(200) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `local`
--

INSERT INTO `local` (`id`, `direccion`, `direccion_en`, `celular`, `telefono`, `email`, `horario`, `latitude`, `longitude`, `estado`) VALUES
(1, 'Lomas de Urdesa, Avenida Quinta 308 entre colinas y Serros.', 'Lomas de Urdesa, Avenida Quinta 308 entre colinas y Serros.', '0999999999', '2-353535', 'carlosguerrerof@hotmail.com', 'Lunes, miércoles y viernes 9:00 - 18:00', -2.1643533, -79.9116593, 0),
(2, 'Alborada Décima Etapa Mz 102, Avenida Principal y Nueva Loja. Condominio Esquinero', 'Alborada Décima Etapa Mz 102, Avenida Principal y Nueva Loja. Condominio Esquinero', '0997524543', '045065705', 'carlos@guerreroferreccio.com', 'Lunes y Viernes 08:30 - 18:00', -2.138403, -79.909887, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portafolio`
--

CREATE TABLE `portafolio` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) DEFAULT NULL,
  `titulo_en` varchar(50) DEFAULT NULL,
  `descripcion` mediumtext,
  `descripcion_en` mediumtext,
  `imagen` varchar(200) NOT NULL,
  `estado` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `portafolio`
--

INSERT INTO `portafolio` (`id`, `titulo`, `titulo_en`, `descripcion`, `descripcion_en`, `imagen`, `estado`) VALUES
(1, 'Parque Histórico', 'Parque Histórico', 'Plaza de las artes y oficio, Nahim Isaías, Maac Manta, Auditorio, Exposición Cosmogonias', 'Plaza de las artes y oficio, Nahim Isaías, Maac Manta, Auditorio, Exposición Cosmogonias', 'b6cc0-parquehistorico-preview.jpg', 1),
(2, 'Min Turismo, Glorieta Cultural, Comuna Sacachum', 'Min Turismo, Glorieta Cultural, Comuna Sacachum', '', '', '5acb2-sacachum-preview.jpg', 1),
(3, 'MIES Vilcabamba Museografía', 'MIES Vilcabamba Museografía', '', '', '1c836-museografico-preview.jpg', 1),
(4, 'Min Turismo, Ferrocarril GYE', 'Min Turismo, Ferrocarril GYE', '', '', 'd6fed-parqueferrocarril-preview.jpg', 1),
(5, 'Casa José Joaquín de Olmedo', 'José Joaquín de Olmedo House', '', '', '38642-casaolmedo-preview.jpg', 1),
(6, 'Gobernación Guayas, Universidad de las Artes', 'Gobernación Guayas, Universidad de las Artes', '', '', '74708-universidadartes-preview.jpg', 1),
(7, 'Casa Castagneto', 'Castagneto House', '', '', '6d028-casacastagneto-preview.jpg', 1),
(8, 'Linea Férrea Durán-Yaguachi', 'Durán-Yaguachi Railway Line', '', '', '5e82e-duranyaguachi-preview.jpg', 1),
(9, 'Empanadas de Chabuca', 'Empanadas de Chabuca', '', '', 'daf92-empanadaschabuca-preview.jpg', 1),
(10, 'Sumesa', 'Sumesa', '', '', 'a1053-sumesa-preview.jpg', 1),
(11, 'Edificio Las Cámaras', 'Edificio Las Cámaras', '', '', '0a52b-lascamaras-preview.jpg', 1),
(12, 'Guerrero Ferreccio', 'Guerrero Ferreccio', '', '', 'b91a202a0bd210373e5cd5a7ab7ac8d0.jpg', 0),
(13, 'Bocetos', 'Sketchs', '', '', '1192781efa15f06fe0cc3daaabb99dd3.png', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `razones`
--

CREATE TABLE `razones` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) DEFAULT NULL,
  `titulo_en` varchar(50) DEFAULT NULL,
  `descripcion` mediumtext,
  `descripcion_en` mediumtext,
  `estado` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `razones`
--

INSERT INTO `razones` (`id`, `titulo`, `titulo_en`, `descripcion`, `descripcion_en`, `estado`) VALUES
(1, 'Razon1', 'Reason1', '<p>\r\n	porque somos los mejores</p>\r\n', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `titulo_en` varchar(50) NOT NULL,
  `descripcion` text NOT NULL,
  `descripcion_en` text NOT NULL,
  `nombre_icono` varchar(50) NOT NULL,
  `estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`id`, `titulo`, `titulo_en`, `descripcion`, `descripcion_en`, `nombre_icono`, `estado`) VALUES
(1, 'Construcción', 'Building', '', '', 'material-icons-layers', 1),
(2, 'Renovación', 'Renovation', '', '', 'material-icons-account_balance', 0),
(3, 'Arquitectura interior', 'Interior architecture', '\r\n', '', 'material-icons-extension', 0),
(4, 'Diseño experimental', 'Experimental design', 'Elementos Corporativo, Residencial\r\n', 'Corporate elements,  Residential Elements', 'material-icons-event_seat', 1),
(5, 'Diseño funcional', 'Functional design', '\r\n', '', 'material-icons-laptop_windows', 0),
(6, 'Planificación', 'Planning', 'Diseño urbano, Diseño arquitectónico, Diseño interior', 'Urban Design, Architecture Desing, Interior Design', 'material-icons-card_travel', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slideshow`
--

CREATE TABLE `slideshow` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `titulo_en` varchar(100) NOT NULL,
  `caption` varchar(100) NOT NULL,
  `caption_en` varchar(100) NOT NULL,
  `enlace` varchar(800) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `fondo` varchar(200) NOT NULL,
  `estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `slideshow`
--

INSERT INTO `slideshow` (`id`, `titulo`, `titulo_en`, `caption`, `caption_en`, `enlace`, `imagen`, `fondo`, `estado`) VALUES
(1, 'TENEMOS PASIÓN EN CREAR NUEVOS Y ÚNICOS ESPACIOS', 'WE HAVE PASSION TO CREATE NEW AND UNIQUE SPACES', 'CREANDO DISEÑOS DE CALIDAD', 'CREATING QUALITY DESIGNS', '', 'bf2bb-page-1_img01.jpg', '0b50b-page-1_slide01.jpg', 1),
(2, 'TENEMOS PASIÓN EN CREAR ESPACIOS NUEVOS Y ÚNICOS', 'WE HAVE PASSION TO CREATE NEW AND UNIQUE SPACES', 'CREANDO DISEÑOS DE CALIDAD', 'CREATING QUALITY DESIGNS', '', '1a125-page-1_img02.jpg', '3b3a7-page-1_slide02.jpg', 1),
(3, 'TENEMOS PASIÓN EN CREAR ESPACIOS NUEVOS Y ÚNICOS', 'WE HAVE PASSION TO CREATE NEW AND UNIQUE SPACES', 'CREANDO DISEÑOS DE CALIDAD', 'CREATING QUALITY DESIGNS', '', 'c7d99-page-1_img03.jpg', '7e68b-page-1_slide03.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `apellido` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `username`, `password`, `email`, `nombre`, `apellido`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'madelyne@cajanegra.com.ec', 'Madelyne', 'Velasco');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `asociado`
--
ALTER TABLE `asociado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `audio`
--
ALTER TABLE `audio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `correo_config`
--
ALTER TABLE `correo_config`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estudio`
--
ALTER TABLE `estudio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `footer`
--
ALTER TABLE `footer`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `local`
--
ALTER TABLE `local`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `portafolio`
--
ALTER TABLE `portafolio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `razones`
--
ALTER TABLE `razones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `slideshow`
--
ALTER TABLE `slideshow`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `asociado`
--
ALTER TABLE `asociado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `audio`
--
ALTER TABLE `audio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `correo_config`
--
ALTER TABLE `correo_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `estudio`
--
ALTER TABLE `estudio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `footer`
--
ALTER TABLE `footer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `local`
--
ALTER TABLE `local`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `portafolio`
--
ALTER TABLE `portafolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `razones`
--
ALTER TABLE `razones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `slideshow`
--
ALTER TABLE `slideshow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
