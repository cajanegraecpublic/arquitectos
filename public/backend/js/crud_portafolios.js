$('.delete-portafolio').on('click', function(){
	var id_portafolio = $(this).attr('data-id-portafolio');
	var confirm_dialog = $.confirm({
	    buttons: {
	        Aceptar: function () {
	            $.ajax({
	            	url: js_base_url('index.php/admin_portafolios/delete_portafolio'),
	            	type: 'POST',
	            	data: {
	            		'id_portafolio': id_portafolio
	            	},
	            	dataType: 'json',
	            	success: function(data){
	            		if (data.error == 0) {
	            			$.alert({
	            			    title: 'Eliminar un portafolio',
	            			    content: 'El portafolio fue eliminado con éxito.',
	            			    buttons:{
		            			    Aceptar: function () {
		            					location.replace(js_base_url('index.php/admin_portafolios/crud_portafolios'));
		            			    }
	            			    }
	            			});
	            		} else {
	            			confirm_dialog.close();
	            			$.alert(data.message);
	            		}
	            	},
	            	error: function(){
	            		console.log('ERROR');
	            	}
	            });
	        },
	        Cancelar: function () {
	            confirm_dialog.close();
	        }
	    },
	    content: '¿Está seguro de querer eliminar este portafolio?',
	    draggable: false,
	    icon: 'fa fa-warning',
	    title: 'Eliminar un portafolio',
	    type: 'red'
	});
});