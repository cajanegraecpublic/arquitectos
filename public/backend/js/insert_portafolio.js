$(document).ready(function() {
	// Ocultamos los contenedores de previews de imagenes para no mostrar tanto espacio
	if ($('#id_portafolio').length == 0) {
		$('#imagen-principal-preview').parent().hide();
		$('#imagenes-portafolio-preview').parent().hide();
	} else {
		$('#imagen-principal-preview').show();
		$('#imagenes-portafolio-preview').show();
	}
});

// Script para la subida de la imagen principal
$('#imagen_principal_boton').on('click', function(){
	$('#imagen_principal_input').click();
});
$('#imagen_principal_input').on('change', function(){
	$('#imagen-principal-form').submit();
});
$('#imagen-principal-form').on('submit', function(e){
	var form = this;
	e.preventDefault();
	$.ajax({
		url: js_base_url('index.php/admin_portafolios/upload_imagenes_portafolio'),
		type: 'POST',
		data: new FormData(this),
		cache: false,
		contentType: false,
		processData: false,
		dataType: 'json',
		beforeSend: function(){
			$('#imagen-principal-form .loading-gif').show();
			$('#imagen-principal-preview').hide();
			$('#imagen-principal-preview .btn-erase').off('click');
		},
		success: function(data){
			if (data.error == 0) {
				if($('label[data-error-input="imagen_principal_portafolio"]').is(':visible')) {
					$('label[data-error-input="imagen_principal_portafolio"] .error-text').html('');
					$('label[data-error-input="imagen_principal_portafolio"]').hide();
				}

				// Desabilitamos el boton para evitar que el usuario suba más imágenes
				$('#imagen_principal_boton').attr('disabled','disabled');
				$('#imagen_principal_input').attr('disabled','disabled');

				// Creamos una columna para la imagen, y la añadimos en #imagen-principal-preview
				var $col = $('<div>', {
					'class': 'col-sm-3 portafolio-img-box pb-10'
				});
				var $img = $('<img>', {
					'class': 'img-responsive img-center',
					'src': js_base_url('assets/uploads/portfolio/' + data['upload_data']['file_name'])
				});
				// La imagen irá dentro de un elemento <a> que nos permitirá mostrar un modal con dicha imagen, el cual se genera posteriormente
				var $modal_trigger = $('<a>', {
					'href': '#',
					'data-toggle': 'modal',
					'data-target': '#imagen-princiapl-portafolio-modal-' + data['upload_data']['raw_name']
				});
				var $btn_erase = $('<button>', {
					'class': 'btn-erase',
					'data-img-name': data['upload_data']['file_name'],
					'data-img-raw-name': data['upload_data']['raw_name']
				}).html('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>');
				$modal_trigger.append($img);
				$col.append($modal_trigger);
				$col.append($btn_erase);
				$('#imagen-principal-preview').append($col);

				// Creamos un modal para la imagen
				var $modal_container = $('<div class="modal fade" id="' + 'imagen-princiapl-portafolio-modal-' + data['upload_data']['raw_name'] + '" tabindex="-1" role="dialog"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><img class="img-responsive img-center" src="' + js_base_url('assets/uploads/portfolio/') + data['upload_data']['file_name'] + '"></div></div></div></div>');
				$('body').append($modal_container);

				// Modificamos el input oculto de la imagen
				$('#imagen-principal-portafolio-input').val(data['upload_data']['file_name']);
				
				// Reajustamos el tamaño de la columna de la imagen y su pocisionamiento, y ocultamos el gif de cargado
				$('#imagen-principal-preview').show(function(e){
					$('#imagen-principal-form .loading-gif').hide();
					$('#imagen-principal-preview .portafolio-img-box, #imagen-principal-preview .portafolio-img-box img').removeAttr('style');
					$('#imagen-principal-preview .portafolio-img-box').matchHeight();
					$('#imagen-principal-preview .portafolio-img-box img').each(function (){
						centrarVertPad($(this));
					});
					// Si se elimina la imagen principal, el elemento #imagen-principal-preview se oculta para no dejar espacios innecesarios en el formulario
					// Siempre que se vaya a subir una imagen nueva, debemos preguntar si este elemento está oculto y mostrarlo de ser el caso
					if(!$('#imagen-principal-preview').parent().is(':visible')) {
						$('#imagen-principal-preview').parent().show();
					}
				});

				$('#imagen-principal-preview .btn-erase').on('click', function(){
					$.ajax({
						url: js_base_url('index.php/admin_portafolios/delete_imagen_portafolio'),
						type: 'POST',
						data: {
							'img_name': $(this).attr('data-img-name'),
							'type': 'principal',
							'img_raw_name': $(this).attr('data-img-raw-name')
						},
						dataType: 'json',
						beforeSend: function(){
							$('#imagen-principal-form .loading-gif').show();
							$('#imagen-principal-preview').hide();
						},
						success: function(data){
							// Habilitamos el input para subir imagenes
							$('#imagen_principal_boton').removeAttr('disabled');
							$('#imagen_principal_input').removeAttr('disabled');

							if (data.error == 0) {
								var img_url = js_base_url('assets/uploads/portfolio/' + data.deleted_image);
								// Eliminamos la comlumna con la imagen del elemento #imagen-principal-preview
								$('img[src~="' + img_url + '"]').parent().parent().remove();
								// Eliminamos el modal que contiene la imagen
								$('div[id="imagen-princiapl-portafolio-modal-' + data.img_raw_name + '"]').remove();
								// Reseteamos el input oculto de la imagen
								$('#imagen-principal-portafolio-input').val('');

								$('#imagen-principal-form .loading-gif').hide();
								$('#imagen-principal-preview').parent().hide();
							} else {
								$('#imagen-principal-preview').show();
								$('#imagen-principal-form .loading-gif').hide();
								$.alert(data.message);
							}
						},
						error: function(){
							$('#imagen-principal-preview').show();
							$('#imagen-principal-form .loading-gif').hide();
							$.alert('Lo sentimos. Hubo un error en el servidor. Contacte con servicio técnico.');
						}
					});
				});
			} else {
				$('#imagen-principal-form .loading-gif').hide();
				$('label[data-error-input="imagen_principal_portafolio"] .error-text').html(data.messages);
				$('label[data-error-input="imagen_principal_portafolio"]').show();
			}

			form.reset();
		},
		error: function(){
			console.log('ERROR');
		}
	});
});

// Script para la subida de las imágenes del portafolio
$('#imagenes_portafolio_boton').on('click', function(){
	$('#imagenes_portafolio_input').click();
});
$('#imagenes_portafolio_input').on('change', function(){
	$('#imagenes-portafolio-form').submit();
});
$('#imagenes-portafolio-form').on('submit', function(e){
	var form = this;
	e.preventDefault();
	$.ajax({
		url: js_base_url('index.php/admin_portafolios/upload_imagenes_portafolio'),
		type: 'POST',
		data: new FormData(this),
		cache: false,
		contentType: false,
		processData: false,
		dataType: 'json',
		beforeSend: function(){
			$('#imagenes-portafolio-form .loading-gif').show();
			$('#imagenes-portafolio-preview').hide();
			$('#imagenes-portafolio-preview .btn-erase').off( 'click');
		},
		success: function(data){
			console.log(data.error);
			if (data['upload_data'].length > 0) {
				if (data.error == 0) {
					if($('label[data-error-input="imagenes_portafolio"]').is(':visible')) {
						$('label[data-error-input="imagenes_portafolio"] .error-text').html('');
						$('label[data-error-input="imagenes_portafolio"]').hide();
					}
				} else {
					if(!$('label[data-error-input="imagenes_portafolio"]').is(':visible')) {
						$('label[data-error-input="imagenes_portafolio"] .error-text').html('Error. Algunos archivos no se pudieron procesar. Asegúrese que el formato sea el adecuado (jpg|jpeg|png|gif).');
						$('label[data-error-input="imagenes_portafolio"]').show();
					}
				}

				for (var i = 0; i < data['upload_data'].length; i++) {
					// Creamos una columna para cada imagen, y la añadimos en #imagenes-portafolio-preview
					var $col = $('<div>', {
						'class': 'col-sm-3 portafolio-img-box pb-10'
					});
					var $img = $('<img>', {
						'class': 'img-responsive img-center',
						'src': js_base_url('assets/uploads/portfolio_imagenes/' + data['upload_data'][i]['file_name'])
					});
					// Las imágenes irán dentro de un elemento <a> que nos permitirá mostrar un modal con dicha imagen, el cual se genera posteriormente
					var $modal_trigger = $('<a>', {
						'href': '#',
						'data-toggle': 'modal',
						'data-target': '#imagen-portafolio-modal-' + data['upload_data'][i]['raw_name']
					});
					var $btn_erase = $('<button>', {
						'class': 'btn-erase',
						'data-img-name': data['upload_data'][i]['file_name'],
						'data-img-raw-name': data['upload_data'][i]['raw_name']
					}).html('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>');
					$modal_trigger.append($img);
					$col.append($modal_trigger);
					$col.append($btn_erase);
					$('#imagenes-portafolio-preview').append($col);

					// Creamos un modal para cada imagen
					var $modal_container = $('<div class="modal fade" id="' + 'imagen-portafolio-modal-' + data['upload_data'][i]['raw_name'] + '" tabindex="-1" role="dialog"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><img class="img-responsive img-center" src="' + js_base_url('assets/uploads/portfolio_imagenes/') + data['upload_data'][i]['file_name'] + '"></div></div></div></div>');
					$('body').append($modal_container);

					// Creamos un input para cada imagen
					var $input = $('<input>', {
						'type': 'hidden',
						'id': 'imagen-portafolio-input-' + data['upload_data'][i]['raw_name'],
						'class': 'imagen-portafolio-input',
						'name': 'imagenes_portafolio[]',
						'value': data['upload_data'][i]['file_name']
					});
					$('#form-create-portafolio').append($input);
					
					// Reajustamos los tamaños de las columnas de imagen y y pocisionamiento, y ocultamos el gif de cargado
					$('#imagenes-portafolio-preview').show(function(e){
						$('#imagenes-portafolio-form .loading-gif').hide();
						$('#imagenes-portafolio-preview .portafolio-img-box, #imagenes-portafolio-preview .portafolio-img-box img').removeAttr('style');
						$('#imagenes-portafolio-preview .portafolio-img-box').matchHeight();
						$('#imagenes-portafolio-preview .portafolio-img-box img').each(function (){
							centrarVertPad($(this));
						});
						// Puede ocurrir que se eliminaron todas las imágenes que se intentó subir
						// Cuando esto ocurre, el elemento #imagenes-portafolio-preview se oculta para no dejar espacios innecesarios en el formulario
						// Siempre que se vaya a subir una imagen nueva, debemos preguntar si este elemento está oculto y mostrarlo de ser el caso
						if(!$('#imagenes-portafolio-preview').parent().is(':visible')) {
							$('#imagenes-portafolio-preview').parent().show();
						}
					});
				}

				$('#imagenes-portafolio-preview .btn-erase').on('click', function(){
					$.ajax({
						url: js_base_url('index.php/admin_portafolios/delete_imagen_portafolio'),
						type: 'POST',
						data: {
							'img_name': $(this).attr('data-img-name'),
							'img_raw_name': $(this).attr('data-img-raw-name')
						},
						dataType: 'json',
						beforeSend: function(){
							$('#imagenes-portafolio-form .loading-gif').show();
							$('#imagenes-portafolio-preview').hide();
						},
						success: function(data){
							if (data.error == 0) {
								var img_url = js_base_url('assets/uploads/portfolio_imagenes/' + data.deleted_image);
								// Eliminamos la comlumna con la imagen del elemento #imagenes-portafolio-preview
								$('img[src~="' + img_url + '"]').parent().parent().remove();
								// Eliminamos el modal que contiene la imagen
								$('div[id="imagen-portafolio-modal-' + data.img_raw_name + '"]').remove();
								// Eliminamos el input de la imagen
								$('input[id="imagen-portafolio-input-' + data.img_raw_name + '"]').remove();

								if ($('#imagenes-portafolio-preview .portafolio-img-box').length > 0) {
									$('#imagenes-portafolio-preview').show(function(e){
										$('#imagenes-portafolio-form .loading-gif').hide();
										$('#imagenes-portafolio-preview .portafolio-img-box, #imagenes-portafolio-preview .portafolio-img-box img').removeAttr('style');
										$('#imagenes-portafolio-preview .portafolio-img-box').matchHeight();
										$('#imagenes-portafolio-preview .portafolio-img-box img').each(function () {
											centrarVertPad($(this));
										});
									});
								} else {
									$('#imagenes-portafolio-form .loading-gif').hide();
									$('#imagenes-portafolio-preview').parent().hide();
								}
							} else {
								$('#imagenes-portafolio-preview').show();
								$('#imagenes-portafolio-form .loading-gif').hide();
								$.alert(data.message);
							}
						},
						error: function(){
							$('#imagenes-portafolio-preview').show();
							$('#imagenes-portafolio-form .loading-gif').hide();
							$.alert('Lo sentimos. Es posible que el archivo especificado no exista.');
						}
					});
				});
			} else {
				$('#imagenes-portafolio-form .loading-gif').hide();
				if(!$('label[data-error-input="imagenes_portafolio"]').is(':visible')) {
					$('label[data-error-input="imagenes_portafolio"] .error-text').html('Error. Algunos archivos no se pudieron procesar. Asegúrese que el formato sea el adecuado (jpg|jpeg|png|gif) y el tamaño no mayor a 3MB');
					$('label[data-error-input="imagenes_portafolio"]').show();
				}
			}

			form.reset();
		},
		error: function(){
			console.log('ERROR');
		}
	});
});

$('#form-create-portafolio').validate({
    debug: false,
    focusCleanup: true,
    ignore: [], // Con esto podremos validar el input oculto con el nombre de la imagen del portafolio
    rules:{
    	titulo_es: {
    		required: true
    	},
        titulo_en: {
            required: true
        },
        estado: {
        	required: true
        },
        imagen_principal_portafolio: {
        	required: true
        }
    },
    messages:{
    	titulo_es: {
    		required: 'Debe escribir un título para el portafolio.'
    	},
        titulo_en: {
        	required: 'Debe escribir un título en Inglés para el portafolio.'
        },
        estado: {
        	required: 'Debe especificar el estado del portafolio.'
        },
        imagen_principal_portafolio: {
        	required: 'Debe incluir una foto preview para el portafolio.'
        }
    },
    errorPlacement: function(error, element) {
    	$('.error-label').each(function(){
    		if ($(this).attr('data-error-input') == element.attr('name')) {
    			$(this).children('.error-text').html(error.html());
    			$(this).show();
    		}
    	});
	}
});

// Confirmamos que el usuario desee regresar al grid de portafolios luego de crear un portafolio
$('#form-button-save').on('click', function(){
	var form = $('#form-create-portafolio')[0];
	$.ajax({
		url: js_base_url('index.php/admin_portafolios/process_data_portafolio'),
		type: 'POST',
		data: new FormData(form),
		cache: false,
		contentType: false,
		processData: false,
		dataType: 'json',
		beforeSend: function(){
			$('.error-label').hide();
			$('.error-label .error-text').html('');
			var validation = $('#form-create-portafolio').valid();
			if (!validation) {
				// $('.error-label').show();
				return validation;
			}
		},
		success: function(data){
			if (data.error == 0) {
				// location.replace(js_base_url('admin_portafolios/crud_portafolios'));
				var confirm_dialog = $.confirm({
				    buttons: {
				        regresar: {
				        	text: 'Aceptar',
				            action: function(){
				            	location.replace(js_base_url('index.php/admin_portafolios/crud_portafolios'));
				            }
				        },
				        seguir_creando: {
				        	text: 'Cancelar',
				        	action: function(){
				        		if ($('#id_portafolio').length == 0) {
					        		// Reseteamos los formularios
					        		$('#form-create-portafolio').trigger('reset');
					        		$('#imagen-principal-form').trigger('reset');
					        		$('#imagenes-portafolio-form').trigger('reset');
					        		// Eliminamos los previews de las imagenes de portafolio
					        		$('#imagen-principal-preview').html('')
					        		$('#imagen-principal-preview').parent().hide();
					        		$('#imagenes-portafolio-preview').html('')
					        		$('#imagenes-portafolio-preview').parent().hide();
					        		// Eliminarmos los modals de las imagenes de portafolio
					        		$('.modal').remove()
					        		// Reseteamos el input oculto de la imagen principal
									$('#imagen-principal-portafolio-input').val('');
					        		//  Eliminamos los inputs de las imagenes de portafolio
					        		$('.imagen-portafolio-input').remove()
				        		}

				            	confirm_dialog.close();
				        	}
				        }
				    },
				    content: 'Portafolio guardado exitosamente. ¿Desea regresar a la lista de portafolios?',
				    draggable: false,
				    icon: 'fa fa-check-circle',
				    title: 'Añadir/Editar Portafolios',
				    type: 'green'
				});
			} else if (data.error == 1) {
				console.log(data.message);
			} else {
				console.log(data.messages);
				for (var key in data.messages) {
					$('.error-label').each(function(){
						if ($(this).attr('data-error-input') == key) {
							$(this).children('.error-text').html(data.messages[key]);
							$(this).show();
						}
					});
				}
			}
		},
		error: function(){
			console.log('ERROR');
		}
	});
});

// Confirmamos que el usuario desee regresar al grid de portafolios
$('#form-button-cancel').on('click', function(){
	// confirm('Los datos que intenta añadir no se han guardado. ¿Está seguro que quiere volver a la lista?');
	var confirm_dialog = $.confirm({
	    buttons: {
	        Aceptar: function () {
	            location.replace(js_base_url('index.php/admin_portafolios/crud_portafolios'));
	        },
	        Cancelar: function () {
	            confirm_dialog.close();
	        }
	    },
	    content: 'Los datos que intenta añadir no se han guardado. ¿Está seguro que quiere volver a la lista?',
	    draggable: false,
	    icon: 'fa fa-warning',
	    title: 'Regresar a la lista de portafolios',
	    type: 'red'
	});
});

function centrarVertPad(elemento){
	// altura1 = elemento.parent().height();
	altura1 = elemento.parents('.portafolio-img-box').height();
	altura2 = elemento.height();
	elemento.css('padding-top',(altura1/2)-(altura2/2)+'px');
}

$('#imagen-principal-preview .btn-erase').on('click', function(){
	$.ajax({
		url: js_base_url('index.php/admin_portafolios/delete_imagen_portafolio'),
		type: 'POST',
		data: {
			'img_name': $(this).attr('data-img-name'),
			'type': 'principal',
			'img_raw_name': $(this).attr('data-img-raw-name')
		},
		dataType: 'json',
		beforeSend: function(){
			$('#imagen-principal-form .loading-gif').show();
			$('#imagen-principal-preview').hide();
		},
		success: function(data){
			// Habilitamos el input para subir imagenes
			$('#imagen_principal_boton').removeAttr('disabled');
			$('#imagen_principal_input').removeAttr('disabled');

			if (data.error == 0) {
				var img_url = js_base_url('assets/uploads/portfolio/' + data.deleted_image);
				// Eliminamos la comlumna con la imagen del elemento #imagen-principal-preview
				$('img[src~="' + img_url + '"]').parent().parent().remove();
				// Eliminamos el modal que contiene la imagen
				$('div[id="imagen-portafolio-modal-' + data.img_raw_name + '"]').remove();
				// Reseteamos el input oculto de la imagen
				$('#imagen-principal-portafolio-input').val('');

				$('#imagen-principal-form .loading-gif').hide();
				$('#imagen-principal-preview').parent().hide();
			} else {
				$('#imagen-principal-preview').show();
				$('#imagen-principal-form .loading-gif').hide();
				$.alert(data.message);
			}
		},
		error: function(){
			$('#imagen-principal-preview').show();
			$('#imagen-principal-form .loading-gif').hide();
			$.alert('Lo sentimos. Hubo un error en el servidor. Contacte con servicio técnico.');
		}
	});
});
$('#imagenes-portafolio-preview .btn-erase').on('click', function(){
	$.ajax({
		url: js_base_url('index.php/admin_portafolios/delete_imagen_portafolio'),
		type: 'POST',
		data: {
			'img_name': $(this).attr('data-img-name'),
			'img_raw_name': $(this).attr('data-img-raw-name')
		},
		dataType: 'json',
		beforeSend: function(){
			$('#imagenes-portafolio-form .loading-gif').show();
			$('#imagenes-portafolio-preview').hide();
		},
		success: function(data){
			if (data.error == 0) {
				var img_url = js_base_url('assets/uploads/portfolio_imagenes/' + data.deleted_image);
				// Eliminamos la comlumna con la imagen del elemento #imagenes-portafolio-preview
				$('img[src~="' + img_url + '"]').parent().parent().remove();
				// Eliminamos el modal que contiene la imagen
				$('div[id="imagen-portafolio-modal-' + data.img_raw_name + '"]').remove();
				// Eliminamos el input de la imagen
				$('input[id="imagen-portafolio-input-' + data.img_raw_name + '"]').remove();

				if ($('#imagenes-portafolio-preview .portafolio-img-box').length > 0) {
					$('#imagenes-portafolio-preview').show(function(e){
						$('#imagenes-portafolio-form .loading-gif').hide();
						$('#imagenes-portafolio-preview .portafolio-img-box, #imagenes-portafolio-preview .portafolio-img-box img').removeAttr('style');
						$('#imagenes-portafolio-preview .portafolio-img-box').matchHeight();
						$('#imagenes-portafolio-preview .portafolio-img-box img').each(function () {
							centrarVertPad($(this));
						});
					});
				} else {
					$('#imagenes-portafolio-form .loading-gif').hide();
					$('#imagenes-portafolio-preview').parent().hide();
				}
			} else {
				$('#imagenes-portafolio-preview').show();
				$('#imagenes-portafolio-form .loading-gif').hide();
				$.alert(data.message);
			}
		},
		error: function(){
			$('#imagenes-portafolio-preview').show();
			$('#imagenes-portafolio-form .loading-gif').hide();
			$.alert('Lo sentimos. Es posible que el archivo especificado no exista.');
		}
	});
});