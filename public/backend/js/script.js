$(window).bind('scroll', function() {
    if ($(window).scrollTop() > 150) {
        $('#navbar-header').addClass('navbar-scrolled li a');
        $('#brand-img').addClass('brand-img-scrolled');
        $('#brand-container').addClass('brand-container-scrolled');
    }
    else {
        $('#navbar-header').removeClass('navbar-scrolled li a');
        $('#brand-img').removeClass('brand-img-scrolled');
        $('#brand-container').removeClass('brand-container-scrolled');
    }
});

$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 500);
        return false;
      }
    }
  });
});

$(document).ready(function () {
	$('#carousel').owlCarousel({
 
      autoPlay: 3000, //Set AutoPlay to 3 seconds
 
      items : 3,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3],
      itemsTablet : [767, 2],
      itemsMobile : false,
      pagination : true
 
  });

  $('#field-nombre').attr('placeholder','Nombre');
});